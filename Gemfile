source 'https://rubygems.org'

ruby '2.0.0'
gem 'rails', '3.2.13'

# Bundle edge Rails instead:
# gem 'rails', :git => 'git://github.com/rails/rails.git'

gem "squeel"  # Last officially released gem

gem 'mysql2'
gem 'pg'

# Devise gem
gem 'devise'
gem 'cancan'
# jQuery file upload
gem 'jquery-fileupload-rails'

# Attachments
gem 'paperclip'
gem 'paperclipdropbox', git: 'git://github.com/mohamagdy/paperclipdropbox.git'

# HAML
gem 'haml'
gem 'haml-rails'
gem 'haml_coffee_assets'

# Backbone
gem 'backbone-on-rails'

# Charts
gem 'chart-js-rails'

# Datatables
gem 'jquery-datatables-rails', git: 'git://github.com/rweng/jquery-datatables-rails.git'
#this gem is used because of jquery datatables
gem 'jquery-migrate-rails'


# Annotate modesl
gem 'annotate'

# Configurations
gem 'configatron'

# Coloring the console
gem 'colorize'

# Monitor directories for changes
gem 'listen'

# For time manipulation
gem 'chronic'
gem 'chronic_duration'

# pagination
gem "will_paginate", "3.0.4"

# importing csv, xls
gem 'roo'

# zipping files
gem "rubyzip", :require => 'zip/zip'

# Unicorn and Capistrano
gem 'unicorn', '~> 4.6.3'
gem 'capistrano', '~> 2.15.5'
gem 'execjs'
gem 'therubyracer'

gem 'thin'
# Gems used only for assets and not required
# in production environments by default.
group :assets do
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'

  # See https://github.com/sstephenson/execjs#readme for more supported runtimes
  # gem 'therubyracer', :platforms => :ruby

  gem 'uglifier', '>= 1.0.3'

  gem 'twitter-bootstrap-rails'
  gem 'chosen-rails'
  gem "mediaelement_rails"
end

gem 'jquery-rails'
gem 'jquery-ui-rails', "~> 4.0.4"
gem 'jquery-timepicker-addon-rails'


# For building APIs
gem 'jbuilder'

# Progress bar gem
gem 'progress_bar'

# JavaScript routes
gem 'js-routes'

group :production do
	gem 'pg'
end

# Solr search engine
gem 'sunspot_rails'
# Development specific gems
group :development do
  gem 'sextant'
  gem 'pry'
  gem 'meta_request'

	# Quieting the assets
	gem 'quiet_assets', group: :development

  # Solr search engine
  gem 'sunspot_solr'
end

# To use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# To use Jbuilder templates for JSON
# gem 'jbuilder'

# Use unicorn as the app server
# gem 'unicorn'

# Deploy with Capistrano
# gem 'capistrano'

# To use debugger
# gem 'debugger'
