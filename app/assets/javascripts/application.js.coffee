# This is a manifest file that'll be compiled into application.js, which will include all the files
# listed below.
#
# Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
# or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
#
# It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
# the compiled file.
#
# WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
# GO AFTER THE REQUIRES BELOW.
#
#= require jquery
#= require jquery-migrate-min
#= require jquery_ujs
#= require jquery.ui.all
# require twitter/bootstrap
# require bootstrap.file-input
# require twitter/bootstrap/bootstrap-button
#= require chart
# require bootstrap-datepicker
#= require mediaelement_rails
# require_tree .
#= require dataTables/jquery.dataTables
#= require dataTables/jquery.dataTables.bootstrap
#= require dataTables/extras/ColReorder
#= require js-routes
#= require underscore
#= require backbone
#= require hamlcoffee
#= require quality_cloud
#= require_tree ../templates
#= require_tree ./helpers
#= require_tree ./models
#= require ./views/base
#= require ./views/modal_base
#= require_tree ./views
#= require_tree ./routers
#= require jquery.form.min
#= require chosen-jquery
#= require jquery-ui-timepicker-addon
#= require ajax-loader
#= require jquery.dragtable
#= require date
#= require backbone.pagination
