class @QualityCloud.Helpers.TableHelpers
  #t_s: table_selector
  #this function before calling data-table function on the table
  @arrangeTableHeader:(t_s, columns_settings)->
    header_row = t_s.find("thead tr")
    for column in columns_settings
      column_header = t_s.find("th[data-column_id=#{column.id}]")
      if column.isVisible
        header_row.append(column_header)
      else
        column_header.remove()

  @arrangeTableRow:(t_r, columns_settings)->
    for column in columns_settings
      column_header = t_r.find("td[data-column_id=#{column.id}]")
      if column.isVisible
        t_r.append(column_header)
      else
        column_header.remove()

  @_renderPagination:(collection, view)->
    num_of_pages = 5
    pages_before = Math.floor(num_of_pages/2)

    view.$("#js_pagination_div").show()
    
    view.$("#js_pages_list").html("")
    #render number of pages
    start = if (ref = collection.currentPage-pages_before) > 0 then ref else 1
    end = if (ref = collection.currentPage+pages_before) < collection.total_pages then ref else collection.total_pages
    view.$("#js_pages_list").html("Page <input type='textbox' id='current' value='#{collection.currentPage}'/> of #{collection.total_pages}")
    view.$("#js_total_pages").html("#{collection.total_entries} Total Results")

    QualityCloud.Helpers.TableHelpers._disableNavigationBtns(collection, view)

  @_disableNavigationBtns:(collection, view)->
    if collection.currentPage is 1 
      view.$("#js_pagination_first, #js_pagination_prev").addClass("pagination-selected")
      view.disableEvent "click #js_pagination_first"
      view.disableEvent "click #js_pagination_prev"
    else
      view.$("#js_pagination_first, #js_pagination_prev").removeClass("pagination-selected")
      view.enableEvent "click #js_pagination_first"
      view.enableEvent "click #js_pagination_prev"

    if collection.currentPage is collection.total_pages
      view.$("#js_pagination_next, #js_pagination_last").addClass("pagination-selected")
      view.disableEvent "click #js_pagination_next"
      view.disableEvent "click #js_pagination_last"
    else
      view.$("#js_pagination_next, #js_pagination_last").removeClass("pagination-selected").removeAttr("disabled")
      view.enableEvent "click #js_pagination_next"
      view.enableEvent "click #js_pagination_last"

    view.$(".js_pagination_page").removeClass("pagination-selected")
    view.$(".js_pagination_page[data-page=#{collection.currentPage}]").addClass("pagination-selected")





