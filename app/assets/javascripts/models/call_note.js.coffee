class QualityCloud.Models.CallNote extends Backbone.Model
  urlRoot: 'call_notes'
  paramRoot: 'call_note'


class QualityCloud.Collections.CallNotes extends Backbone.Collection
  model: QualityCloud.Models.CallNote
  initialize:(options)=>
    @url = "calls/#{options.call_id}/notes"