class QualityCloud.Models.Group extends Backbone.Model
  urlRoot: '/groups'
  paramRoot: 'group'

class QualityCloud.Collections.Groups extends Backbone.Collection
  model: QualityCloud.Models.Group
  url:-> 
    return "/groups"