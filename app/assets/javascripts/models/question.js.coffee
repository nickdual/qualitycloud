class QualityCloud.Models.Question extends Backbone.Model
  urlRoot: 'questions'
  paramRoot: 'question'


class QualityCloud.Collections.Questions extends Backbone.Collection
  model: QualityCloud.Models.Question
  initialize:(options)=>
    @url = "templates/#{options.template_id}/questions"