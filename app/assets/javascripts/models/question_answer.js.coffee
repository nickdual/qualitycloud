class QualityCloud.Models.QuestionAnswer extends Backbone.Model
  urlRoot: 'question_answers'
  paramRoot: 'question_answer'

class QualityCloud.Collections.QuestionAnswers extends QualityCloud.Collections.BaseColection
  model: QualityCloud.Models.QuestionAnswer
  initialize:(options = {})=>
    @url = "template_answers/#{options.template_answer_id}/question_answers"
