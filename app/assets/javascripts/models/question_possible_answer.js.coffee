class QualityCloud.Models.QuestionPossibleAnswer extends Backbone.Model
  urlRoot: 'question_possible_answers'
  paramRoot: 'question_possible_answer'


class QualityCloud.Collections.QuestionPossibleAnswers extends Backbone.Collection
  model: QualityCloud.Models.QuestionPossibleAnswer
  initialize:(options)=>
    @url = "question/#{options.question_id}/question_possible_answers"