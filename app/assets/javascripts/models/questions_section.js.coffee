class QualityCloud.Models.QuestionsSection extends Backbone.Model
  urlRoot: 'questions_sections'
  paramRoot: 'questions_section'


class QualityCloud.Collections.QuestionsSections extends Backbone.Collection
  model: QualityCloud.Models.QuestionsSection
  initialize:(options)=>
    @url = "templates/#{options.template_id}/questions_sections"