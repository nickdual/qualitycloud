class QualityCloud.Models.Role extends Backbone.Model
  urlRoot: '/roles'
  paramRoot: 'role'

class QualityCloud.Collections.Roles extends Backbone.Collection
  model: QualityCloud.Models.Role
  url:-> 
    return "/roles"