class QualityCloud.Models.RoleAccess extends Backbone.Model
  urlRoot: '/role_accesses'
  paramRoot: 'role_access'

class QualityCloud.Collections.RoleAccesses extends Backbone.Collection
  model: QualityCloud.Models.RoleAccess
  url:->
    return "/role_accesses"