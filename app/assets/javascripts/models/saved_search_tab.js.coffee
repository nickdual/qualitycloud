class QualityCloud.Models.SavedSearchTab extends Backbone.Model
  urlRoot: 'saved_search_tabs'
  paramRoot: 'saved_search_tab'

class QualityCloud.Collections.UserSavedSearchTabs extends Backbone.Collection
  model: QualityCloud.Models.SavedSearchTab
  initialize:(options)=>
    @url = "users/#{options.user_id}/saved_search_tabs"

class QualityCloud.Collections.CurrentUserSavedSearchTabs extends Backbone.Collection
  model: QualityCloud.Models.SavedSearchTab
  initialize:(options)=>
    @url = "users/current_user_saved_search_tabs"