class QualityCloud.Models.Template extends Backbone.Model
  urlRoot: 'templates'
  paramRoot: 'template'


class QualityCloud.Collections.Templates extends QualityCloud.Collections.BaseColection
  model: QualityCloud.Models.Template

  initialize:(options)=>
    @url = "templates"
   