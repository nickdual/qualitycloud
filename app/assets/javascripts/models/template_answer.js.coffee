class QualityCloud.Models.TemplateAnswer extends Backbone.Model
  urlRoot: 'template_answers'
  paramRoot: 'template_answer'


class QualityCloud.Collections.TemplateAnswers extends Backbone.Collection
  model: QualityCloud.Models.TemplateAnswer
  initialize:(options)=>
    @url = "templates/#{options.template_id}/template_answers"