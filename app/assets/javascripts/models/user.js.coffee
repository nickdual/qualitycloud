class QualityCloud.Models.User extends Backbone.Model
  urlRoot: 'admin/users'
  paramRoot: 'user'

  has_permission:(permission)=>
    if permission in @get("permissions")
      return true
    else if permission in QualityCloud.permissions
      return false
    else
      throw "unknown permission"

class QualityCloud.Collections.Users extends QualityCloud.Collections.BaseColection
  model: QualityCloud.Models.User

  initialize:(options = {})=>
    pagination_attr = {per_page: QualityCloud.current_user.get("settings").users_table_per_page}
    super($.extend(pagination_attr, options))
    QualityCloud.current_user.on "change:settings", @updatePagination

  updatePagination:=>
    users_per_page = QualityCloud.current_user.get("settings").users_table_per_page
    if @paginationConfig?.ipp? and users_per_page? and @paginationConfig?.ipp != users_per_page
      @paginationConfig?.ipp = users_per_page

  url:-> 
    return "admin/users"

class QualityCloud.Collections.ImportedUsers extends Backbone.Collection
  model: QualityCloud.Models.User
  initialize:(options)=>
    @url = "admin/users"

class QualityCloud.Collections.UserNames extends QualityCloud.Collections.Users
  model: QualityCloud.Models.User
  url:-> 
    return "admin/users/user_names"

class QualityCloud.Collections.GroupUsers extends QualityCloud.Collections.Users
  model: QualityCloud.Models.User
  data:
    members: true
  initialize:(options)=>
    super(options)
    @url = Routes.group_users_path(options.group_id)

class QualityCloud.Collections.NonGroupUsers extends QualityCloud.Collections.Users
  model: QualityCloud.Models.User
  data:
    members: false
  initialize:(options)=>
    super(options)
    @url = Routes.group_users_path(options.group_id)
