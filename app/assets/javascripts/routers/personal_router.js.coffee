class QualityCloud.Routers.Personal extends Backbone.Router

  initialize:(options)->
    @view = options.personal_tab

  routes:
    '': "users"
    "users": "users"
    "groups": "groups"
    "roles": "roles"

  users:->
    @view.showUsersTab()

  groups:->
    @view.showGroupsTab()

  roles:->
    @view.showRolesTab()
