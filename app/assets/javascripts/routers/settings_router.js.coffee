class QualityCloud.Routers.Settings extends Backbone.Router

  initialize:(options)->
    @view = options.settings_tab

  routes:
    '': "template_tool"
    "template_tool": "template_tool"

  template_tool:->
    @view.showTemplateTool()