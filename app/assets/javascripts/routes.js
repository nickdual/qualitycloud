(function() {
  var NodeTypes, ParameterMissing, Utils, defaults,
    __hasProp = {}.hasOwnProperty;

  ParameterMissing = function(message) {
    this.message = message;
  };

  ParameterMissing.prototype = new Error();

  defaults = {
    prefix: "",
    default_url_options: {}
  };

  NodeTypes = {"GROUP":1,"CAT":2,"SYMBOL":3,"OR":4,"STAR":5,"LITERAL":6,"SLASH":7,"DOT":8};

  Utils = {
    serialize: function(obj) {
      var i, key, prop, result, s, val, _i, _len;

      if (!obj) {
        return "";
      }
      if (window.jQuery) {
        result = window.jQuery.param(obj);
        return (!result ? "" : result);
      }
      s = [];
      for (key in obj) {
        if (!__hasProp.call(obj, key)) continue;
        prop = obj[key];
        if (prop != null) {
          if (this.getObjectType(prop) === "array") {
            for (i = _i = 0, _len = prop.length; _i < _len; i = ++_i) {
              val = prop[i];
              s.push("" + key + (encodeURIComponent("[]")) + "=" + (encodeURIComponent(val.toString())));
            }
          } else {
            s.push("" + key + "=" + (encodeURIComponent(prop.toString())));
          }
        }
      }
      if (!s.length) {
        return "";
      }
      return s.join("&");
    },
    clean_path: function(path) {
      var last_index;

      path = path.split("://");
      last_index = path.length - 1;
      path[last_index] = path[last_index].replace(/\/+/g, "/").replace(/\/$/m, "");
      return path.join("://");
    },
    set_default_url_options: function(optional_parts, options) {
      var i, part, _i, _len, _results;

      _results = [];
      for (i = _i = 0, _len = optional_parts.length; _i < _len; i = ++_i) {
        part = optional_parts[i];
        if (!options.hasOwnProperty(part) && defaults.default_url_options.hasOwnProperty(part)) {
          _results.push(options[part] = defaults.default_url_options[part]);
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    },
    extract_anchor: function(options) {
      var anchor;

      anchor = "";
      if (options.hasOwnProperty("anchor")) {
        anchor = "#" + options.anchor;
        delete options.anchor;
      }
      return anchor;
    },
    extract_options: function(number_of_params, args) {
      var ret_value;

      ret_value = {};
      if (args.length > number_of_params && this.getObjectType(args[args.length - 1]) === "object") {
        ret_value = args.pop();
      }
      return ret_value;
    },
    path_identifier: function(object) {
      var property;

      if (object === 0) {
        return "0";
      }
      if (!object) {
        return "";
      }
      property = object;
      if (this.getObjectType(object) === "object") {
        property = object.to_param || object.id || object;
        if (this.getObjectType(property) === "function") {
          property = property.call(object);
        }
      }
      return property.toString();
    },
    clone: function(obj) {
      var attr, copy, key;

      if ((obj == null) || "object" !== this.getObjectType(obj)) {
        return obj;
      }
      copy = obj.constructor();
      for (key in obj) {
        if (!__hasProp.call(obj, key)) continue;
        attr = obj[key];
        copy[key] = attr;
      }
      return copy;
    },
    prepare_parameters: function(required_parameters, actual_parameters, options) {
      var i, result, val, _i, _len;

      result = this.clone(options) || {};
      for (i = _i = 0, _len = required_parameters.length; _i < _len; i = ++_i) {
        val = required_parameters[i];
        result[val] = actual_parameters[i];
      }
      return result;
    },
    build_path: function(required_parameters, optional_parts, route, args) {
      var opts, parameters, result, url, url_params;

      args = Array.prototype.slice.call(args);
      opts = this.extract_options(required_parameters.length, args);
      if (args.length > required_parameters.length) {
        throw new Error("Too many parameters provided for path");
      }
      parameters = this.prepare_parameters(required_parameters, args, opts);
      this.set_default_url_options(optional_parts, parameters);
      result = "" + (this.get_prefix()) + (this.visit(route, parameters));
      url = Utils.clean_path("" + result + (this.extract_anchor(parameters)));
      if ((url_params = this.serialize(parameters)).length) {
        url += "?" + url_params;
      }
      return url;
    },
    visit: function(route, parameters, optional) {
      var left, left_part, right, right_part, type, value;

      if (optional == null) {
        optional = false;
      }
      type = route[0], left = route[1], right = route[2];
      switch (type) {
        case NodeTypes.GROUP:
          return this.visit(left, parameters, true);
        case NodeTypes.STAR:
          return this.visit_globbing(left, parameters, true);
        case NodeTypes.LITERAL:
        case NodeTypes.SLASH:
        case NodeTypes.DOT:
          return left;
        case NodeTypes.CAT:
          left_part = this.visit(left, parameters, optional);
          right_part = this.visit(right, parameters, optional);
          if (optional && !(left_part && right_part)) {
            return "";
          }
          return "" + left_part + right_part;
        case NodeTypes.SYMBOL:
          value = parameters[left];
          if (value != null) {
            delete parameters[left];
            return this.path_identifier(value);
          }
          if (optional) {
            return "";
          } else {
            throw new ParameterMissing("Route parameter missing: " + left);
          }
          break;
        default:
          throw new Error("Unknown Rails node type");
      }
    },
    visit_globbing: function(route, parameters, optional) {
      var left, right, type, value;

      type = route[0], left = route[1], right = route[2];
      value = parameters[left];
      if (value == null) {
        return this.visit(route, parameters, optional);
      }
      parameters[left] = (function() {
        switch (this.getObjectType(value)) {
          case "array":
            return value.join("/");
          default:
            return value;
        }
      }).call(this);
      return this.visit(route, parameters, optional);
    },
    get_prefix: function() {
      var prefix;

      prefix = defaults.prefix;
      if (prefix !== "") {
        prefix = (prefix.match("/$") ? prefix : "" + prefix + "/");
      }
      return prefix;
    },
    _classToTypeCache: null,
    _classToType: function() {
      var name, _i, _len, _ref;

      if (this._classToTypeCache != null) {
        return this._classToTypeCache;
      }
      this._classToTypeCache = {};
      _ref = "Boolean Number String Function Array Date RegExp Undefined Null".split(" ");
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        name = _ref[_i];
        this._classToTypeCache["[object " + name + "]"] = name.toLowerCase();
      }
      return this._classToTypeCache;
    },
    getObjectType: function(obj) {
      var strType;

      if (window.jQuery && (window.jQuery.type != null)) {
        return window.jQuery.type(obj);
      }
      strType = Object.prototype.toString.call(obj);
      return this._classToType()[strType] || "object";
    },
    namespace: function(root, namespaceString) {
      var current, parts;

      parts = (namespaceString ? namespaceString.split(".") : []);
      if (!parts.length) {
        return;
      }
      current = parts.shift();
      root[current] = root[current] || {};
      return Utils.namespace(root[current], parts.join("."));
    }
  };

  Utils.namespace(window, "Routes");

  window.Routes = {
// admin_users_user_names => /admin/users/user_names(.:format)
  admin_users_user_names_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"users",false]],[7,"/",false]],[6,"user_names",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// administration_tab => /administration(.:format)
  administration_tab_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"administration",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// assigned_role => /assigned_roles/:id(.:format)
  assigned_role_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"assigned_roles",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// assigned_roles => /assigned_roles(.:format)
  assigned_roles_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"assigned_roles",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// call => /calls/:id(.:format)
  call_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"calls",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// call_note => /call_notes/:id(.:format)
  call_note_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"call_notes",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// call_notes => /call_notes(.:format)
  call_notes_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"call_notes",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// calls => /calls(.:format)
  calls_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"calls",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// calls_download_calls => /calls/download_calls(.:format)
  calls_download_calls_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"calls",false]],[7,"/",false]],[6,"download_calls",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// calls_load_calls => /calls/load_calls(.:format)
  calls_load_calls_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"calls",false]],[7,"/",false]],[6,"load_calls",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// calls_search => /calls/search(.:format)
  calls_search_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"calls",false]],[7,"/",false]],[6,"search",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// cancel_user_registration => /users/cancel(.:format)
  cancel_user_registration_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"users",false]],[7,"/",false]],[6,"cancel",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// dashboard_index => /dashboard(.:format)
  dashboard_index_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"dashboard",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// destroy_user_session => /users/sign_out(.:format)
  destroy_user_session_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"users",false]],[7,"/",false]],[6,"sign_out",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_assigned_role => /assigned_roles/:id/edit(.:format)
  edit_assigned_role_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"assigned_roles",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_call => /calls/:id/edit(.:format)
  edit_call_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"calls",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_call_note => /call_notes/:id/edit(.:format)
  edit_call_note_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"call_notes",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_group => /groups/:id/edit(.:format)
  edit_group_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"groups",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_question => /questions/:id/edit(.:format)
  edit_question_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"questions",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_question_answer => /question_answers/:id/edit(.:format)
  edit_question_answer_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"question_answers",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_question_possible_answer => /question_possible_answers/:id/edit(.:format)
  edit_question_possible_answer_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"question_possible_answers",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_questions_section => /questions_sections/:id/edit(.:format)
  edit_questions_section_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"questions_sections",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_role => /roles/:id/edit(.:format)
  edit_role_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"roles",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_role_access => /role_accesses/:id/edit(.:format)
  edit_role_access_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"role_accesses",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_saved_search_tab => /saved_search_tabs/:id/edit(.:format)
  edit_saved_search_tab_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"saved_search_tabs",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_template => /templates/:id/edit(.:format)
  edit_template_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"templates",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_template_answer => /template_answers/:id/edit(.:format)
  edit_template_answer_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"template_answers",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_template_question => /templates/:template_id/questions/:id/edit(.:format)
  edit_template_question_path: function(_template_id, _id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["template_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"templates",false]],[7,"/",false]],[3,"template_id",false]],[7,"/",false]],[6,"questions",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_user => /admin/users/:id/edit(.:format)
  edit_user_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"users",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_user_password => /users/password/edit(.:format)
  edit_user_password_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"users",false]],[7,"/",false]],[6,"password",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_user_registration => /users/edit(.:format)
  edit_user_registration_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"users",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// group => /groups/:id(.:format)
  group_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"groups",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// group_users => /groups/:group_id/users(.:format)
  group_users_path: function(_group_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["group_id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"groups",false]],[7,"/",false]],[3,"group_id",false]],[7,"/",false]],[6,"users",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// groups => /groups(.:format)
  groups_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"groups",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// home_email_confirmation => /home/email_confirmation(.:format)
  home_email_confirmation_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"home",false]],[7,"/",false]],[6,"email_confirmation",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// import_users => /admin/users/import(.:format)
  import_users_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"users",false]],[7,"/",false]],[6,"import",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_assigned_role => /assigned_roles/new(.:format)
  new_assigned_role_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"assigned_roles",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_call => /calls/new(.:format)
  new_call_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"calls",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_call_note => /call_notes/new(.:format)
  new_call_note_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"call_notes",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_group => /groups/new(.:format)
  new_group_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"groups",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_question => /questions/new(.:format)
  new_question_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"questions",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_question_answer => /question_answers/new(.:format)
  new_question_answer_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"question_answers",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_question_possible_answer => /question_possible_answers/new(.:format)
  new_question_possible_answer_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"question_possible_answers",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_questions_section => /questions_sections/new(.:format)
  new_questions_section_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"questions_sections",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_role => /roles/new(.:format)
  new_role_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"roles",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_role_access => /role_accesses/new(.:format)
  new_role_access_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"role_accesses",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_saved_search_tab => /saved_search_tabs/new(.:format)
  new_saved_search_tab_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"saved_search_tabs",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_template => /templates/new(.:format)
  new_template_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"templates",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_template_answer => /template_answers/new(.:format)
  new_template_answer_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"template_answers",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_template_question => /templates/:template_id/questions/new(.:format)
  new_template_question_path: function(_template_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["template_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"templates",false]],[7,"/",false]],[3,"template_id",false]],[7,"/",false]],[6,"questions",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_user => /admin/users/new(.:format)
  new_user_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"users",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_user_password => /users/password/new(.:format)
  new_user_password_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"users",false]],[7,"/",false]],[6,"password",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_user_registration => /users/sign_up(.:format)
  new_user_registration_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"users",false]],[7,"/",false]],[6,"sign_up",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_user_session => /users/sign_in(.:format)
  new_user_session_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"users",false]],[7,"/",false]],[6,"sign_in",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// permissions => /permissions(.:format)
  permissions_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"permissions",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// personal_tab => /organization(.:format)
  personal_tab_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"organization",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// question => /questions/:id(.:format)
  question_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"questions",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// question_answer => /question_answers/:id(.:format)
  question_answer_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"question_answers",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// question_answers => /question_answers(.:format)
  question_answers_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"question_answers",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// question_possible_answer => /question_possible_answers/:id(.:format)
  question_possible_answer_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"question_possible_answers",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// question_possible_answers => /question_possible_answers(.:format)
  question_possible_answers_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"question_possible_answers",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// questions => /questions(.:format)
  questions_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"questions",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// questions_section => /questions_sections/:id(.:format)
  questions_section_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"questions_sections",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// questions_sections => /questions_sections(.:format)
  questions_sections_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"questions_sections",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// rails_info_properties => /rails/info/properties(.:format)
  rails_info_properties_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"rails",false]],[7,"/",false]],[6,"info",false]],[7,"/",false]],[6,"properties",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// rails_routes => /rails/routes(.:format)
  rails_routes_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"rails",false]],[7,"/",false]],[6,"routes",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// role => /roles/:id(.:format)
  role_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"roles",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// role_access => /role_accesses/:id(.:format)
  role_access_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"role_accesses",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// role_accesses => /role_accesses(.:format)
  role_accesses_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"role_accesses",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// roles => /roles(.:format)
  roles_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"roles",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// root => /
  root_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], [], [7,"/",false], arguments);
  },
// saved_search_tab => /saved_search_tabs/:id(.:format)
  saved_search_tab_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"saved_search_tabs",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// saved_search_tabs => /saved_search_tabs(.:format)
  saved_search_tabs_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"saved_search_tabs",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// search_tab => /search(.:format)
  search_tab_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"search",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// searches => /search(.:format)
  searches_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"search",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// set_permissions_role => /roles/:id/set_permissions(.:format)
  set_permissions_role_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"roles",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"set_permissions",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// settings_tab => /settings(.:format)
  settings_tab_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"settings",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// show_popup_call => /calls/:id/show_popup(.:format)
  show_popup_call_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"calls",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"show_popup",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// status_call => /calls/:id/status(.:format)
  status_call_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"calls",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"status",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// template => /templates/:id(.:format)
  template_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"templates",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// template_answer => /template_answers/:id(.:format)
  template_answer_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"template_answers",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// template_answers => /template_answers(.:format)
  template_answers_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"template_answers",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// template_question => /templates/:template_id/questions/:id(.:format)
  template_question_path: function(_template_id, _id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["template_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"templates",false]],[7,"/",false]],[3,"template_id",false]],[7,"/",false]],[6,"questions",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// template_questions => /templates/:template_id/questions(.:format)
  template_questions_path: function(_template_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["template_id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"templates",false]],[7,"/",false]],[3,"template_id",false]],[7,"/",false]],[6,"questions",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// templates => /templates(.:format)
  templates_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"templates",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// user => /admin/users/:id(.:format)
  user_path: function(_id, options) {
  if (!options){ options = {}; }
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"users",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// user_password => /users/password(.:format)
  user_password_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"users",false]],[7,"/",false]],[6,"password",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// user_registration => /users(.:format)
  user_registration_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"users",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// user_session => /users/sign_in(.:format)
  user_session_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"users",false]],[7,"/",false]],[6,"sign_in",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// users => /admin/users(.:format)
  users_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"users",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// users_current_user_saved_search_tabs => /users/current_user_saved_search_tabs(.:format)
  users_current_user_saved_search_tabs_path: function(options) {
  if (!options){ options = {}; }
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"users",false]],[7,"/",false]],[6,"current_user_saved_search_tabs",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  }}
;

  window.Routes.options = defaults;

}).call(this);
