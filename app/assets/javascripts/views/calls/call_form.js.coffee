class QualityCloud.Views.CallForm extends QualityCloud.Views.BaseView

  el: "#call_form"
  template: JST['calls/call_form']

  events:
    "hidden #call_modal" : 'removeView'

  views_permissions:
    "playback_call": "#playback_div"
    "view_notes": "#call_notes"
    "create_notes": "#call_note_form"

  initialize:()=>
    @render()

  render:=>
    @prepareAttributes()
    @$el.html(@template(call: @model))
    @filterByPermission()
    $("#call_modal").on "hidden", @removeView
    $('video,audio').mediaelementplayer()

  prepareAttributes:=>
    type = switch @model.get('call_type')
      when 0 then 'voice'
      when 1 then 'video'
      when 2 then 'voice_and_video'
      else @model.get('call_type')
    @model.set('call_type', type) 

  removeView:=>
    @undelegateEvents()

  closeModal:=>
    $('#call_modal').hide()
    @removeView()
    @$el.html("")
