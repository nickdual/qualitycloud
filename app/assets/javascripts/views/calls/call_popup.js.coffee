class QualityCloud.Views.CallPopup extends QualityCloud.Views.ModalBaseView

  className: "videoPopup2 video-popup-container popup-z-index"
  # template: JST['calls/call_popup']

  events:
    "click .js-close" : 'close'

  initialize:()=>
    @render()

  render:=>
    # @prepareAttributes()
    # @$el.html(@template(call: @model))
    # @show(false)
    # @$('.js-call-video,.js-call-audio').mediaelementplayer
      # enableAutosize: true

    @player = new MediaElementPlayer(
      @$('.js-call-video,.js-call-audio'), 
      enableAutosize: true
    )
    @$(".mejs-mediaelement").css("z-index", 20100)
    @$(".mejs-controls").css("z-index", 20200)

    # window.player = @player
    # @$el.draggable
    #   handle: ".heading"
    # @$el.resizable
    #   aspectRatio: 600/420

  prepareAttributes:=>
    type = switch @model.get('call_type')
      when 0 then 'voice'
      when 1 then 'video'
      when 2 then 'voice_and_video'
      else @model.get('call_type')
    @model.set('call_type', type) 
