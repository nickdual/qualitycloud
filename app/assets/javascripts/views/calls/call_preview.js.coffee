class QualityCloud.Views.CallPreview extends QualityCloud.Views.BaseView
  
  template: JST["calls/call_preview"]

  events:
    "click #save_call_note" : 'newCallNote'
    "keypress #new_call_note": "commentOnEnter"
    "click .js-popout-video" : "showVideoPopup"

  views_permissions:
    "playback_call": "#playback_div"
    "view_notes": "#call_notes_list"
    "create_notes": "#call_note_form"

  initialize:(options = {})->
    @popup = null
    @render()
    if QualityCloud.current_user.has_permission("view_notes")
      @callNotesCollection = new QualityCloud.Collections.CallNotes(call_id: @model.id)
      @callNotesCollection.fetch
        success:=>
          @renderCollection(@callNotesCollection)

  showVideoPopup:(e)=>
    e.preventDefault()
    @player.pause()
    window.open(Routes.show_popup_call_path(@model), @model.id, 'width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');

  render:=>
    @prepareAttributes()
    @$el.html @template(call: @model)
    @player = new MediaElementPlayer(@$('.js-media-element'), enableAutosize: true)
    window.player = @player
    @filterByPermission()
    @$("#call_stage").resizable
      aspectRatio: 600/420,
      handles: "e"


  prepareAttributes:=>
    type = switch @model.get('call_type')
      when 0 then 'voice'
      when 1 then 'video'
      when 2 then 'voice_and_video'
      else @model.get('call_type')
    @model.set('call_type', type) 

  commentOnEnter:(e) => 
    if e.keyCode == 13
      @newCallNote() 
      return false

  newCallNote:=>
    call_note = new QualityCloud.Models.CallNote()
    attributes = 
      call_id: @model.id
      user_id: QualityCloud.current_user.id
      note: @$('#new_call_note').val()
    call_note.save attributes,
      success:=>
        @$('#call_notes_list').append(new QualityCloud.Views.CallNoteRow(model: call_note).el)
        @$('#new_call_note').val("")
        $("#call_row_#{@model.id}").addClass("with-notes")
        @callNotesCollection.add call_note
    return false

  renderCollection:(collection)=>
    collection.each (callNote)=> 
      @$('#call_notes_list').append(new QualityCloud.Views.CallNoteRow(model: callNote).el)


