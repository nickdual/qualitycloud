class QualityCloud.Views.CallRow extends QualityCloud.Views.BaseView

  template: JST['calls/call_row']
  tagName: "tr"

  events:->
    events_h = 
      "click .close": "closeCall"
    events_h["click .js-clickable"] = "triggerPreview" if QualityCloud.current_user.has_permission("view_notes") or QualityCloud.current_user.has_permission("playback_call")
    return events_h

  views_permissions:
    "delete_calls": ".call-checkbox"

  initialize:(options)->
    @render()
    @model.on "updateSuccess", @render, this
    @model.on "destroy", @remove, this

  render:=>
    @prepareAttributes()
    @$el.html(@template(call: @model))
    @filterByPermission()
    if @model.get('call_notes')? && @model.get('call_notes').length > 0
      @$el.addClass("with-notes")

    @$el.attr("id", "call_row_#{@model.id}")

    QualityCloud.Helpers.TableHelpers.arrangeTableRow(@$el, QualityCloud.current_user.get("settings").calls_table_columns)

  prepareAttributes:=>
    if @model.get('user')?
      agent_name = if @model.get('user')? then "#{@model.get('user').first_name} #{@model.get('user').last_name}" else 'None'
      @model.set('agent_name', agent_name)
      group_name = if @model.get('user').group? then @model.get('user').group.name else 'None'
      @model.set('agent_group', group_name)
      @model.set('agent_id', @model.get('user').agent_id)
    if @model.get('media_type')?
      switch @model.get('media_type')
        when 1 then @model.set('type_img', "images/video.png")
        when 2 then @model.set('type_img', "images/tel-img.png")
        when 3 then @model.set('type_img', "images/video-tel.png")
        else @model.set('type_img', "images/cancle-btn2.jpg")
    d = new Date(@model.get('audio_recording_time'))
    d = d.toString("MM/dd/yyyy HH:mm:ss")
    @model.set('datetime', d)

  triggerPreview:=>
    @options.calls_index_dispatcher.trigger "call:preview", @model.id
    $("#download_record a").attr('href',"/calls/#{@model.id}/stream_video")

  deleteCall:=>
    return unless confirm('Are you sure?')
    @model.destroy
      success:()=>
        @unbind() # Unbind all local event bindings
        @remove()

  closeCall:=>
    $('#call_modal').hide()
    @call_form.closeModal()
