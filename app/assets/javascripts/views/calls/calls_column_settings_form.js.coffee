class QualityCloud.Views.CallsColumnSettingsForm extends QualityCloud.Views.ModalBaseView

  className: "columnSelector"
    
  events:
    "click .js-submit": "submit"
    'click .js-close': 'close'
    "click .js-move-up": "moveColumnUp"
    "click .js-move-down": "moveColumnDown"

  template: JST["calls/calls_column_settings_form"]

  initialize:(options)->
    @columns_list = options.columns_list
    @calls_index_dispatcher = options.calls_index_dispatcher
    @columns_names = options.columns_names
    @render()

  render:=>
    users_columns = QualityCloud.current_user.get("settings").calls_table_columns

    #don't show checkbox column in modify columns popup
    users_columns = _.reject users_columns, (col)-> col.id is "checkbox"

    @$el.html(@template(
      calls_table_columns: users_columns
      columns_names: @columns_names
      ))
    @$el.draggable
      handle: ".heading"

    @$("#js_calls_per_page").val(QualityCloud.current_user.get("settings").calls_table_per_page||@$("#js_calls_per_page option:first").val())
    @show()

  submit:=>
    settings = QualityCloud.current_user.get("settings")
    settings.calls_table_columns = @_getColumnsHash()
    settings.calls_table_per_page = @$("#js_calls_per_page").val()

    QualityCloud.current_user.save {settings: settings},
      success:(user)=>
        @close()
        user.trigger "change:settings", user
        @calls_index_dispatcher.trigger "columns:changed"

    #TODO: continue saving object then continue with populating this after saving
    #also populate when saving table

  _getColumnsHash:=>
    columns_arr =  _.map @$("#js_columns_settings input"), (el) ->
      return {
        id: $(el).attr("value")
        isVisible: $(el).is(":checked")
      }
    columns_arr.unshift({id:"checkbox", isVisible: true})
    return columns_arr

  moveColumnUp:(e)=>
    length = $("#js_columns_settings .js-column-row").length
    index = $(e.currentTarget).closest(".js-column-row").index("#js_columns_settings .js-column-row")
    if 0 < index < length
      $("#js_columns_settings .js-column-row").eq(index-1).insertAfter $(e.currentTarget).closest ".js-column-row"

  moveColumnDown:(e)=>
    length = $("#js_columns_settings .js-column-row").length
    index = $(e.currentTarget).closest(".js-column-row").index("#js_columns_settings .js-column-row")
    if -1 < index < length-2
      $(e.currentTarget).closest(".js-column-row").insertAfter $("#js_columns_settings .js-column-row").eq(index+1)
