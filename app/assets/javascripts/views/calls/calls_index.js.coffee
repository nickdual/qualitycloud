class QualityCloud.Views.CallsIndex extends Backbone.View

  template: JST['calls/index']
  events:
    "click #filter_calls_btn" : 'filterCalls'
    'keypress #filter_calls': 'filterOnEnter'

  initialize:()=>
    # fetch new call form popup from server
    # new QualityCloud.Views.CallForm()
    @collection = new QualityCloud.Collections.Calls()
    @collection.on 'reset', @renderCollection, this
    @render()
    @populateCollection()

  render:=>
    @$el.html(@template())

  renderCollection:(collection)=>
    collection.each (call)=> 
      @$('tbody').append(new QualityCloud.Views.CallRow(model: call).el)

  populateCollection:=>
    @collection.fetch
      success:=>
        @renderCollection(@collection)

  filterCalls:=>
    @collection.fetch
      data:
        filter_calls: $("#filter_calls").val()
      success:=>
        @$('tbody').html("")
        @renderCollection(@collection)

  filterOnEnter:(e)=>
    @filterCalls() if e.keyCode == 13