class QualityCloud.Views.SearchResult extends QualityCloud.Views.BaseView

  indexTemplate: JST['calls/index']
  formTemplate: JST['calls/search_form']
  calls_table_template: JST['calls/calls_table']

  events:
    "click #filter_calls_btn" : 'filterCalls'
    'keypress #filter_search': 'filterOnEnter'
    "click #export_as_csv" : 'exportResultCSV'
    "click #modify_columns" : "modifyColumns"
    "click #load_calls" : "showLoadCalls"
    "click .js-order" : "orderBy"
    "click #delete_calls_btn" : "deleteMarkedCalls"
    "click #download_calls_btn" : "downloadMarkedCalls"
    "click #js_pagination_first": "fetchFirstPage"
    "click #js_pagination_last": "fetchLastPage"
    "click #js_pagination_next": "fetchNextPage"
    "click #js_pagination_prev": "fetchPrevPage"
    "click .js_pagination_page": "fetchPage"
    "keyup #current": "fetchCurrentPage"

  columns_names:
    "calls_call_guid": "Call GUID"
    "calls_type_img": "Call type"
    "calls_audio_recording_time": "Date/Time"
    "calls_call_duration": "Duration"
    "users_agent_id": "Agent ID"
    "users_full_name": "Agent Name"
    "groups_name": "Agent Group"
    "calls_call_direction": "Call direction"
    "calls_dnis": "VDN/DNIS"
    "calls_dialed_number": "Digits Dialed"
    "calls_caller_id": "Caller ID"

  views_permissions:
    "export_csv": "#export_as_csv"
    "load_calls": "#load_calls"
    "delete_calls"  :  "#delete_calls_btn"
    "modify_call_columns": "#modify_columns"
    
  initialize:(options)=>
    @groups = options.groups
    @users = options.users
    @view_id = options.id
    @saved = options.saved
    @name = options.name
    @search_params = options.search_params
    if @search_params.order_by?
      @orderByValue = @search_params.order_by #"#{@orderAttr} #{@orderDir}"
      @orderAttr = @orderByValue.split(" ")[0]
      @orderDir = @orderByValue.split(" ")[1]
    if @search_params.filter_calls?
      @$("#filter_search").val(@filterCalls)

    @collection = new QualityCloud.Collections.Calls(paginated: true)

    $(".right-content-search").css("background-image", "url(../images/Watermark.png)")
    @calls_index_dispatcher = _.clone(Backbone.Events)
    @calls_index_dispatcher.on "columns:changed", @renderUpdatedColumns, this
    @calls_index_dispatcher.on "call:preview", @previewCall, this
    @_syncColumnsList()

  fetchFirstPage:=>
    @loadPage(1)

  fetchLastPage:=>
    @loadPage(@collection.end_entry || 1)

  fetchNextPage:=>
    @populateCollection()

  fetchPrevPage:=>
    @populateCollection(false)

  fetchCurrentPage:(e)=>
    if e.keyCode == 13
      page = @$(e.currentTarget).val()
      @loadPage(page)
      
  fetchPage:(e)=>
    page = @$(e.currentTarget).data("page")
    @loadPage(page)

  populateCollection:(forward=true, fetch_options={})=>
    params = 
      data:
        @search_params
      success:=>
        @renderCollection(@collection)
    $.extend params, fetch_options
    if forward then @collection.nextPage(params) else @collection.previousPage(params)

  loadPage:(page, fetch_options={})=>
    params=
      data:
        @search_params
      success:(collection, response, options)=>
        @renderCollection(@collection)
    $.extend params, fetch_options
    @collection.loadPage page, params

  renderCollection:(collection)=>
    @reRenderTable()
    @$('tbody#calls_body').html("")
    collection.each (call)=> 
      @$('tbody#calls_body').append(new QualityCloud.Views.CallRow(model: call, calls_index_dispatcher: @calls_index_dispatcher).el)

    QualityCloud.Helpers.TableHelpers._renderPagination(collection, this)
    $(".right-content-search").css("background-image", "none")
    ex = @$('#calls_table')[0]

    unless $.fn.DataTable.fnIsDataTable( ex ) and @rendered
      @rendered= true
      oTable = @$('#calls_table').dataTable
        "sDom": 'Rt'
        "bInfo": false
        "sScrollY": @table_size||"530px"
        "bSort": false
        "bPaginate": false

    if collection.length == 0
      @$('tbody#calls_body').html('<tr class="odd"><td valign="top" colspan="30" class="dataTables_empty">No data available in table</td></tr>')

    @renderOrderState()

  render:(dontPopulate)=>
    @$el.html(@indexTemplate(columns_names: @columns_names))
    QualityCloud.Helpers.TableHelpers.arrangeTableHeader(@$("#calls_table"), 
      QualityCloud.current_user.get("settings").calls_table_columns)

    # @$(".js-order").click @orderBy
    if dontPopulate then $(".right-content-search").css("background-image", "url(../images/Watermark.png)") else @loadPage(1)

    @attachManageTabEvents()
    @changed = false # indicates if search params have changed after last search
    @enableSavingPin() unless @saved # means new tab, so it can be saved

    $("#below_side_bar").html(@formTemplate(users: @users, groups: @groups, search_params: @search_params))
    @activateUserSearch()
    @activateDatepicker()
    @renderOrderState()
    @$el.show()
    @$("#filter_search").val(@search_params.filter_calls) if @search_params.filter_calls?
    @attachEvents()
    @filterByPermission()

  renderUpdatedColumns:=>
    @render()
    @renderCollection(@collection)

  exportResultCSV:=>
    $("#below_side_bar form").submit()

  submitSearch:(filterCalls)=>
    @prepareParams(filterCalls)
    @loadPage 1,
      data:
        @search_params
      reset: true if filterCalls == "and order"
      success:(collection, response, options)=>
        @$el.find('tbody#calls_body').html("")
        if filterCalls == "and order"
          @collection.reset()
          collection.currentPage = 1
          @collection.add(new QualityCloud.Models.User(userAttr)) for userAttr in response
        @renderCollection(@collection)
        @enableSavingPin() if @changed # means that tab isn't saved yet

    return false

  filterCalls:=>
    @submitSearch("filterCalls")

  filterOnEnter:(e)=>
    @filterCalls() if e.keyCode == 13

  orderBy:(e)=>
    @$(".js-order").removeClass("drop").removeClass("drop-up").addClass("drop-sortable")
    
    @orderDir = $(e.target).data("order_dir")
    @orderAttr = $(e.target).data("order_by")
    @orderByValue = "#{@orderAttr} #{@orderDir}"
    
    @renderOrderState()
    @submitSearch("and order")

  renderOrderState:()=>
    header = @$("th[data-order_by='#{@orderAttr}']")
    
    header.removeClass("drop").removeClass("drop-up").removeClass("drop-sortable")
    
    if @orderDir is 'asc'
      header.data("order_dir", 'desc')
      header.addClass('drop')
    else
      header.data("order_dir", 'asc')
      header.addClass('drop-up')

  hide:()=>
    @$el.hide()

  show:(dontPopulate)=>
    @render(dontPopulate)

  attachEvents:=>
    # disable submitting on enter key
    keyHandler = (e) => 
      if e.keyCode == 13
        @submitSearch() 
        return false
    $("#below_side_bar form input").bind("keypress", keyHandler)
    $("#search_button").click(@submitSearch)
    $("#clear_button").click(@clearForm)

  activateUserSearch:=>
    $('.chzn-select').chosen
      allow_single_deselect: true
      no_results_text: 'No results matched'
      width: '166px'

  activateDatepicker:=>
    return if $( "input.datepicker" ).hasClass('datepickered')
    # $( "input.datepicker" ).datepicker()
    $("input.datepicker").datetimepicker
      dateFormat: "mm/dd/yy"
      timeFormat: "HH:mm:ss"
    $( "input.datepicker" ).addClass('datepickered')

  clearForm:=>
    $("form input").val("")
    $("form option:selected").removeAttr("selected")
    $(".chzn-select").trigger("liszt:updated")
    @$('tbody#calls_body').html("")
    $(".right-content-search").css("background-image", "url(../images/Watermark.png)")
    today = new Date().toString('MM/dd/yyyy')
    $("#below_side_bar .js-from-date").val( today + " 00:00:01" )
    $("#below_side_bar .js-to-date").val( today + " 23:59:59" )

  prepareParams:(filterCalls)=>
    old_search = @search_params
    form = $("#below_side_bar form")
    @search_params = form.serializeObject()
    @search_params['filter_calls'] = @$("#filter_search").val() # if filterCalls
    @search_params['order_by'] = @orderByValue if @orderByValue?
    @search_params['group_id'] = ""  if !@search_params['group_id']
    @search_params['user_id'] = ""  if !@search_params['user_id']
    @search_params['agent_id'] = ""  if !@search_params['agent_id']
    @search_params['record_type'] = ""  if !@search_params['record_type']
    if @search_params['from']
      d = new Date(@search_params['from'])
      month = d.getUTCMonth() + 1;
      day = d.getUTCDate();
      year = d.getUTCFullYear();
      @search_params['from'] = "" + month + "/" + day + "/" + year + " " + (d.getUTCHours()) + ":" + (d.getUTCMinutes()) + ":" + (d.getUTCSeconds());
      # fix date format
      @search_params["from"] = new Date(@search_params["from"]).toString('MM/dd/yyyy HH:mm:ss')
    if @search_params['to']
      d = new Date(@search_params['to'])
      month = d.getUTCMonth() + 1;
      day = d.getUTCDate();
      year = d.getUTCFullYear();
      @search_params['to'] = "" + month + "/" + day + "/" + year + " " + (d.getUTCHours()) + ":" + (d.getUTCMinutes()) + ":" + (d.getUTCSeconds());
      # fix date format
      @search_params["to"] = new Date(@search_params["to"]).toString('MM/dd/yyyy HH:mm:ss')
    @changed = true unless JSON.stringify(old_search) == JSON.stringify(@search_params) 

  submitSaveResult:=>
    user_id = QualityCloud.current_user.id
    tab_name = $("##{@view_id}_input").val()
    attributes = {user_id: user_id, name: tab_name, search_params: JSON.stringify(@search_params)}
    @model.save attributes,
      success:=>
        @saved = true
        @name = tab_name
        @changed = false
        @afterSaveResult()

  renameResult:(e)=>
    if $(".nav-new-menu.active").attr('id') ==  @view_id
      $("##{@view_id} a").contents().filter(->@nodeType is 3).replaceWith ""
      $("##{@view_id} div.tab-name").show()
      $("##{@view_id} div.pin").show()
      $("##{@view_id}_input").focus()
      $("div.ui-tooltip.ui-widget:contains('Click to rename tab')").remove()
      @enableSavingPin()
      # to prevent click handler of the whole tab
      e = window.event  unless e
      e.cancelBubble = true
      e.stopPropagation()  if e.stopPropagation

  afterSaveResult:()=>
    $("##{@view_id} div.tab-name").before(@name) if $("##{@view_id} div.tab-name").is(":visible")
    $("##{@view_id} div.tab-name").hide()
    @disableSavingPin() unless @changed

  tabNameKeyHandler:(e)=>
    @submitSaveResult() if e.keyCode == 13
    @afterSaveResult() if e.keyCode == 27

  pinClick:(e)=>
    if $("##{@view_id} div.pin").hasClass('pin-active')
      @submitSaveResult()
      # to prevent click handler of the whole tab
      e = window.event  unless e
      e.cancelBubble = true
      e.stopPropagation()  if e.stopPropagation

  enableSavingPin:()=>
    $("##{@view_id} div.pin").addClass('pin-active')
    $("##{@view_id} div.pin").attr('title', "Click to save this tab")
    $("##{@view_id} div.pin").css('cursor', 'pointer')

  disableSavingPin:()=>
    if @saved
      $("##{@view_id} div.pin").removeClass('pin-active')
      $("##{@view_id} div.pin").removeAttr('title')
      $("##{@view_id} div.pin").css('cursor', 'normal')
    else
      @enableSavingPin()

  attachManageTabEvents:()=>
    $("##{@view_id} span").off('click').on('click', @deleteResult)
    if QualityCloud.current_user.has_permission "manage_search_tabs"
      $("##{@view_id} a").click @renameResult
      $("##{@view_id} div.pin").show() if @saved
      $("##{@view_id} div.pin").click(@pinClick)
      $("##{@view_id}_input").bind("keyup", @tabNameKeyHandler)
    else
      $("##{@view_id} div.pin").remove()

  deleteResult:=>
    return unless confirm("Are you sure you want to remove '#{@name}' ?")
    if @saved
      @model.destroy
        success:()=>
          @deleteView()
    else
      @deleteView()

  deleteView:=>
    $("##{@view_id}").remove()
    @trigger "searchResult:closed"
    @unbind() # Unbind all local event bindings
    @remove()

  modifyColumns:=>
    new QualityCloud.Views.CallsColumnSettingsForm
      calls_index_dispatcher: @calls_index_dispatcher
      columns_names: @columns_names
    $('.columnSelector').css('position','absolute')
      

  showLoadCalls:=>
    @_showOverlay()
    $("#load_calls_popup").show()

  #sync in case new fields added and not saved to user settings yet
  _syncColumnsList:()=>
    user_columns = QualityCloud.current_user.get("settings").calls_table_columns
    user_columns = _.reject(user_columns, (c)=>
        return !_.has(@columns_names, c.id)
      )

    user_columns_ids = _.pluck(user_columns, "id")
    user_columns.push({id:column_id, isVisible:true}) for column_id of @columns_names when column_id not in user_columns_ids

    QualityCloud.current_user.get("settings").calls_table_columns = user_columns

  reRenderTable:=>
    @$(".personal-users").html @calls_table_template(columns_names: @columns_names)
    QualityCloud.Helpers.TableHelpers.arrangeTableHeader(@$("#calls_table"), 
      QualityCloud.current_user.get("settings").calls_table_columns)
    
  previewCall:(call_id)=>
    call = @collection.get(call_id)
    #shoren table
    @reRenderTable()
    @table_size = 300
    @renderCollection @collection
    # $('.dataTables_scrollBody').css('height', 300)
    #create div element
    temp_div = $("<div></div>")
    @$("#js_video_preview").append temp_div
    #remove old view if exists
    @call_preview?.remove()
    #create the new view
    @call_preview = new QualityCloud.Views.CallPreview(model: call, el: temp_div)


  deleteMarkedCalls:=>
    no_of_calls = @$(".call-checkbox:checked").length
    if no_of_calls > 0
      return unless confirm("Are you sure to delete #{no_of_calls} calls?")
      @$(".call-checkbox:checked").each (i, domElement)=>
        id = $(domElement).attr "call_id"
        call = @collection.get id
        call.destroy
          wait: true

  downloadMarkedCalls:=>
    no_of_calls = @$(".call-checkbox:checked").length
    if no_of_calls > 0
      return unless confirm("Are you sure to download #{no_of_calls} calls?")
      call_ids = []
      params = ""
      id = 0
      @$(".call-checkbox:checked").each (i, domElement)=>
        id = $(domElement).attr "call_id"
        call_ids.push id
        params += "call_ids[]=#{id}&"
      window.location = "calls/download_calls?#{params}"
    else
      alert("Please, select the calls you want to download first.")

  _showOverlay:=>
    @overlay = $("<div id='black_overlay' class='black_overlay'></div>")
    @overlay.show()
    $("body").append(@overlay)
    @$el.css("z-index", @overlay.css("z-index"))

  _removeOverlay:=>
    @overlay.remove()







