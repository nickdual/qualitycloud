class QualityCloud.Views.DeleteGroupPopup extends QualityCloud.Views.ModalBaseView
  className: "columnSelector"

  events:
    "click #js-submit": "submitDelete"
    "click .js-close": "close"

  template: JST["groups/delete_group_popup"]

  initialize:(options)->
    @all_groups = options.groups
    @available_groups = @all_groups.without @model
    @render()

  render:=>
    @$el.html @template(groups: @available_groups)
    @show()

  submitDelete:=>
    new_group_id = $("#js_new_group").val()
    @model.destroy 
      data:
        new_group_id: new_group_id
      processData: true
      success:=> 
        @all_groups.remove(@model)
        @close()
