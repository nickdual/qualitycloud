class QualityCloud.Views.GroupsIndex extends QualityCloud.Views.BaseView

  template: JST['groups/index']
  events:
    'change #select_group'        : 'groupSelected'
    'click #assign_all_to_group'  : 'assignAllToGroup'
    'click #assign_one_to_group'  : 'assignOneToGroup'
    'click #remove_one_from_group': 'removeOneFromGroup'
    'click #remove_all_from_group': 'removeAllFromGroup'
    "click #create_group" : 'createNewGroup'
    "click #edit_group" : 'editGroup'
    "click #delete_group" : 'deleteGroup'
  
  views_permissions:
    "create_groups":  "#create_group, #new_group_modal"
    "modify_groups":  "#edit_group, #edit_group_modal"
    "delete_groups":  "#delete_group"

  initialize:()=>
    @dispatcher = _.clone(Backbone.Events)
    @dispatcher.on "renderGroup", @renderGroup, this
    @collection = new QualityCloud.Collections.Groups()
    @collection.on "remove", @groupRemoved, this
    @render()
    @collection.fetch
      success:=>
        @renderCollection()
    @roles = new QualityCloud.Collections.Roles()
    @roles.fetch()
 
  render:=>
    @$el.html(@template())
    @filterByPermission()

  renderCollection:=>
    @$('#select_group').html("<option value='-1'>Select Group</option>")
    @collection.each (group)=> 
      @$('#select_group').append(JST['groups/group_record_dropdown'](group:group))

  renderGroup:=>
    @$('#available_users_list').html("")
    @$("#assigned_users_list").html("")
    @$('#group_roles').text("")

    return if @group_id is "-1"
    @assigned_group_roles = new QualityCloud.Collections.AssignedRoles()
    @assigned_group_roles.fetch 
      data: 
        roleable_id: @group_id
        roleable_type: "Group"
      success:=>
        roles_names = (@roles.get(role_id)?.get("name") for role_id in @assigned_group_roles.pluck("role_id"))
        @$('#group_roles').text(roles_names.join(", "))
    @users = new QualityCloud.Collections.GroupUsers(group_id: @group_id)
    @users.fetch
      success:=>
        @users.each (user)=>
          if user.id?
            @$('#assigned_users_list').append(@_userSelectRow(user))
    
    @non_users = new QualityCloud.Collections.NonGroupUsers(group_id: @group_id)
    @non_users.reset()
    @non_users.fetch
      success:=>
        @non_users.each (user)=>
          if user.id? and not user.get("group_id")?
            @$('#available_users_list').append(@_userSelectRow(user))
    
  groupSelected:(e)=>
    @group_id = $(e.currentTarget).val()
    @renderGroup()
  
  assignAllToGroup:=>
    @$("#available_users_list > option").each (index, option)=>
      user_id = $(option).val()
      @_assignUserToGroup(user_id,  @group_id)

  assignOneToGroup:(e)=>
    users_ids = @$("#available_users_list").val()
    @$("#available_users_list").val([])
    _.each users_ids, (user_id)=>
      @_assignUserToGroup(user_id, @group_id)

  removeOneFromGroup:=>
    users_ids = @$("#assigned_users_list").val()
    @$("#assigned_users_list").val([])
    _.each users_ids, (user_id)=>
      @_removeUserFromGroup(user_id, @group_id)

  removeAllFromGroup:=>
    @$("#assigned_users_list > option").each (index, option)=>
      user_id = $(option).val()
      @_removeUserFromGroup(user_id,  @group_id)

  _removeUserFromGroup:(user_id, group_id)=>
    user = @users.get(user_id)
    attributes = {group_id: null}
    user.save attributes,
      success:(user)=>
        @users.remove user
        @non_users.add user
        @_moveUserToList(user, "#assigned_users_list", "#available_users_list")

  _assignUserToGroup:(user_id, group_id)=>
    user = @non_users.get(user_id)
    attributes = {group_id: group_id}
    user.save  attributes,
      success:(user)=>
        @non_users.remove user
        @users.add user
        @_moveUserToList(user, "#available_users_list", "#assigned_users_list")

  _moveUserToList:(user, from_list_id, to_list_id)=>
    $(to_list_id).append $("#{from_list_id} > option[value=#{user.id}]")

  createNewGroup:=>
    @newGroupForm ||= new QualityCloud.Views.GroupForm( collection: @collection, dispatcher: @dispatcher )
    # activate new group popup
    @$('#new_group_modal').show()

  editGroup:=>
    model_id = @$('#select_group option:selected').val()
    return alert('Invalid action!') if model_id == "-1"
    @edit_group_form = new QualityCloud.Views.GroupForm(collection: @collection, el: '#edit_group_modal', model_id: model_id, dispatcher: @dispatcher)
    $('#edit_group_modal').show()

  deleteGroup:=>    
    model_id = @$('#select_group option:selected').val()
    return alert('Invalid action!') if model_id == "-1"
    model = @collection.get(model_id)

    return alert("Sorry! You can't delete default group '#{model.get('name')}' group!") if QualityCloud.Constants.DEFAULT_GROUP == model.get('name')
    return alert("Sorry! You can't delete admin group '#{model.get('name')}' group!") if QualityCloud.Constants.SUPER_GROUP == model.get('name')
    new QualityCloud.Views.DeleteGroupPopup(groups: @collection, model: model)

  groupRemoved:(model, collection, options = {})=>
    @$("#select_group option[value=#{model.id}]").remove()
    @$("#select_group").change()

  _userSelectRow:(user)=>
    $("<option value=\"#{user.id}\" class=\"list-white list-white-group fl\">#{user.get("full_name")}</option>")






