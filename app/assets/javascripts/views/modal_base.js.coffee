class QualityCloud.Views.ModalBaseView extends QualityCloud.Views.BaseView

  events:
    'click .close': 'close'

  close: (e) =>
    @$el.remove()
    @overlay?.remove()


  show:(showOverlay = true)=>
    if showOverlay
      @overlay = $("<div id='black_overlay' class='black_overlay'></div>")
      $("body").append(@overlay)
      @overlay.show()
      @$el.css("z-index", @overlay.css("z-index")+4)

    $("body").append(@el)
    @$el.show()