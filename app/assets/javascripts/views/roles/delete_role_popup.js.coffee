class QualityCloud.Views.DeleteRolePopup extends QualityCloud.Views.ModalBaseView
  className: "columnSelector"

  events:
    "click #js-submit": "submitDelete"
    "click .js-close": "close"

  template: JST["roles/delete_role_popup"]

  initialize:(options)->
    @all_roles = options.roles
    @available_roles = @all_roles.without @model
    @render()

  render:=>
    @$el.html @template(roles: @available_roles)
    @show()

  submitDelete:=>
    new_role_id = $("#js_new_role").val()
    @model.destroy 
      data:
        new_role_id: new_role_id
      processData: true
      success:=> 
        @all_roles.remove(@model)
        @close()
