class QualityCloud.Views.RolesIndex extends QualityCloud.Views.ModalBaseView

  template: JST['roles/index']
  events:
    'change #select_role'        : 'renderRole'
    "click #create_role" : 'createNewRole'
    "click #edit_role" : 'editRole'
    "click #delete_role" : 'deleteRole'

    'click #assign_all_permissions'  : 'assignAllPermissions'
    'click #assign_selected_permissions'  : 'assignSelectedPermissions'
    'click #remove_all_permissions': 'removeAllPermissions'
    'click #remove_selected_permissions': 'removeSelectedPermissions'

    'click #assign_all_groups'  : 'assignAllGroups'
    'click #assign_selected_groups'  : 'assignSelectedGroups'
    'click #remove_selected_groups': 'removeSelectedGroups'
    'click #remove_all_groups': 'removeAllGroups'

    'click #assign_all_users'  : 'assignAllUsers'
    'click #assign_selected_users'  : 'assignSelectedUsers'
    'click #remove_selected_users': 'removeSelectedUsers'
    'click #remove_all_users': 'removeAllUsers'
    'click .js-user-el': 'userSelected'
    'change .js-inherit-group-access':'changeUserAccessInherit'

  views_permissions:
    "assign_role_permission": "#js_permissions_btns"
    "add_roles":  "#create_role,#new_role_modal"
    "modify_roles":  "#edit_role,#edit_role_modal"
    "delete_roles":  "#delete_role"
    "assign_user_roles": "#js_assign_users_btn"
    "assign_group_roles": "#js_assign_groups_btn"

  initialize:->
    @render()

    @collection = new QualityCloud.Collections.Roles()
    @collection.on "remove", @roleRemoved, this
    @collection.fetch
      success:=>
        @renderRolesCollection()
    $.get "/permissions",
      dataType: 'json',
      (@permissions) =>
    @groups = new QualityCloud.Collections.Groups()
    @groups.fetch
      success:=>
        @renderGroups()
    @users = new QualityCloud.Collections.Users()
    @users.fetch()
    @role_accesses = new QualityCloud.Collections.RoleAccesses()

  render:=>
    @$el.html(@template())
    @filterByPermission()

  renderRole:(e)=>
    @$("#assigned_groups_list, #available_groups_list, #assigned_permission_list, #available_permission_list, #available_users_list, #assigned_users_list").html("")

    role_id = $(e.currentTarget).val()
    @model = @collection.get(role_id)
    return unless @model

    @renderPermissions()
    @role_accesses.fetch
      data:
        role_id: @model.id
      success:=>
        @renderGroups()
        @renderUsers()

  changeUserAccessInherit:(e)=>
    inherit_group_access =  @$(e.currentTarget).is(":checked")
    user_id = $(e.currentTarget).closest(".js-user-el").attr("value")
    user = @users.get user_id
    user.save {inherit_group_access: inherit_group_access}

  userSelected:(e)=>
    return if @$(e.currentTarget).find(".js-inherit-group-access").is(":checked")
    if e.metaKey
      $(e.currentTarget).toggleClass("selected") 
    else
      @$(".js-user-el").removeClass("selected")
      @$(e.currentTarget).addClass("selected")

  renderUsers:=>
    @$("#available_users_list, #assigned_users_list").html("")
    assigned_users_ids = (role_access.get("accessable_id") for role_access in @role_accesses.where(accessable_type: "User"))
    assigned_groups_ids = (role_access.get("accessable_id") for role_access in @role_accesses.where(accessable_type: "Group"))
    if @users and @model
      @users.each (user)=>
        option_element = $("<div class='list-white js-user-el fl' value=\"#{user.id}\">#{user.get("full_name")}<input type='checkbox' title='inherit group access' class='js-inherit-group-access inherit-group-access-checkbox' #{"checked" if user.get("inherit_group_access")}></input></div>")
        if (!user.get("inherit_group_access") and user.id in assigned_users_ids) or (user.get("inherit_group_access") and user.get("group_id") in assigned_groups_ids)
          @$("#assigned_users_list").append option_element
        else
          @$("#available_users_list").append option_element

  renderGroups:=>
    assigned_groups_ids = (role_access.get("accessable_id") for role_access in @role_accesses.where(accessable_type: "Group"))
    if @groups and @model
      @groups.each (group)=>
        option_element = $("<option class='list-white list-white-group fl' value=\"#{group.id}\">#{group.get("name")}</option>")
        if group.id in assigned_groups_ids
          @$("#assigned_groups_list").append option_element
        else
          @$("#available_groups_list").append option_element


  renderPermissions:=>
    @$("#assigned_permission_list, #available_permission_list").html("")
    for group, permissions of @permissions
      available_opt_group = $("<optgroup></optgroup>").attr("label", group)
      assigned_opt_group = $("<optgroup></optgroup>").attr("label", group)

      _.each permissions, (permission)=>
        option_element = $("<option class=\"fl\" value=\"#{permission.id}\">#{permission.name}</option>")
        if permission.id in @model.get("permissions")
          assigned_opt_group.append option_element
        else
          available_opt_group.append option_element
      @$("#available_permission_list").append available_opt_group
      @$("#assigned_permission_list").append assigned_opt_group


  _roleSelectRow:(role)=>
    $("<option class='fl' value=\"#{role.id}\">#{role.get("name")}</option>")

  renderRolesCollection:(collection)=>
    @$('#select_role').html("<option value='-1'>Select Role</option>")
    @collection.each (role)=> 
      @$('#select_role').append(JST['roles/role_record_dropdown'](role:role))
    
  createNewRole:=>
    @newRoleForm = new QualityCloud.Views.RoleForm( collection: @collection )
    # activate new role popup
    @$('#new_role_modal').show()

  editRole:=>
    model_id = @$('#select_role option:selected').val()
    return alert('Invalid action!') if model_id == "-1"
    @edit_role_form = new QualityCloud.Views.RoleForm(collection: @collection, el: '#edit_role_modal', model_id: model_id)
    $('#edit_role_modal').show()

  deleteRole:=>
    model_id = @$('#select_role option:selected').val()
    return alert('Invalid action!') if model_id == "-1"
    model = @collection.get(model_id)

    return alert("Sorry! You can't delete '#{model.get('name')}' role!") if QualityCloud.Constants.DEFAULT_ROLE == model.get('name')
    return alert("Sorry! You can't delete '#{model.get('name')}' role!") if QualityCloud.Constants.SUPER_ROLE == model.get('name')
    new QualityCloud.Views.DeleteRolePopup(roles: @collection, model: model)
    
  roleRemoved:(model, collection, options = {})=>
    @$("#select_role option[value=#{model.id}]").remove()
    @$("#select_role").change()

  assignAllPermissions:=>
    permissions = @model.get("permissions")
    @$("#available_permission_list option").each (index, option)=>
      permissions.push $(option).val()
    @model.save "permissions", permissions,
      success:=>
        @renderPermissions()
      url: Routes.set_permissions_role_path(@model)

  assignSelectedPermissions:=>
    new_permissions = @$("#available_permission_list").val()
    @$("#available_permission_list").val([])
    new_permissions = @_validatePermissionsDependency(new_permissions, @model.get("permissions"))
    permissions = @model.get("permissions").concat new_permissions

    @model.save "permissions", permissions,
      success:=>
        @renderPermissions()
      url: Routes.set_permissions_role_path(@model)

  _validatePermissionsDependency:(new_permissions, current_permissions)=>
    permissions = current_permissions.concat new_permissions
    for new_permission in new_permissions
      dependent_permission = @_getParentPermissions(new_permission)
      if dependent_permission? and dependent_permission not in permissions
        new_permissions = new_permissions.filter (permission) -> permission isnt new_permission
        alert("Permission (#{@_getPermissionName new_permission}) needs permission (#{@_getPermissionName dependent_permission})")
    return new_permissions

  _validateRemovePermission:(removed_permissions, current_permissions)=>
    temp = removed_permissions.reverse()#do this because of chained dependencies
    for permission in temp
      child_permissions = @_getChildPermissions(permission)
      #check that child permission is removed with parent
      dependant_permissions = []
      for child_permission in child_permissions
        if child_permission in removed_permissions
          continue
        else if child_permission in current_permissions
          dependant_permissions.push child_permission
      if dependant_permissions.length > 0
        permissions_names = (@_getPermissionName p for p in dependant_permissions)
        alert("permissions (#{permissions_names}) depend on the permission (#{@_getPermissionName permission})")
        removed_permissions = removed_permissions.filter (a_permission) -> 
          return a_permission isnt permission
    return removed_permissions

  _getPermissionName:(permission)=>
    permissions = _.flatten (_.map @permissions, (permissions_arr, group)-> return permissions_arr)
    val =  _.find permissions,(permission_hash)-> permission_hash.id is permission
    return val?.name

  _getParentPermissions:(permission)=>
    permissions = _.flatten (_.map @permissions, (permissions_arr, group)-> return permissions_arr)
    val =  _.find permissions,(permission_hash)-> permission_hash.id is permission
    return val?.dependant

  _getChildPermissions:(permission)=>
    permissions = _.flatten (_.map @permissions, (permissions_arr, group)-> return permissions_arr)
    permissions =  _.where permissions,{dependant: permission}
    return _.pluck permissions, "id"

  removeSelectedPermissions:=>
    remove_permissions = @$("#assigned_permission_list").val()
    @$("#assigned_permission_list").val([])
    remove_permissions = @_validateRemovePermission(remove_permissions, @model.get("permissions"))
    permissions = _.difference @model.get("permissions"),  remove_permissions
    @model.save "permissions", permissions,
      success:=>
        @renderPermissions()
      url: Routes.set_permissions_role_path(@model)


  removeAllPermissions:=>
    @model.save "permissions", [],
      success:=>
        @renderPermissions()
      url: Routes.set_permissions_role_path(@model)

  assignAllGroups:=>
    @$("#available_groups_list > option").each (index, option)=>
      group_id = $(option).val()
      @_assignGroupToRole(group_id,  @model)

  assignSelectedGroups:=>
    groups_ids = @$("#available_groups_list").val()
    @$("#available_groups_list").val([])
    _.each groups_ids, (group_id)=>
      @_assignGroupToRole(group_id, @model)

  removeSelectedGroups:=>
    groups_ids = @$("#assigned_groups_list").val()
    @$("#assigned_groups_list").val([])
    _.each groups_ids, (group_id)=>
      @_removeGroupFromRole(group_id, @model)

  removeAllGroups:=>
    @$("#assigned_groups_list > option").each (index, option)=>
      group_id = $(option).val()
      @_removeGroupFromRole(group_id,  @model)


  _removeGroupFromRole:(group_id, role)=>
    assigned_role = @role_accesses.findWhere(accessable_id: parseInt(group_id), accessable_type: "Group")
    assigned_role.destroy
      success:(assigned_role)=>
        @_moveElementToList(assigned_role.get("accessable_id"), "#assigned_groups_list", "#available_groups_list")
        @role_accesses.remove assigned_role
        @renderUsers()

  _assignGroupToRole:(group_id, role)=>
    group = @groups.get(group_id)
    assigned_role = new QualityCloud.Models.RoleAccess()
    attributes = 
      role_id: role.id
      accessable_id: group_id
      accessable_type: "Group"

    assigned_role.save attributes,
      success:(assigned_role)=>
        @role_accesses.add assigned_role
        @_moveElementToList(assigned_role.get("accessable_id"), "#available_groups_list", "#assigned_groups_list")
        @renderUsers()

  assignAllUsers:=>
    @$("#available_users_list > .js-user-el:has(.js-inherit-group-access:not(:checked))").each (index, option)=>
      user_id = $(option).attr("value")
      @_assignUserToRole(user_id,  @model)

  assignSelectedUsers:=>
    users_ids = ($(user).attr("value") for user in @$("#available_users_list .selected"))
    _.each users_ids, (user_id)=>
      @_assignUserToRole(user_id, @model)

  removeSelectedUsers:=>
    users_ids = ($(user).attr("value") for user in @$("#assigned_users_list .selected"))
    _.each users_ids, (user_id)=>
      @_removeUserFromRole(user_id, @model)

  removeAllUsers:=>
    @$("#assigned_users_list > .js-user-el:has(.js-inherit-group-access:not(:checked))").each (index, option)=>
      user_id = $(option).attr("value")
      @_removeUserFromRole(user_id,  @model)

  _removeUserFromRole:(user_id, role)=>
    assigned_role = @role_accesses.findWhere(accessable_id: parseInt(user_id), accessable_type: "User")
    assigned_role.destroy
      success:(assigned_role)=>
        @_moveElementToList(assigned_role.get("accessable_id"), "#assigned_users_list", "#available_users_list")
        @role_accesses.remove assigned_role

  _assignUserToRole:(user_id, role)=>
    user = @users.get(user_id)
    assigned_role = new QualityCloud.Models.RoleAccess()
    attributes = 
      role_id: role.id
      accessable_id: user_id
      accessable_type: "User"

    assigned_role.save attributes,
      success:(assigned_role)=>
        @role_accesses.add assigned_role
        @_moveElementToList(assigned_role.get("accessable_id"), "#available_users_list", "#assigned_users_list")

  _moveElementToList:(value, from_list_id, to_list_id)=>
    $(to_list_id).append $("#{from_list_id} > [value=#{value}]")





