class QualityCloud.Views.SearchTab extends Backbone.View

  el: "#search_tab"
  tabTemplate: JST['calls/search_tab']
  events:
    'click #new_search_tab': 'newSearchResult'
    "click .js-load-calls-close" : "hideLoadCalls"
    "click #submit_load_calls" : "loadCalls"

  initialize:(options)=>
    @initSidebarButton()
    @groups_fetched = false
    @users_fetched = false
    @groupCollection = new QualityCloud.Collections.Groups(options.groupCollection)
    @userCollection = new QualityCloud.Collections.UserNames(options.userCollection)
    @searchResultViews = {}
    @tabs_counter = 0
    @ids_counter = 0
    if QualityCloud.current_user.has_permission "manage_search_tabs"
      @_loadSavedTabs()
    else
      @newSearchResult()
    $(document).tooltip
      items: "[data-help], [title]"
      content: ->
        element = $(this)
        # return "<div>Here you can search for calls and filter the results, you can also create multiple tabs: name, edit and save them to open up next time you come here.</div><hr><div>You can switch between tabs by clicking on the tab header.</div><div>You can change the name of the tab, by clicking its name: edit the name and press 'Enter' to save, or 'Esc' to cancel.</div><div><img src='images/add-new.jpg'> Click to add new search tab.</div><div><img src='images/pin.png'> Means that this tabs is saved.</div><div><img src='images/pin-active.png'> Means that this tab isn't saved yet, click to save it.</div><div><img src='images/cancle-btn2.jpg'> Click to close/delete search tab.</div>"  if element.is("#help")
        return element.attr("title")  if element.is("[title]")
        element.attr "alt"  if element.is("img")
    # "click .js-load-calls-close" : "hideLoadCalls"
    # "click #submit_load_calls" : "loadCalls"
    $(".js-load-calls-close").click @hideLoadCalls
    $("#submit_load_calls").click @loadCalls

  _loadSavedTabs:=>
    @collection = new QualityCloud.Collections.CurrentUserSavedSearchTabs()
    @collection.fetch
      success:=>
        @collection.each (savedSearch)=>
          @renderSearchResult(@initSavedSearchParams(savedSearch))
        if @tabs_counter < 4 then @newSearchResult() else @switchTabs("result_view_0", true) # Show the first tab

  renderSearchResult:(searchResult)=>
    # Keeping track of all search tabs
    @tabs_counter += 1
    @ids_counter += 1
    @$("#new_search_tab").before(@tabTemplate(tab_id: searchResult.id, tab_name: searchResult.name))
    @$("#content").append("<div class='content' id='#{searchResult.id}_content'></div>")
    @$("##{searchResult.id}").click ()=> @switchTabs(searchResult.id, false)
    searchResult.el = $("##{searchResult.id}_content")
    @searchResultViews[searchResult.id] = new QualityCloud.Views.SearchResult(searchResult)
    @searchResultViews[searchResult.id].on "searchResult:closed", @searchResultClosed, this
    @switchTabs(searchResult.id, true)

  searchResultClosed:=>
    @tabs_counter-- if @tabs_counter > 0
    if @tabs_counter == 0 then @newSearchResult() else @switchTabs("result_view_0", true) # Show the first tab


  switchTabs:(tabID, dontPopulate)=>
    oldID = @$(".nav-new-menu.active").attr('id')
    if oldID?
      return if oldID == tabID
      @$("##{oldID}").removeClass('active')
      @searchResultViews[oldID].hide() 
    @$("##{tabID}").addClass('active')
    @searchResultViews[tabID].show(dontPopulate)

  initSidebarButton:()=>
    $("#search_sidebar").css("cursor", "pointer")
    $("#search_sidebar").attr("title", "Clear Form")
    $("#search_sidebar").click @clearForm
    $("#search_sidebar a").click @clearForm

  clearForm:(e)=>
    id = @$(".nav-new-menu.active").attr('id')
    @searchResultViews[id].clearForm() 
    e = window.event  unless e
    e.cancelBubble = true
    e.stopPropagation()  if e.stopPropagation

  newSearchResult:()=>
    return alert "Sorry, You can open up to 4 tabs!" if @tabs_counter == 4
    searchResult = @initSearchParams()
    @renderSearchResult(searchResult)

  initSearchParams:()=>
    searchResult = {}
    search_params = {}
    today = new Date().toString('MM/dd/yyyy')
    search_params["from"] = today + " 00:00:01" 
    search_params["to"] = today + " 23:59:59"
    search_params["min_duration"] = ''
    search_params["max_duration"] = ''
    search_params["user_id"] = ''
    search_params["agent_id"] = ''
    search_params["group_id"] = ''
    search_params["caller_id"] = ''
    search_params["dnis"] = ''
    search_params["record_type"] = ''
    searchResult["search_params"] = search_params

    searchResult.id = "result_view_#{@ids_counter}"
    searchResult.saved = false
    searchResult.name = "New Search" 
    searchResult.model = new QualityCloud.Models.SavedSearchTab()
    searchResult.groups = @groupCollection
    searchResult.users = @userCollection
    return searchResult

  initSavedSearchParams:(savedSearch)=>
    searchResult = {}
    search_params = JSON.parse(savedSearch.get('search_params'))
    # fix date params format
    search_params["from"] = new Date(search_params["from"]).toString('MM/dd/yyyy HH:mm:ss') if search_params['from']
    search_params["to"] = new Date(search_params["to"]).toString('MM/dd/yyyy HH:mm:ss') if search_params['to']
    searchResult.search_params = search_params
    searchResult.id = "result_view_#{@ids_counter}"
    searchResult.saved = true
    searchResult.name = savedSearch.get('name')
    searchResult.model = savedSearch
    searchResult.groups = @groupCollection
    searchResult.users = @userCollection
    return searchResult    

  hideLoadCalls:=>
    $("#load_calls_popup").hide()
    @_removeOverlay()

  loadCalls:=>
    folders = []
    $("#folders_to_load input:checkbox:checked").each ->
      folders.push $(this).val()
    $.ajax
      dataType: "html"
      url: "calls/load_calls"
      data: 
        folders_paths: folders
      success: (response) =>
        alert(response)
        @hideLoadCalls()


  _showOverlay:=>
    @overlay = $("<div id='black_overlay' class='black_overlay'></div>")
    @overlay.show()
    $("body").append(@overlay)
    @$el.css("z-index", @overlay.css("z-index"))

  _removeOverlay:=>
    @overlay = $("#black_overlay")
    @overlay.remove()





