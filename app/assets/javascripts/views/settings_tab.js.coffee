class QualityCloud.Views.SettingsTab extends QualityCloud.Views.BaseView

  el: "#settings_tab"

  views_permissions:
    "show_qa_template_tool":"#template_tool_tab"

  initialize:=>
    @render()

  render:=>
    @filterByPermission()

  showTemplateTool:()->
    if QualityCloud.current_user.has_permission "show_qa_template_tool"
      @$(".sub_menue").removeClass('active')
      @$("#template_tool_tab").addClass('active')
      @templateToolTab ?= new QualityCloud.Views.TemplateToolTab()
      @templateToolTab.show()    
