class QualityCloud.Views.NestedQuestion extends Backbone.View

  template: JST['question_templates/nested_question']

  initialize:->
    @render()

  # render:=>
  #   @prepareAttributes()
  #   @$el.html(@template(call_note: @model))
  #   @$("#delete_call_note").click @deleteCallNote

  # prepareAttributes:=>
  #   d = new Date(@model.get('created_at'))
  #   d = d.toString("MM/dd/yyyy HH:mm:ss")
  #   @model.set('datetime', d)
  #   if @model.get('user')?
  #     call_note_name = if @model.get('user')? then "#{@model.get('user').first_name} #{@model.get('user').last_name}" else 'None'
  #     @model.set('agent_name', call_note_name)
  #     @model.set('user_id', @model.get('user').id)

  # deleteCallNote:=>
  #   return unless QualityCloud.current_user.id is @model.get('user_id')
  #   return unless confirm('Are you sure?')
  #   call_id = @model.get("call_id")
  #   length = @model.collection.length
  #   @model.destroy
  #     success:()=>
  #       $("#call_row_#{call_id}").removeClass("with-notes") if length == 1
  #       @unbind() # Unbind all local event bindings
  #       @remove()
  #   return false

  
