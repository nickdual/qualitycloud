class QualityCloud.Views.QuestionForm extends QualityCloud.Views.BaseView

  events:
    "click .close": "hidePopups"
    "click #submit_question_form" : 'submitForm'

  initialize:(options)->
    @questionRowView = options.questionRowView
    @template = options.template
    @render()
    @submitOnEnter()

  render:=>
    @$el.html(@template(question: @model))
    @$("label.control-label").css('float', 'none')
    $("#question_form_container").append(@el)
   
  repopulate:=> 
    @$el.html(@template(question: @model))
    @$("label.control-label").css('float', 'none')

  submitOnEnter:=>
    # disable submitting on enter key
    keyHandler = (e) => 
      if e.keyCode == 13
        @submitForm()
        return false 
    @$('form input').bind("keypress", keyHandler)

  submitForm:()=>
    success = (newAttributes) =>
      @model.set(newAttributes)
      @questionRowView.render()
      @questionRowView.markSaved()
      @repopulate()
      @hidePopups()
    form = @$('form')
    options = success: success, error: @formErrorsCallback, dataType: 'json'
    form.ajaxSubmit(options); 
  
  clearForm:()=>
    # clearing the form
    @$("input").val("")
    @$(".errors").html("")
    @$(".js-alert-errors").hide()
    
    # hide popup and layout
    @hidePopups()

  formErrorsCallback:(errors)=>
    errorsString = ""
    errorsString += "<p class='text-error'>#{key} #{value}.</p>" for key, value of errors.responseJSON

    @$(".errors").html(errorsString)
    @$(".js-alert-errors").show()

  showPopup:=>
    @_showOverlay()
    @$(".modal").show()

  hidePopups:=>
    @$(".modal").hide()
    @_removeOverlay()

  _showOverlay:=>
    @overlay = $("<div id='black_overlay' class='black_overlay'></div>")
    @overlay.show()
    $("body").append(@overlay)
    @$el.css("z-index", @overlay.css("z-index")+4)

  _removeOverlay:=>
    @overlay.remove()