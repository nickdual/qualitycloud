class QualityCloud.Views.QuestionRow extends QualityCloud.Views.BaseView

  template: JST['question_templates/question_row']
  tagName: "tr"

  # events:
  #   "click #open_question_form" : 'openQuestion'
  #   "blur #question_text" : 'setText'
  #   "blur #question_weight" : 'setWeight'
  #   "blur #question_type" : 'setType'

  views_permissions:
    "edit_templates":  "#open_question_form"

  initialize:(options)->
    @render()
    @model.on "updateSuccess", @render, this
    @model.on "destroy", @remove, this

  render:=>
    @$el.html(@template(question: @model))
    @$("#open_question_form").click @openQuestion
    @$("#question_text").blur @setText
    @$("#question_weight").blur @setWeight
    @$("#question_type").change @setType
    @filterByPermission()

  markSaved:=>
    @$el.attr("style", "background: rgb(202, 240, 205) !important;")
   
  deleteQuestion:=>
    return unless confirm('Are you sure?')
    @model.destroy
      success:()=>
        @unbind() # Unbind all local event bindings
        @remove()

  setWeight:=>
    @model.set('weight', @$("#question_weight").val())
  
  setText:=>
    @model.set('text', @$("#question_text").val())

  setType:=>
    # TODO warn the user of deleting all question data
    if @questionFormView?
      @questionFormView.unbind()
      @questionFormView.remove()
    @model.set('type', @$("#question_type").val())

  openQuestion:=>
    type = @$("#question_type").val()
    @model.set('type', type)
    # @questionFormView = window["QualityCloud.Views.#{type}Form"](model: @model)
    @questionFormView ?= new QualityCloud.Views.QuestionForm(template: JST["question_templates/question_forms/#{type}"], model: @model, questionRowView: @)
    @questionFormView.showPopup()
