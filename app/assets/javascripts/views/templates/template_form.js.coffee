class QualityCloud.Views.TemplateForm extends QualityCloud.Views.BaseView

  # ----------- Initializing

  el: "#template_form"
  
  template: JST['question_templates/template_form']

  events:
    "click #delete_template_btn" : 'deleteTemplate'
    "click #add_question_btn" : 'addQuestion'
    "click #delete_selected_btn" : 'deleteSelectedQuestions'
    "click #submit_save_template" : 'saveTemplate'
  
  views_permissions:
    "edit_templates":  "#save_template_btn, #add_question_btn, #delete_selected_btn"
    "delete_templates":  "#delete_template_btn, #create_template_btn"

  initialize:(options)=>
    @templates = options.templates

    @questionViews = []
    # don't show it if there is no templates
    if @templates.length > 0
      @model = @templates.at(0)
      # cashing questions collection
      @model.set('questionsCollection', new QualityCloud.Collections.Questions(template_id: @model.id))
      @questionsCollection = @model.get('questionsCollection')
      @questionsCollection.fetch
        success:=>
          @render()

  # ----------- Rendering

  render:()=>
    @$el.html(@template(template: @model))
    @renderQuestions()
    @handlePermissions()

  repopulate:(template)=>
    @cleanOldViews()
    @model = template
    @$el.html(@template(template: @model))

    # fetch questions collection or get from cache
    if @model.get('questionsCollection')?
      @questionsCollection = @model.get('questionsCollection')
      @renderQuestions()
      @handlePermissions()
    else
      @questionsCollection = new QualityCloud.Collections.Questions(template_id: @model.id)
      @questionsCollection.fetch
        success:=>
          # cashing questions collection
          @model.set('questionsCollection', @questionsCollection)
          @renderQuestions()
          @handlePermissions()

  handlePermissions:()=>
    @filterByPermission()
    if !QualityCloud.current_user.has_permission("edit_templates") || !@model.get('editable')
      @$(':input').attr("disabled", true)
      $("#save_template_btn, #add_question_btn, #delete_selected_btn, .js-open-adv-form").remove()
    # unless @model.get('deletable')
    #   @$('#delete_template_btn').remove()

  renderQuestions:()=>
    @questionsCollection.each (question)=> 
      @renderQuestion(question)

  renderQuestion:(question)=>
    questionView = new QualityCloud.Views.QuestionRow(model: question)
    @questionViews.push(questionView)
    @$("#questions_table").append(questionView.el)

  # ----------- Data Logic

  # saves emails, threshold, etc..
  # saveTemplate:()=>

  addQuestion:()=>
    question = new QualityCloud.Models.Question()
    question.set("rank", @questionsCollection.length+1)
    question.set("template_id", @model.id)
    question.set("user_id", @model.get('user_id'))
    @questionsCollection.add(question)
    @renderQuestion(question)
  
  deleteSelectedQuestions:()=>
    questionsNumber = @$(".js-question-checkbox:checked").length
    if questionsNumber > 0
      return unless confirm("Are you sure to delete #{questionsNumber} questions?")
      @$(".js-question-checkbox:checked").each (i, domElement)=>
        id = $(domElement).attr "question_id"
        question = @questionsCollection.get(id)
        question.destroy
          wait: true
  
  deleteTemplate:()=>
    return unless confirm("Are you sure you want to remove '#{@model.get('title')}' ?")
    template_id = @model.id
    @model.destroy
      success:()=>
        # remove from template index
        $("#template_#{template_id}").remove()
        @resetView()
      error:(response)=>
        alert(response)

  # ----------- Utilities

  cleanOldViews:()=>
    for view in @questionViews
      view.undelegateEvents()
      view.unbind()
      view.remove()
    @questionViews = []

  resetView:()=>
    @cleanOldViews()
    @$el.html("<h2 style='text-align: center;margin-top: 50px;margin-bottom: 50px;'>Select a template from the 'Template Inventory'</h2>")

  _showOverlay:=>
    @overlay = $("<div id='black_overlay' class='black_overlay'></div>")
    @overlay.show()
    $("body").append(@overlay)
    @$el.css("z-index", @overlay.css("z-index"))

  _removeOverlay:=>
    @overlay.remove()