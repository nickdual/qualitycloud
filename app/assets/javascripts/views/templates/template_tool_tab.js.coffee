class QualityCloud.Views.TemplateToolTab extends QualityCloud.Views.BaseView

  el: "#qa_tool"

  templateToolTemplate: JST['question_templates/template_tool_tab']

  # events:
  #   "hidden #template_modal" : 'removeView'

  # views_permissions:
  #   "playback_template": "#playback_div"
  #   "view_notes": "#template_notes"
  #   "create_notes": "#template_note_form"

  initialize:()=>
    @$el.html(@templateToolTemplate())
    @templates = new QualityCloud.Collections.Templates()
    # TODO bootstrap them
    @templates.fetch
      success:=>
        @render()

  render:=>
    @templateFormView = new QualityCloud.Views.TemplateForm(templates: @templates)
    @templatesIndexView =new QualityCloud.Views.TemplatesIndex(templates: @templates, templateFormView: @templateFormView)
    @show()

  show:=>
    @$el.show()