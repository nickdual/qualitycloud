class QualityCloud.Views.TemplatesIndex extends QualityCloud.Views.BaseView

  el: "#template_index"
  
  indexTemplate: JST['question_templates/templates_index']
  templateRowTemplate: JST["question_templates/template_row"]

  events:
    "click #create_template_btn" : 'newTemaplte'
    "click .close": "hidePopups"
    "click #submit_new_template_form" : 'submitNewTemaplte'
    "click .js-template-row" : 'showTemplate'

  views_permissions:
    "create_templates":  "#new_template_modal, #create_template_btn"

  initialize:(options)=>
    @templates = options.templates
    @templateFormView = options.templateFormView
    @render()

  render:=>
    @$el.html(@indexTemplate(templates: @templates, templateRowTemplate: @templateRowTemplate))
    @filterByPermission()

  newTemaplte:()=>
    @_showOverlay()
    @$("#new_template_modal").show()

  submitNewTemaplte:()=>
    template = new QualityCloud.Models.Template()
    title = @$("#template_title").val()
    userID = QualityCloud.current_user.id
    template.save {user_id: userID, title: title},
      success:=>
        # adding the new template to the index
        @$("#templates_list_container").append(@templateRowTemplate(template: template))
        @$("#template_anchor_#{template.id}").click(@showTemplate)

        # update collection
        @templates.add(template)

        @clearForm("new_template_modal")

        # show the new template
        @templateFormView.repopulate(template)
      error:(model, errors)=>
       @formErrorsCallback(errors, "new_template_modal")
  
  clearForm:(formID)=>
    # clearing the form
    @$("##{formID} input").val("")
    @$("##{formID} .errors").html("")
    @$("##{formID} .js-alert-errors").hide()
    
    # hide popup and layout
    @$("##{formID}").hide()
    @_removeOverlay()

  formErrorsCallback:(errors, formID)=>
    errorsString = ""
    errorsString += "<p class='text-error'>#{key} #{value}.</p>" for key, value of errors.responseJSON

    @$("##{formID} .errors").html(errorsString)
    @$("##{formID} .js-alert-errors").show()

  showTemplate:(e)=>
    template = @templates.get($(e.currentTarget).data("id"))
    @templateFormView.repopulate(template)

  hidePopups:=>
    @$(".modal").hide()
    @_removeOverlay()

  _showOverlay:=>
    @overlay = $("<div id='black_overlay' class='black_overlay'></div>")
    @overlay.show()
    $("body").append(@overlay)
    @$el.css("z-index", @overlay.css("z-index")+4)

  _removeOverlay:=>
    @overlay.remove()