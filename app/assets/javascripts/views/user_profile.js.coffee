class QualityCloud.Views.UserProfile extends QualityCloud.Views.BaseView

  el: "#user_profile_modal"
  events:
    "click #submit_save_form" : 'submitSaveForm'
    "click #submit_apply_form" : 'submitApplyForm'
    "click .js-close-profile": "closeForm"

  initialize:()=>
    @disableSubmitOnEnter()
    @filterByPermission()
    $(document).tooltip()
    $(".js-open-profile").click @render

  render:=>
    @_showOverlay()
    @$el.show()

  _showOverlay:=>
    @overlay = $("<div id='black_overlay' class='black_overlay'></div>")
    @overlay.show()
    $("body").append(@overlay)
    @$el.css("z-index", @overlay.css("z-index")+4)

  _removeOverlay:=>
    @overlay.remove()

  submitSaveForm:=>
    @submitForm (user) => 
      @cleanForm()
      @updateUserValues(user)

  submitApplyForm:=>
    @submitForm (user) => 
      dontHide = true
      @cleanForm(dontHide)
      @updateUserValues(user)

  updateUserValues:(user)=>
    # update form values
    @$("#user_img_profile").attr("src", "/admin/users/#{user.id}/user_avatar/medium")
    # update header values
    $("#user_img_header").attr("src", "/admin/users/#{user.id}/user_avatar/medium")
    $("#welcome_user_header").html("Welcome, #{user.full_name}")

  userFormErrorsCallback:(errors)=>
    errorsString = ""
    errorsString += "<p class='text-error'>#{key} #{value}.</p>" for key, value of errors.responseJSON

    @$(".errors").html(errorsString)
    @$('.js-alert-errors').show()

  cleanForm:(dontHide)=>
    @$el.hide() unless dontHide
    @$(".errors").html("")
    @$('.js-alert-errors').hide()
    @_removeOverlay() unless dontHide

  closeForm:=>
    @$el.hide()
    @_removeOverlay()

  disableSubmitOnEnter:=>
    # disable submitting on enter key
    @$('form input').bind "keypress", (e) => 
      if e.keyCode == 13
        @submitApplyForm()
        return false 

  submitForm:(callback)=>
    form = @$('form')
    options = success: callback, error: @userFormErrorsCallback, dataType: 'json'
    form.ajaxSubmit(options); 