class QualityCloud.Views.UserForm extends QualityCloud.Views.BaseView

  el: "#new_user_modal"
  events:
    "click #submit_new_form" : 'submitNewForm'
    "click #submit_save_new_form" : 'submitSaveAndNewForm'
    "click #submit_edit_form" : 'submitEditForm'
    "click .close": "closeForm"
    "change #user_inherit_role": "toggleRoleCheckBox"
    "change #user_group_id": "groupChanged"
  views_permissions:
    "change_user_password": "#js_password_fields"
    "assign_roles_user_tab": "#js_role_btns"

  initialize:(options)=>
    @edit_user_form = options.edit_user_form if options?.edit_user_form?
    @fetchForm(options.dont_render) if options?.fetch

  reinitialize:(model)=>
    @model = model
    @fetchForm()

  fetchForm:(dont_render)=>
    @isEditForm = (@model?)
    url = if @model? then Routes.edit_user_path(@model.id) else Routes.new_user_path()
    $.get url,
      id: @model?.id
      (@form) => 
        @render() unless dont_render
      'html'

  render:=>
    @$(".js-user-form").html(@form)
    @disableSubmitOnEnter()
    @filterByPermission()
    @_showOverlay()
    @$el.show()
    @$el.draggable
      handle: ".heading_new"
    @groupChanged()

  show:=>
    @_showOverlay()
    @$el.show()

  _showOverlay:=>
    @overlay = $("<div id='black_overlay' class='black_overlay'></div>")
    @overlay.show()
    $("body").append(@overlay)
    @$el.css("z-index", @overlay.css("z-index")+4)

  _removeOverlay:=>
    @overlay.remove()
    $(".black_overlay").remove()

  toggleRoleCheckBox:=>
    disable = inherit_role = @$("#user_inherit_role").is(":checked")
    @$("[name='user[user_role_ids][]']").attr("disabled", disable)
    if inherit_role
      @$("[name='user[user_role_ids][]']").prop('checked', false);
      for role in (@group?.get("roles") ? [])
        @$("#user_user_role_ids_#{role.id}").prop('checked', true);

  groupChanged:=>
    group_id = @$("#user_group_id").val()
    unless _.isEmpty(group_id)
      @group = new QualityCloud.Models.Group(id: group_id)
      @group.fetch
        success:=>
          @toggleRoleCheckBox()
      @$("#user_inherit_role").attr("disabled", false)
    else
      @group = null
      @$("#user_inherit_role").prop('checked', false)
      @$("#user_inherit_role").attr("disabled", true)
      @toggleRoleCheckBox()

  submitNewForm:=>
    @submitForm (response) => 
      user = new QualityCloud.Models.User(response)
      $('tbody#users_table_body').prepend(new QualityCloud.Views.UserRow(model: user, edit_user_form: @edit_user_form).el)
      @cleanForm()

  submitSaveAndNewForm:=>
    @submitForm (response) => 
      user = new QualityCloud.Models.User(response)
      $('tbody#users_table_body').prepend(new QualityCloud.Views.UserRow(model: user, edit_user_form: @edit_user_form).el)
      dontHide = true
      @cleanForm(dontHide)

  submitEditForm:=>
    @submitForm (response) => 
      @model.set(response)
      @model.trigger 'updateSuccess'
      @cleanForm()

  userFormErrorsCallback:(errors)=>
    @$('.btn').removeClass('disabled')
    errorsString = ""
    errorsString += "<p class='text-error'>#{key} #{value}.</p>" for key, value of errors.responseJSON

    @$(".errors").html(errorsString)
    @$('.js-alert-errors').show()

  cleanForm:(dontHide)=>
    @$('.btn').removeClass('disabled')
    @$el.hide() unless dontHide
    @$(".errors").html("")
    @$('.js-alert-errors').hide()
    @$("#user_supervisor_id").val("")
    @$("form input[type!='hidden']").val("")
    @$(".js-checkbox-false").attr("checked", true)
    @$("#user_inherit_role").trigger("click")
    @_removeOverlay() unless dontHide
    # @undelegateEvents()

  closeForm:=>
    @$('.btn').removeClass('disabled')
    $(@el).hide()
    @_removeOverlay()


  disableSubmitOnEnter:=>
    # disable submitting on enter key
    @$('form input').bind "keypress", (e) => 
      if e.keyCode == 13
        if @isEditForm then @submitEditForm() else @submitNewForm()
        return false 

  submitForm:(callback)=>
    @$('.btn').addClass('disabled')
    form = @$('form')
    options = success: callback, error: @userFormErrorsCallback, dataType: 'json'
    form.ajaxSubmit(options); 