class QualityCloud.Views.UserRow extends QualityCloud.Views.BaseView

  template: JST['users/user_row']
  tagName: "tr"
  events:->
    events_hash = {}
    events_hash["dblclick"] = "editUser" if QualityCloud.current_user.has_permission "edit_users"
    return events_hash

  views_permissions:
    "delete_users": ".user-checkbox"

  initialize:(options)->
    @render(options.valid)
    @edit_user_form = options.edit_user_form
    if options.valid? && !options.valid
      # TODO make it open a new form with the data inside
      @edit_user_form.userFormErrorsCallback(@model.get('errors')) 
    @model.on "updateSuccess", @render, this
    @model.on "destroy", @remove, this
    @filterByPermission()
    @$

  render:(valid)=>
    @prepareAttributes()
    @$el.html(@template(user: @model))
    if valid?
      if valid
        @$el.addClass("valid") 
        @$el.dblclick @editUser
      else 
        @$el.addClass("invalid")
        errorsString = ""
        errorsString += "#{error}\n" for error, index in @model.get("full_error_msgs")
        @$el.attr("title", errorsString)
        @$el.css("cursor", 'pointer')
        @$el.tooltip({ position: { at: "bottom center" } })
    @filterByPermission()
    QualityCloud.Helpers.TableHelpers.arrangeTableRow(@$el, QualityCloud.current_user.get("settings").users_table_columns)

  prepareAttributes:=>
    supervisor_name = if @model.get('supervisor_id')? then "#{@model.get('supervisor').first_name} #{@model.get('supervisor').last_name}" else 'None'
    @model.set('supervisor_name', supervisor_name)
    group_name = if @model.get('group')? then @model.get('group').name else 'None'
    @model.set('group_name', group_name)
    role_name = if @model.get('role')? then @model.get('role').name else 'None'
    @model.set('role_name', role_name)
    @model.set('name', "#{@model.get('first_name')} #{@model.get('last_name')}")

  editUser:=>
    @edit_user_form.reinitialize(@model)
