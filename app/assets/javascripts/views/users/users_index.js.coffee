class QualityCloud.Views.UsersIndex extends QualityCloud.Views.BaseView

  template: JST['users/index']
  users_table_template: JST['users/users_table']

  events:
    "click #create_user" : 'createNewUser'
    "click #filter_users_btn" : 'filterUsers'
    "click #delete_user" : "deleteMarkedUsers"
    "click #modify_columns" : "modifyColumns"
    'keypress #filter_users': 'filterOnEnter'
    "click .js-order" : "orderBy"
    "click #import_btn" : "openImportFile"
    "change #import_file" : "importUsers"
    "click #js_pagination_first": "fetchFirstPage"
    "click #js_pagination_last": "fetchLastPage"
    "click #js_pagination_next": "fetchNextPage"
    "click #js_pagination_prev": "fetchPrevPage"
    "click .js_pagination_page": "fetchPage"
    "keyup #current": "fetchCurrentPage"


  views_permissions:
    "create_users":  "#js_create_user_div"
    "edit_users"  :  "#edit_user_modal"
    "delete_users"  :  "#delete_user"
    "search_users": "#js_search_users_form"
    "modify_user_columns": "#modify_columns"

  columns_names:
    "user_agent_id": "Agent ID"
    "user_full_name": "Agent Name"
    "group_name": "Group"
    "user_network_login":"Network Login"
    "user_phone": "Phone Number"
    "user_email": "Email Address"
    "supervisors_users_full_name": "Supervisor"

  #this hash to hold real column settings as user settings may be not updated
  rendered_columns:{}

  initialize:()=>
    @collection = new QualityCloud.Collections.Users(paginated: true)
    @collection.on 'reset', @renderCollection, this
    @user_index_dispatcher = _.clone(Backbone.Events)
    @user_index_dispatcher.on "columns:changed", @renderUpdatedColumns, this
    @_syncColumnsList()
    @render()
    @populateCollection()

  render:=>
    @$el.html(@template(columns_names: @columns_names))
    @filterByPermission()

  renderUpdatedColumns:=>
    @loadPage 1, 
      success:(collection, response, options)=>
        @render()
        @renderCollection(collection)

  modifyColumns:=>
    new QualityCloud.Views.UsersColumnSettingsForm
      columns_list: QualityCloud.current_user.get("settings").users_table_columns
      user_index_dispatcher: @user_index_dispatcher
      columns_names: @columns_names
    $('.columnSelector').css('position','absolute')

  #sync in case new fields added and not saved to user settings yet
  _syncColumnsList:()=>
    user_columns = QualityCloud.current_user.get("settings").users_table_columns
    user_columns = _.reject(user_columns, (c)=>
        return !_.has(@columns_names, c.id)
      )

    user_columns_ids = _.pluck(user_columns, "id")
    user_columns.push({id:column_id, isVisible:true}) for column_id of @columns_names when column_id not in user_columns_ids

    QualityCloud.current_user.get("settings").users_table_columns = user_columns

  renderCollection:(collection)=>
    @$(".personal-users .main").html(@users_table_template(columns_names: @columns_names))
    
    QualityCloud.Helpers.TableHelpers.arrangeTableHeader(@$("#users_table"),
      QualityCloud.current_user.get("settings").users_table_columns)
    QualityCloud.Helpers.TableHelpers._renderPagination(collection, this)
    @renderOrderState()

    collection.each (user)=> 
      @edit_user_form ||= new QualityCloud.Views.UserForm(el: '#edit_user_modal')
      @$('tbody#users_table_body').append(new QualityCloud.Views.UserRow(
          model: user
          edit_user_form: @edit_user_form
        ).el)

    ex = document.getElementById('users_table');
    unless $.fn.DataTable.fnIsDataTable( ex )
      oTable = @$('#users_table').dataTable
        "sDom": 'Rt'
        "bInfo": false
        "sScrollY": "530px"
        "bSort": false
        "bPaginate": false

  _renderPagination:(collection)=>
    num_of_pages = 5
    pages_before = Math.floor(num_of_pages/2)

    if collection.currentPage is collection.total_pages
      @$("#js_pagination_prev").attr("disable", "disable")
    else
      @$("#js_pagination_prev").enable("disable", "")

    if collection.currentPage is 1
      @$("#js_pagination_prev").attr("disable", "disable")
    else
      @$("#js_pagination_prev").enable("disable", "")
    
    @$("#js_pages_list").html("")

    start = if (ref = collection.currentPage-pages_before) > 0 then ref else 1
    end = if (ref = collection.currentPage+pages_before) < collection.total_pages then ref else collection.total_pages
    for i in [start..end]
      @$("#js_pages_list").append $("<a href='javascript:;' class='js_pagination_page' data-page='#{i}'>#{i}</a>")
  
  renderImportedCollection:(collection, valid)=>
    collection.each (user)=> 
      @edit_user_form ||= new QualityCloud.Views.UserForm(el: '#edit_user_modal')
      @$('tbody#users_table_body').prepend(new QualityCloud.Views.UserRow(
          model: user
          edit_user_form: @edit_user_form
          valid: valid
        ).el)
      
  fetchFirstPage:=>
    @loadPage(1)

  fetchLastPage:=>
    @loadPage(@collection.end_entry || 1)

  fetchNextPage:=>
    @populateCollection()

  fetchPrevPage:=>
    @populateCollection(false)

  fetchCurrentPage:(e)=>
    if e.keyCode == 13
      page = @$(e.currentTarget).val()
      @loadPage(page)

  fetchPage:(e)=>
    page = @$(e.currentTarget).data("page")
    @loadPage(page)

  populateCollection:(forward=true, fetch_options={})=>
    params = 
      success:(collection, response, options)=>
        @renderCollection(@collection)
    $.extend params, fetch_options
    if forward then @collection.nextPage(params) else @collection.previousPage(params)
    @_disableNavigationBtns(@collection)

  loadPage:(page, fetch_options={})=>
    params=
      success:(collection, response, options)=>
        @renderCollection(@collection)
    $.extend params, fetch_options
    @collection.loadPage page, params
    @_disableNavigationBtns(@collection)

  _disableNavigationBtns:(collection)=>
    cursor  = if collection.currentPage is 1 then "default" else "pointer"
    @$("#js_pagination_first, #js_pagination_prev").css("cursor:#{cursor}")

    cursor  = if collection.currentPage is collection.total_pages then "default" else "pointer"
    @$("#js_pagination_next, #js_pagination_last").css("cursor:#{cursor}")    

  createNewUser:=>
    if @newUserForm?
      @newUserForm.show()
    else
      @newUserForm = new QualityCloud.Views.UserForm(fetch: true, edit_user_form: @edit_user_form)

  deleteMarkedUsers:=>
    no_of_users = @$(".user-checkbox:checked").length
    if no_of_users > 0
      return unless confirm("Are you sure to delete #{no_of_users} users?")
      @$(".user-checkbox:checked").each (i, domElement)=>
        id = $(domElement).attr "user_id"
        user = @collection.get id
        user.destroy
          wait: true

  filterUsers:()=>
    return @orderUsers() if @orderByValue?
    $.extend(@collection.data, {filter_users: $("#filter_users").val()})
    @loadPage 1

  filterOnEnter:(e)=>
    @filterUsers() if e.keyCode == 13

  loadOrderedPage:(page, fetch_options={})=>
    params=
      success:(collection, response, options)=>
        @collection.reset()
        collection.currentPage = 1
        @collection.add(new QualityCloud.Models.User(userAttr)) for userAttr in response
        @renderCollection(@collection)
    $.extend params, fetch_options
    @collection.loadPage page, params
    @_disableNavigationBtns(@collection)

  orderUsers:=>
    @loadOrderedPage 1,
    $.extend(@collection.data,
        filter_users: $("#filter_users").val()
        order_by: @orderByValue
    )

  orderBy:(e)=>
    @$(".js-order").removeClass("drop").removeClass("drop-up").addClass("drop-sortable")
    
    @orderDir = $(e.target).data("order_dir")
    @orderAttr = $(e.target).data("order_by")
    @orderByValue = "#{@orderAttr} #{@orderDir}"
    
    @renderOrderState()
    @orderUsers()

  renderOrderState:()=>
    header = @$("th[data-order_by='#{@orderAttr}']")
    
    header.removeClass("drop").removeClass("drop-up").removeClass("drop-sortable")
    
    if @orderDir is 'asc'
      header.data("order_dir", 'desc')
      header.addClass('drop')
    else
      header.data("order_dir", 'asc')
      header.addClass('drop-up')

  openImportFile:()=>
    $("#import_file").trigger "click"

  importUsersErrorsCallback:(errors)=>
    return

  importUsersCallback:(response)=>
    @renderImportedCollection(new QualityCloud.Collections.ImportedUsers(JSON.parse(response.valid_users)), true)
    @renderImportedCollection(new QualityCloud.Collections.ImportedUsers(JSON.parse(response.invalid_users)), false)

  importUsers:()=>
    form = @$('#import_form')
    options = success: @importUsersCallback, error: @importUsersErrorsCallback, dataType: 'json'
    form.ajaxSubmit(options); 

