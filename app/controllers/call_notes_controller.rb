class CallNotesController < ApplicationController
  load_and_authorize_resource
  # GET /call_notes
  # GET /call_notes.json
  def index
    @call_notes = CallNote.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @call_notes }
    end
  end

  def call_notes
    @call_notes = CallNote.where(:call_id => params[:call_id])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @call_notes.to_json(:include =>  
            {:user => {:only => [:first_name, :last_name, :id]}})  }
    end
  end

  # GET /call_notes/1
  # GET /call_notes/1.json
  def show
    @call_note = CallNote.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @call_note }
    end
  end

  # GET /call_notes/new
  # GET /call_notes/new.json
  def new
    @call_note = CallNote.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @call_note }
    end
  end

  # GET /call_notes/1/edit
  def edit
    @call_note = CallNote.find(params[:id])
  end

  # POST /call_notes
  # POST /call_notes.json
  def create
    @call_note = CallNote.new(params[:call_note])

    respond_to do |format|
      if @call_note.save
        format.html { redirect_to @call_note, notice: 'Call note was successfully created.' }
        format.json { render json: @call_note.to_json(:include =>  
            {:user => {:only => [:first_name, :last_name, :id]}}),
             status: :created, location: @call_note }
      else
        format.html { render action: "new" }
        format.json { render json: @call_note.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /call_notes/1
  # PUT /call_notes/1.json
  def update
    @call_note = CallNote.find(params[:id])

    respond_to do |format|
      if @call_note.update_attributes(params[:call_note])
        format.html { redirect_to @call_note, notice: 'Call note was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @call_note.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /call_notes/1
  # DELETE /call_notes/1.json
  def destroy
    @call_note = CallNote.find(params[:id])
    @call_note.destroy

    respond_to do |format|
      format.html { redirect_to call_notes_url }
      format.json { head :no_content }
    end
  end
end
