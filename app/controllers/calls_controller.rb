class CallsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :selected_item
  after_filter :set_pagination_headers, only: [:index, :search]

  # Authorization
  load_and_authorize_resource

  # GET /calls
  # GET /calls.json
  def index
    redirect_to root_path
  end

  def search
    @calls = Call.where("calls.user_id in (?)", current_user.accessed_users.pluck(:id))
    @calls = @calls.search(params)
    @calls = @calls.filter_calls params[:filter_calls] unless params[:filter_calls].blank?
    @calls = @calls.order_by_joins params[:order_by] unless params[:order_by].blank?

    @calls = @calls.paginate pagination_options
    respond_to do |format|
      format.json { render json: @calls.all.to_json(:include => {
            :user => {:include => {:group => {:only => [:name]}},
                :only => [:first_name, :last_name, :agent_id]},
            :call_notes => {:include => {:user => {:only => [:first_name, :last_name, :id]}}}
            })  }
      format.html { 
        # Alert: SUPER_USER
        authorize! :export_csv, :calls unless current_user.is_super?
        send_data @calls.to_csv, :filename => 'search_result.csv' 
      } # stupid workaround, can't make form submit with response type 'csv'!!
    end
  end


  # GET /calls/1
  # GET /calls/1.json
  def show
    @call = Call.find(params[:id])
    respond_to do |format|
      format.html{render layout: false} # show.html.erb
      format.json { render json: @call }
    end
  end

  # GET /calls/new
  # GET /calls/new.json
  def new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @call }
    end
  end

  # GET /calls/1/edit
  def edit
  end

  # POST /calls
  # POST /calls.json
  def create
    respond_to do |format|
      if @call.save
        format.html { redirect_to @call, notice: 'Call was successfully created.' }
        format.json { render json: @call, status: :created, location: @call }
      else
        format.html { render action: "new" }
        format.json { render json: @call.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /calls/1
  # PUT /calls/1.json
  def update
    respond_to do |format|
      if @call.update_attributes(params[:call])
        format.html { redirect_to @call, notice: 'Call was successfully updated.' }
        format.json { render nothing: true }
      else
        format.html { render action: 'edit' }
        format.json { render json: @call.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /calls/1
  # DELETE /calls/1.json
  def destroy
    @call.destroy

    respond_to do |format|
      format.html { redirect_to calls_url }
      format.json { head :no_content }
    end
  end

  def download_calls
    calls = Call.find(params[:call_ids])
    media_files = calls.map{|c| c.media_file }.compact
    unless media_files.empty?
      respond_to do |format|
        format.html { send_file zip(media_files, params[:call_ids].sort), 
          :type => 'application/zip',
          :filename => "Calls-#{current_user.full_name}.zip" }
      end
    end
  end

  def stream_video
    video_path = Call.find(params[:id]).mp4_file
    response.header["Cache-Control"] = "public, must-revalidate, max-age=0"
    response.header["Pragma"] = "no-cache"
    response.header["Accept-Ranges"]=  "bytes"
    response.header["Content-Transfer-Encoding"] = "binary"
    respond_to do |format|
      format.html { send_data open(video_path) {|f| f.read }, :filename => "call_#{params[:id]}_v.mp4", :type => 'video/mp4' }
    end
  end

  def stream_audio
    audio_path = Call.find(params[:id]).audio_file
    response.header["Cache-Control"] = "public, must-revalidate, max-age=0"
    response.header["Pragma"] = "no-cache"
    response.header["Accept-Ranges"]=  "bytes"
    response.header["Content-Transfer-Encoding"] = "binary"
    respond_to do |format|
      format.html { send_data open(audio_path) {|f| f.read }, :filename => "call_#{params[:id]}_a.mp3", :type => 'audio/mpeg' }
    end
  end

  # GET /calls/:id/status
  # GET /calls/:id/status.json
  def status
    status = {
      progress: @call.progress,
      status:   @call.status
    }

    respond_to do |format|
      format.json { render json: status }
    end
  end

  def load_calls
    folders = params[:folders_paths]
    report = Call.load folders

    respond_to do |format|
      format.html { render :text => report }
      format.json { render json: report, status: :created }
    end
  end

  def show_popup
    @call = Call.find(params[:id])
    respond_to do |format|
      format.html{render layout: false}
      format.json { render json: @call }
    end
  end

  private
  def set_pagination_headers
    headers["X-Pagination"] = {      
        total_pages: @calls.total_pages,
        total_entries: @calls.total_entries,
        start_entry: @calls.offset,
        end_entry: @calls.offset+@calls.length
      }.to_json
  end

  def selected_item
    @selected = :dashboard
  end

  def zip files, file_stamp
    require 'zip/zip'
    require 'zip/zipfilesystem'
    archive = File.join("public", "system", "calls_#{file_stamp}") +"_#{Time.now}.zip"
    unless File.exist? archive
      Zip::ZipFile.open(archive, 'w') do |zip_file|
        files.each_with_index do |call, index|
          ext = File.extname(call)
          guid = File.basename(call, ext)
          zip_file.add("#{guid}_#{index}#{ext}", call)
        end
      end
    end
    archive
  end
end
