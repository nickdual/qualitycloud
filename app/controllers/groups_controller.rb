class GroupsController < ApplicationController
  before_filter :selected_item
  load_and_authorize_resource
  # GET /groups
  # GET /groups.json
  def index
    @groups = current_user.accessed_groups

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @groups }
    end
  end

  # GET /groups/1
  # GET /groups/1.json
  def show
    @group = Group.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @group }
    end
  end

  # GET /groups/new
  # GET /groups/new.json
  def new
    @group = Group.new
    @roles = Role.all()

    respond_to do |format|
      format.html { render partial: 'form', layout: false }
      format.json { render json: @group }
    end
  end

  # GET /groups/1/edit
  def edit
    @group = Group.find(params[:id])
    @roles = Role.all()
    if @group.blank?
      "Group doesn't exist"
    else
      respond_to do |format|
        format.html { render partial: 'form', layout: false }
        format.json { render json: @group }
      end
    end
  end

  # POST /groups
  # POST /groups.json
  def create
    @group = Group.new(params[:group].merge(:creator => current_user))
    unless current_user.has_permissions? [:assign_roles_group_tab]
      @group.role_ids = current_user.roles.map(&:id)
    end
    respond_to do |format|
      if @group.save
        format.html { redirect_to @group, notice: 'Group was successfully created.' }
        format.json { render json: @group, status: :created }
      else
        format.html { render action: "new" }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /groups/1
  # PUT /groups/1.json
  def update
    @group = Group.find(params[:id])
    unless current_user.has_permissions? [:assign_roles_group_tab]
      params[:group].delete :role_ids
    end
    unless current_user.has_permissions? [:modify_groups]
      params[:group].slice! :role_ids
    end


    respond_to do |format|
      if @group.update_attributes(params[:group])
        format.html { redirect_to @group, notice: 'Group was successfully updated.' }
        format.json { render json: @group.to_json , status: :created }
      else
        format.html { render action: "edit" }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /groups/1
  # DELETE /groups/1.json
  def destroy
    new_group_id = params[:new_group_id]
    @group = Group.find(params[:id])
    @group.new_group_id = new_group_id
    @group.destroy

    respond_to do |format|
      format.html { redirect_to groups_url }
      format.json { head :no_content }
    end
  end

  private
  def selected_item
    @selected = :personal
    @sub_menue = :groups
  end
end
