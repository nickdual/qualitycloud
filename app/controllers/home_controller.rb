class HomeController < ApplicationController
  skip_before_filter :authenticate_user!, :only => :email_confirmation

	# GET /
  def index
  end

  def personal
    @selected = :personal
  end
  
  def administration
    @selected = :administration
  end

  def search
    # Alert: SUPER_USER
    authorize! :show, :calls unless current_user.is_super?
    @groups = current_user.accessed_groups
    @users  = current_user.accessed_users
    @calls_folders = Dir.glob("#{configatron.local_storage_path}/**/*").select {|f| File.directory? f}
    @calls_folders.unshift(configatron.local_storage_path)

    @selected = :search
  end

  def settings
    @selected = :settings
  end

  def email_confirmation
    render layout: "devise"
  end

end
