class QuestionPossibleAnswersController < ApplicationController
  # GET /question_possible_answers
  # GET /question_possible_answers.json
  def index
    @question_possible_answers = QuestionPossibleAnswer.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @question_possible_answers }
    end
  end

  # GET /question_possible_answers/1
  # GET /question_possible_answers/1.json
  def show
    @question_possible_answer = QuestionPossibleAnswer.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @question_possible_answer }
    end
  end

  # GET /question_possible_answers/new
  # GET /question_possible_answers/new.json
  def new
    @question_possible_answer = QuestionPossibleAnswer.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @question_possible_answer }
    end
  end

  # GET /question_possible_answers/1/edit
  def edit
    @question_possible_answer = QuestionPossibleAnswer.find(params[:id])
  end

  # POST /question_possible_answers
  # POST /question_possible_answers.json
  def create
    @question_possible_answer = QuestionPossibleAnswer.new(params[:question_possible_answer])

    respond_to do |format|
      if @question_possible_answer.save
        format.html { redirect_to @question_possible_answer, notice: 'Question possible answer was successfully created.' }
        format.json { render json: @question_possible_answer, status: :created, location: @question_possible_answer }
      else
        format.html { render action: "new" }
        format.json { render json: @question_possible_answer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /question_possible_answers/1
  # PUT /question_possible_answers/1.json
  def update
    @question_possible_answer = QuestionPossibleAnswer.find(params[:id])

    respond_to do |format|
      if @question_possible_answer.update_attributes(params[:question_possible_answer])
        format.html { redirect_to @question_possible_answer, notice: 'Question possible answer was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @question_possible_answer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /question_possible_answers/1
  # DELETE /question_possible_answers/1.json
  def destroy
    @question_possible_answer = QuestionPossibleAnswer.find(params[:id])
    @question_possible_answer.destroy

    respond_to do |format|
      format.html { redirect_to question_possible_answers_url }
      format.json { head :no_content }
    end
  end
end
