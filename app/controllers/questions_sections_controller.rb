class QuestionsSectionsController < ApplicationController
  # GET /questions_sections
  # GET /questions_sections.json
  def index
    @questions_sections = QuestionsSection.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @questions_sections }
    end
  end

  # GET /questions_sections/1
  # GET /questions_sections/1.json
  def show
    @questions_section = QuestionsSection.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @questions_section }
    end
  end

  # GET /questions_sections/new
  # GET /questions_sections/new.json
  def new
    @questions_section = QuestionsSection.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @questions_section }
    end
  end

  # GET /questions_sections/1/edit
  def edit
    @questions_section = QuestionsSection.find(params[:id])
  end

  # POST /questions_sections
  # POST /questions_sections.json
  def create
    @questions_section = QuestionsSection.new(params[:questions_section])

    respond_to do |format|
      if @questions_section.save
        format.html { redirect_to @questions_section, notice: 'Questions section was successfully created.' }
        format.json { render json: @questions_section, status: :created, location: @questions_section }
      else
        format.html { render action: "new" }
        format.json { render json: @questions_section.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /questions_sections/1
  # PUT /questions_sections/1.json
  def update
    @questions_section = QuestionsSection.find(params[:id])

    respond_to do |format|
      if @questions_section.update_attributes(params[:questions_section])
        format.html { redirect_to @questions_section, notice: 'Questions section was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @questions_section.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /questions_sections/1
  # DELETE /questions_sections/1.json
  def destroy
    @questions_section = QuestionsSection.find(params[:id])
    @questions_section.destroy

    respond_to do |format|
      format.html { redirect_to questions_sections_url }
      format.json { head :no_content }
    end
  end
end
