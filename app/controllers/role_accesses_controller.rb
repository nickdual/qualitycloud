class RoleAccessesController < ApplicationController
  load_and_authorize_resource
  # GET /groups
  # GET /groups.json
  def index
    @role_accesses = if params[:role_id]
      RoleAccess.where(:role_id => params[:role_id])
    elsif params[:accessable_id] and params[:accessable_type]
      RoleAccess.where :accessable_id => params[:accessable_id], :accessable_type => params[:accessable_type]
    else
      RoleAccess.all
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @role_accesses }
    end
  end

  # GET /groups/1
  # GET /groups/1.json
  def show
    @role_access = RoleAccess.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @role_access }
    end
  end

  # GET /groups/new
  # GET /groups/new.json
  def new
    @role_access = RoleAccess.new

    respond_to do |format|
      format.html { render partial: 'form', layout: false }
      format.json { render json: @role_access }
    end
  end

  # GET /groups/1/edit
  def edit
    @role_access = RoleAccess.find(params[:id])
    @roles = Role.all()
    if @role_access.blank?
      "RoleAccess doesn't exist"
    else
      respond_to do |format|
        format.html { render partial: 'form', layout: false }
        format.json { render json: @role_access }
      end
    end
  end

  # POST /groups
  # POST /groups.json
  def create
    @role_access = RoleAccess.find_or_initialize_by_accessable_id_and_accessable_type_and_role_id(params[:role_access][:accessable_id], params[:role_access][:accessable_type], params[:role_access][:role_id])

    respond_to do |format|
      if @role_access.save
        format.html { redirect_to @role_access, notice: 'RoleAccess was successfully created.' }
        format.json { render json: @role_access, status: :created }
      else
        format.html { render action: "new" }
        format.json { render json: @role_access.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /groups/1
  # PUT /groups/1.json
  def update
    @role_access = RoleAccess.find(params[:id])

    respond_to do |format|
      if @role_access.update_attributes(params[:group])
        @role_access.users.inheriters.update_all :role_id => @role_access.role_id
        format.html { redirect_to @role_access, notice: 'RoleAccess was successfully updated.' }
        format.json { render json: @role_access.to_json( :include =>  
            { :role => {:only => [:id, :name]} }) , status: :created }
      else
        format.html { render action: "edit" }
        format.json { render json: @role_access.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /groups/1
  # DELETE /groups/1.json
  def destroy
    @role_access = RoleAccess.find(params[:id])
    @role_access.destroy

    respond_to do |format|
      format.html { redirect_to groups_url }
      format.json { head :no_content }
    end
  end



end
