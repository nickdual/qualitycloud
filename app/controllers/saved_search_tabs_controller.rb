class SavedSearchTabsController < ApplicationController
  
  load_and_authorize_resource
  # GET /users/:user_id/saved_search_tabs
  def user_saved_tabs
    @saved_search_tabs = User.find(params[:user_id]).saved_search_tabs

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @saved_search_tabs }
    end
  end

  # GET /users/current_user_saved_search_tabs
  def current_user_saved_tabs
    @saved_search_tabs = current_user.saved_search_tabs

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @saved_search_tabs }
    end
  end

  # GET /saved_search_tabs
  # GET /saved_search_tabs.json
  def index
    @saved_search_tabs = SavedSearchTab.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @saved_search_tabs }
    end
  end

  # GET /saved_search_tabs/1
  # GET /saved_search_tabs/1.json
  def show
    @saved_search_tab = SavedSearchTab.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @saved_search_tab }
    end
  end

  # GET /saved_search_tabs/new
  # GET /saved_search_tabs/new.json
  def new
    @saved_search_tab = SavedSearchTab.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @saved_search_tab }
    end
  end

  # GET /saved_search_tabs/1/edit
  def edit
    @saved_search_tab = SavedSearchTab.find(params[:id])
  end

  # POST /saved_search_tabs
  # POST /saved_search_tabs.json
  def create
    @saved_search_tab = SavedSearchTab.new(params[:saved_search_tab])

    respond_to do |format|
      if @saved_search_tab.save
        format.html { redirect_to @saved_search_tab, notice: 'Saved search tab was successfully created.' }
        format.json { render json: @saved_search_tab, status: :created, location: @saved_search_tab }
      else
        format.html { render action: "new" }
        format.json { render json: @saved_search_tab.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /saved_search_tabs/1
  # PUT /saved_search_tabs/1.json
  def update
    @saved_search_tab = SavedSearchTab.find(params[:id])

    respond_to do |format|
      if @saved_search_tab.update_attributes(params[:saved_search_tab])
        format.html { redirect_to @saved_search_tab, notice: 'Saved search tab was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @saved_search_tab.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /saved_search_tabs/1
  # DELETE /saved_search_tabs/1.json
  def destroy
    @saved_search_tab = SavedSearchTab.find(params[:id])
    @saved_search_tab.destroy

    respond_to do |format|
      format.html { redirect_to saved_search_tabs_url }
      format.json { head :no_content }
    end
  end
end
