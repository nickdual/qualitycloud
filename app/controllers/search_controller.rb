class SearchController < ApplicationController
	before_filter :selected_item
	before_filter :set_calls
  before_filter :initiate_search

  # Authorization
  authorize_resource class: false # Authorizing resource has no model
  
	# GET /search
  def index
  end

  # POST /search
  def create
    render :index
  end

  private
  def selected_item
  	@selected = :search
  end

  def set_calls
    @calls = Call.search(params[:search] || {})
  end

  def initiate_search
    @search = Search.new(params[:search])
  end
end
