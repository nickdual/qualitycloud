class TemplateAnswersController < ApplicationController
  # GET /template_answers
  # GET /template_answers.json
  def index
    @template_answers = TemplateAnswer.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @template_answers }
    end
  end

  # GET /template_answers/1
  # GET /template_answers/1.json
  def show
    @template_answer = TemplateAnswer.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @template_answer }
    end
  end

  # GET /template_answers/new
  # GET /template_answers/new.json
  def new
    @template_answer = TemplateAnswer.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @template_answer }
    end
  end

  # GET /template_answers/1/edit
  def edit
    @template_answer = TemplateAnswer.find(params[:id])
  end

  # POST /template_answers
  # POST /template_answers.json
  def create
    @template_answer = TemplateAnswer.new(params[:template_answer])

    respond_to do |format|
      if @template_answer.save
        format.html { redirect_to @template_answer, notice: 'Template answer was successfully created.' }
        format.json { render json: @template_answer, status: :created, location: @template_answer }
      else
        format.html { render action: "new" }
        format.json { render json: @template_answer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /template_answers/1
  # PUT /template_answers/1.json
  def update
    @template_answer = TemplateAnswer.find(params[:id])

    respond_to do |format|
      if @template_answer.update_attributes(params[:template_answer])
        format.html { redirect_to @template_answer, notice: 'Template answer was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @template_answer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /template_answers/1
  # DELETE /template_answers/1.json
  def destroy
    @template_answer = TemplateAnswer.find(params[:id])
    @template_answer.destroy

    respond_to do |format|
      format.html { redirect_to template_answers_url }
      format.json { head :no_content }
    end
  end
end
