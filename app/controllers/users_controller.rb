class UsersController < ApplicationController
  before_filter :selected_item
  before_filter :process_create_params, only: [:create, :update]
  after_filter :set_pagination_headers, only: :index
  # Authorization
  load_and_authorize_resource

  # GET /users
  # GET /users.json
  def index
    @users = current_user.accessed_users
    Rails.logger.debug  "@" * 40    
    Rails.logger.debug  "Check the params: #{params}"

    @users = @users.filter_users_and_users_group(params[:filter_users]) unless params[:filter_users].blank?
    @users = @users.order_joins(params[:order_by]) unless params[:order_by].blank?
    @users = @users.where("group_id = ?", params[:group_id]) if params[:group_id]
    if params[:search_params]
      @users = @users.uninheriters if params[:search_params][:uninherited]
    end
    Rails.logger.debug "@" * 40
    Rails.logger.debug "Users List: #{@users}"
    Rails.logger.debug "Users Count: #{@users.size}"
    Rails.logger.debug "Users Paginated: #{@users.paginate(pagination_options)}"
    Rails.logger.debug "@" * 40
    @users = @users.paginate(pagination_options)
    respond_to do |format|
      format.html # index.html.erb
      # format.json {render "index"}
      format.json { render json: @users.to_json(:include =>  
            {:supervisor => {:only => [:first_name, :last_name]},
            :group => {:only => [:name]}})  }
            # :role => {:only => [:name]}})  }
    end
  end

  def new
    @user = User.new
    @roles = Role.all
    @groups = current_user.accessed_groups

    respond_to do |format|
      format.html { render partial: 'form', layout: false }
      format.json { render json: @user }
    end
  end

  def create
    params[:user].delete(:avatar) if params[:user][:avatar].blank?
    @user = User.new(params[:user].merge(:creator => current_user))

    respond_to do |format|
      if @user.save
        # format.html { redirect_to users_url, notice: 'User was successfully created.' }
        format.json { render json: @user.to_json(:include =>  
            {:supervisor => {:only => [:first_name, :last_name]},
            :group => {:only => [:name]},
            :role => {:only => [:name]}})  }
      else
        # format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user.to_json(:include =>  
            {:supervisor => {:only => [:first_name, :last_name]},
            :group => {:only => [:name]},
            :role => {:only => [:name]}})  }
    end
  end

  def user_names
    @users = current_user.accessed_users #TODO till we fix the jBuilders .select("id, first_name, last_name")
    respond_to do |format|
      format.json { render json: @users }
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
    if @user.blank?
      "User doesn't exist"
    else
      @roles = Role.all
      @groups = current_user.accessed_groups
      
      respond_to do |format|
        format.html { render partial: 'form', layout: false }
        format.json { render json: @user }
      end
    end
  end

  
  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find params[:id]
    error = false
    password_changed = false
    if params[:user][:password].blank?
      params[:user].delete("password")
      params[:user].delete("password_confirmation")
    # if current user has no permission to change password
    elsif !@user.change_password && current_user == @user && !@user.has_permissions?([:change_user_password])
      error = true # raise error and don't continue with update
    # if successfully changed password of current user
    elsif @user.change_password && (current_user == @user)
      password_changed = true # bypass the user and sign him in with the new password
    end

    unless @user.has_permissions? [:assign_roles_user_tab]
      params[:user].delete :roles
    end
    
    respond_to do |format|
      if !error && @user.update_attributes(params[:user].merge(:creator => current_user))
        if password_changed
          # refresh credentials in the session
          sign_out(current_user)
          sign_in(@user, :bypass => true)
        end
        format.html { redirect_to settings_tab_url }
        format.json { render json: @user.to_json(:include =>  
          {:supervisor => {:only => [:first_name, :last_name]},
          :group => {:only => [:name]},
          :role => {:only => [:name]}}) , status: :ok }
      else
        @user.errors.add(:password, "You don't have permission to change it") if error
        flash[:errors] = @user.errors.full_messages
        format.html { redirect_to settings_tab_url}
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def import
    # counter = User.import(params[:file])
    # redirect_to personal_tab_url, notice: "#{counter} Users imported successfully."
    users = User.import(params[:file], current_user)
    respond_to do |format|
      format.html { redirect_to personal_tab_url, notice: "#{users[:valid_users].count} Users imported successfully out of #{users[:invalid_users].count} records."}
      format.json { 
        json_valid_users = users[:valid_users].to_json(:include =>  
            {:supervisor => {:only => [:first_name, :last_name]},
            :group => {:only => [:name]}})  
        json_invalid_users = users[:invalid_users].to_json(:include =>  
            {:supervisor => {:only => [:first_name, :last_name]},
            :group => {:only => [:name]}},
            :methods => [:errors] )  
        render json: {:valid_users => json_valid_users, :invalid_users => json_invalid_users}.to_json
      }
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json

  def destroy
    unless @user == current_user
      @user.creator = current_user
      @user.destroy
    end

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

  # GET /groups/:group_id/non_users
  def group_members
    @users = if params[:members].blank? or params[:members] == "true"
      current_user.accessed_users.where("group_id = ?", params[:group_id])
    else
      current_user.accessed_users.where("group_id != ? or group_id IS NULL", params[:group_id])
    end
    respond_to do |format|
      format.json { render json: @users }
    end
  end

  def user_avatar
    avatar_path = User.find(params[:id]).avatar.path(params[:type])
    avatar_path ||= "#{Rails.root}/public/system/avatars/#{params[:type]}/missing.png"
    
    respond_to do |format|
      format.html { send_data open(avatar_path) {|f| f.read }, :filename => "user_#{params[:id]}", :type => 'image' }
    end
  end

  private
  def set_pagination_headers
    headers["X-Pagination"] = {      
        total_pages: @users.total_pages,
        total_entries: @users.total_entries,
        start_entry: @users.offset,
        end_entry: @users.offset+@users.length
      }.to_json
  end
  def selected_item
    @selected = :personal
    @sub_menue = :users
  end

  def init_user
    @user = User.new(params[:user], as: :admin)
  end

  def process_create_params
    params[:user].delete(:avatar) if params[:user][:avatar].blank?
  end

end
