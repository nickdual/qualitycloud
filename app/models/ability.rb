class Ability
  include CanCan::Ability

  def initialize(user)
    can :email_confirmation, HomeController
    
    can :manage, RoleAccess

    # Alert: SUPER_USER
    if user.is_super?
      can :manage, :all
    end
    
    # QA Designer

    if user.has_permissions? [:show_qa_template_tool]
      can :read, Template
      can :read, QuestionsSection
      can :read, Question
      can :read, QuestionPossibleAnswer
    end

    if user.has_permissions?([:create_templates]) || user.has_permissions?([:copy_templates])
      can [:new, :create], Template
      can [:new, :create], QuestionsSection
      can [:new, :create], Question
      can [:new, :create], QuestionPossibleAnswer
    end

    if user.has_permissions? [:edit_templates]
      can :update, Template
      can :update, QuestionsSection
      can :update, Question
      can :update, QuestionPossibleAnswer
    end

    if user.has_permissions? [:delete_templates]
      can :destroy, Template
      can :destroy, QuestionsSection
      can :destroy, Question
      can :destroy, QuestionPossibleAnswer
    end

    if user.has_permissions? [:delete_template_data]
      can :destroy, TemplateAnswer
      can :destroy, QuestionAnswer
    end

    if user.has_permissions? [:import_templates]
      can :import, Template
    end

    if user.has_permissions? [:export_templates]
      can :export, Template
    end
    # EOF QA Designer

    # QA Scoring
    if user.has_permissions? [:show_qa_tab]
      can :read, Template
      can :read, QuestionsSection
      can :read, Question
      can :read, QuestionPossibleAnswer
      can :create, TemplateAnswer
      can :create, QuestionAnswer
      can :update, TemplateAnswer do |ta|
        ta.user == user
      end  
      can :update, QuestionAnswer do |qa|
        qa.user == user
      end  
    end

    if user.has_permissions? [:delete_template_answer]
      can :destroy, TemplateAnswer
      can :destroy, QuestionAnswer
    end

    if user.has_permissions? [:edit_other_template_answer]
      can :update, TemplateAnswer
      can :update, QuestionAnswer
    end

    if user.has_permissions? [:per_line_comment]
      can :comment, QuestionAnswer
    end

    if user.has_permissions? [:overall_comment]
      can :comment, TemplateAnswer
    end
    # EOF QA Scoring

    if user.has_permissions? [:load_calls]
      can :load_calls, Call
    end

    if user.has_permissions? [:show_organization_tab]
      can :personal, HomeController
    end
    
    if user.has_permissions? [:show_search_tab]
      can :search, HomeController
    end

    if user.has_permissions? [:show_dashboard_tab]
      can :manage, DashboardController
    end
    
    if user.has_permissions? [:show_settings_tab]
      can :settings, HomeController
    end

    if user.has_permissions? [:import_users]
      can :import, User
    end

    if user.has_permissions? [:read_users]
      can :read, User
      can :user_avatar, User
    else
      can :user_avatar, User do |u|
        u == user
      end  
    end

    if user.has_permissions? [:assign_roles_user_tab]
      can :update, User
      can :read, Role
    end

    if user.has_permissions? [:create_users]
      can [:new, :create], User
    end

    if user.has_permissions? [:edit_users]
      can :update, User
    else
      can :update, User do |u|
        u == user
      end  
    end

    if user.has_permissions? [:delete_users]
      can :destroy, User
    end

    if user.has_permissions? [:read_groups]
      can :read, Group
      can :group_members, User
      can :read, Role
      can :read, AssignedRole
    end
    if user.has_permissions? [:delete_groups]
      can :destroy, Group 
    end
    if user.has_permissions? [:create_groups]
      can :create, Group 
    end

    if user.has_any_permissions? [:modify_groups, :assign_roles_group_tab]
      can :update, Group
    end

    if user.has_permissions? [:assign_role_permission]
      can :set_permissions, Role
    end

    if user.has_permissions? [:read_roles]
      can :permissions, Role
      can :read, Role
      can :read, AssignedRole
      can :read, Group
      can :read, User
    end

    if user.has_permissions? [:add_roles]
      can :create, Role
    end

    if user.has_permissions? [:delete_roles]
      can :destroy, Role
    end

    if user.has_permissions? [:modify_roles]
      can :update, Role
    end
    can [:update, :create, :destroy], RoleAccess do |role_access|
      (role_access.accessable_type == "Group" and user.has_permissions? [:assign_group_roles]) or (role_access.accessable_type == "User" and user.has_permissions? [:assign_user_roles])
      #add another condition that current user have access to this user/group
    end

    if user.has_permissions? [:read_calls]
      can :show, :calls
      can :read, Call
      can :search, Call
      can :download_calls, Call
    end

    if user.has_permissions? [:playback_call]
      can [:stream_video, :stream_audio], Call
    end

    if user.has_permissions? [:manage_search_tabs]
      can :manage, SavedSearchTab do |saved_search_tab|
        saved_search_tab.user = user
      end
    end

    if user.has_permissions? [:delete_calls]
      can :destroy, Call
    end

    if user.has_permissions? [:export_csv]
      can :search, Call
      can :export_csv, :calls #this is needed until we make a separate action for exporting
    end

    if user.has_permissions? [:view_notes]
      can :read, CallNote
      can :call_notes, CallNote
    end

    if user.has_permissions? [:create_notes]
      can :create, CallNote
      can :manage, CallNote do |call_note|
        call_note.user == user
      end
    end

    if user.has_permissions? [:search_calls]
      can :user_names, User
      can :search, Call 
    end

    # if user.system_admin?
    #   can :manage, :all
    # else
    #   can :manage, Call
    #   can :read, :dashboard
    #   can :manage, :search                
    # end
  end
end
