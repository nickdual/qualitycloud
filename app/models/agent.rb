class Agent < User

	# This method is overriden because the form_for
	# depends on it. 
	# Now we can use the form_for as follows
	# = form_for @user
	# Whatever the type of "@user" (agent, system admin or customer agent admin)
  def self.model_name
    User.model_name
  end
end

# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  type                   :string(255)      default("Agent")
#  first_name             :string(255)
#  last_name              :string(255)
#  network_login          :string(255)
#  supervisor_id          :integer
#  department             :string(255)
#  phone                  :string(255)
#  avatar_file_name       :string(255)
#  avatar_content_type    :string(255)
#  avatar_file_size       :integer
#  avatar_updated_at      :datetime
#  group_id               :integer
#  agent_id               :string(255)
#  inherit_role           :boolean
#  full_name              :string(255)
#

