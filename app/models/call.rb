class Call < ActiveRecord::Base
  # Accessible attributes
  # c = Call.create :user_id => 1, :caller_id => "588-513-433", :dialed_number => "25363-2141",
  #  :call_recording_time => (Time.now-12.days), :call_duration => 60, :dnis => "200490115315"

  # c = Call.create :user_id => 2, :caller_id => "321-555-180", :dialed_number => "98453-2903",
  #  :call_recording_time => (Time.now-14.days), :call_duration => 90, :dnis => "200463468315"

  # c = Call.create :user_id => 3, :caller_id => "456-865-423", :dialed_number => "65323-2823",
  #  :call_recording_time => (Time.now), :call_duration => 120, :dnis => "2011901141315"

  # c = Call.create :user_id => 4, :caller_id => "955-422-423", :dialed_number => "90323-9823",
  #  :call_recording_time => (Time.now-24.hours), :call_duration => 360, :dnis => "203213123"

  # c = Call.create :user_id => 5, :caller_id => "825-402-423", :dialed_number => "96663-9823",
  #  :call_recording_time => (Time.now-6.hours), :call_duration => 45, :dnis => "902333123"

  # c = Call.create :user_id => 5, :caller_id => "809-000-423", :dialed_number => "12343-9823",
  #  :call_recording_time => (Time.now-5.days), :call_duration => 1024, :dnis => "94813123"

  # , :dialed_number => "#{(100..999).to_a.sample}-#{(1000..9999).to_a.sample}-#{(100..999).to_a.sample}"
    # :call_recording_url, :subscriber_id_1, :subscriber_id_2,
    # :call_recording_time, :name, :file, 
  
  # Attachments
  # quality.rocket.dev@gmail.com/quality.rocket123
  # has_attached_file :file, storage: :Dropboxstorage, path: "/#{Rails.env}/calls/:attachment/:id/:filename"

  attr_accessible :user_id, :agent_id, :call_type, :call_guid,
    :caller_id, :digits_dialed, :call_direction, :dnis,
    :call_duration, :mp4_file, :audio_file, :media_type, :audio_recording_time

  # Duration getter & setter
  def call_duration=(duration_string)
    # parse the format '1:30:00' into seconds
    write_attribute(:call_duration, ChronicDuration.parse(duration_string||"", :keep_zero => true))
  end

  def call_duration
    # outputs the seconds into this format '1:30:00'
    
    ChronicDuration.output(read_attribute(:call_duration)||0)#, :format => :chrono)
  rescue 
    return "0 sec"
  end

  def media_file
    if self.media_type == MEDIA_TYPES[:none]
      return nil
    else
      self.mp4_file.blank? ? self.audio_file : self.mp4_file
    end
  end

  # Validations
  validates :call_guid, :uniqueness => true

  # Media Type
  MEDIA_TYPES = {:none => 0, :video => 1,
   :audio => 2, :audio_and_video => 3}

  # Scopes
  scope :today, where("audio_recording_time >= ?", Time.zone.now.beginning_of_day)

  scope :filter_calls, lambda{|key|
    wild_key = "%#{key}%"
    int_key = (key == (key.to_i.to_s)) ? key : -1
    joins{user.outer}.joins{group.outer}.
       where{
         (user.first_name.like wild_key) |
         (user.last_name.like wild_key) |
         (user.full_name.like wild_key) |
         # (user.agent_id.eq int_key) |
         (user.agent_id.like wild_key) |
         (user.network_login.like wild_key) |
         (group.name.like wild_key) |
         (digits_dialed.like wild_key) |
         (dnis.like wild_key) |
         (caller_id.like wild_key)
       }
  }

  #Associations
  belongs_to :user
  has_one :group, through: :user
  has_many :call_notes, dependent: :destroy, :order => 'created_at DESC'
  accepts_nested_attributes_for :call_notes, reject_if: proc { |attributes| attributes['note'].blank? }

  # Search
  def self.search params
    scope = Call
    unless params[:from].blank?
      date_time = DateTime.strptime(params[:from], '%m/%d/%Y %H:%M:%S')
      scope = scope.where([ 'calls.audio_recording_time >= ?', date_time ]) 
    end
    unless params[:to].blank?
      date_time = DateTime.strptime(params[:to], '%m/%d/%Y %H:%M:%S')
      scope = scope.where([ 'calls.audio_recording_time <= ?', date_time ]) 
    end
    scope = scope.where([ 'call_duration >= ?', ChronicDuration.parse(params[:min_duration], :keep_zero => true)]) unless params[:min_duration].blank?
    scope = scope.where([ 'calls.call_duration <= ?', ChronicDuration.parse(params[:max_duration], :keep_zero => true) ]) unless params[:max_duration].blank?
    scope = scope.where([ 'calls.caller_id LIKE ?', "%#{params[:caller_id]}%" ]) unless params[:caller_id].blank?
    scope = scope.where([ 'calls.dnis LIKE ?', "%#{params[:dnis]}%" ]) unless params[:dnis].blank?
    scope = scope.where{ user_id.in params[:user_id] } unless params[:user_id].blank?
    scope = scope.joins{user.outer} unless params[:agent_id].blank? && params[:group_id].blank? 
    scope = scope.where{ (user.agent_id.in params[:agent_id]) } unless params[:agent_id].blank?
    scope = scope.where{ (user.group_id.in params[:group_id]) } unless params[:group_id].blank?

    # record type searching
    if !params[:record_type].blank? && params[:record_type].include?("audio_only") && params[:record_type].include?("with_video")
      scope = scope.where('calls.media_type > 0') 
    else
      scope = scope.where('calls.media_type = 1 or calls.media_type = 3') if !params[:record_type].blank? && params[:record_type].include?("with_video")
      scope = scope.where('calls.media_type = 2') if !params[:record_type].blank? && params[:record_type].include?("audio_only")
    end
    scope = scope.joins{call_notes} if !params[:record_type].blank? && params[:record_type].include?("with_notes")

    scope
  end

  def self.order_by_joins order_by
    joins{user.outer}.joins{group.outer}.order(order_by)
  end

  # Exporting as CSV
  def self.to_csv(options = {})
    exported_column_names = ["id",
       "audio_recording_time",
       "agent_id",
       "call_type",
       "call_guid",
       "caller_id",
       "digits_dialed",
       "call_direction",
       "call_duration",
       "dnis"]

    CSV.generate(options) do |csv|
      csv << exported_column_names
      all.each do |call|
        csv << call.attributes.values_at(*exported_column_names)
      end
    end
  end

  # Loading XML files from given folders paths
  def self.load folders_paths
    call_xml_paths = Set.new
    folders_paths.each do |folder_path|
      call_xml_paths << Dir.glob("#{folder_path}/**/*.xml")
    end
    self.parse_new_call call_xml_paths
  end

  # Parsing XML files
  def self.parse_new_call call_paths
    xml_files = []
    new_ones = 0
    edited_ones = 0
    invalid_ones = 0
    invalid_ones_guids = []
    call_paths.to_a.flatten.uniq.collect{ |cp| xml_files << cp if File.extname(cp) == '.xml' }
    xml_files.each do |call_path|
      attr_hash = extract_attributes call_path
      call = Call.where :call_guid => attr_hash["call_guid"]
      call_notes = attr_hash.delete "call_notes_attributes"
      if call.blank?
        call = Call.create attr_hash
        if call 
          call_notes.each do |note|
            call_note = CallNote.new note
            call_note.call = call
            call_note.save
          end
          new_ones += 1
        else
          invalid_ones += 1
          invalid_ones_guids << attr_hash["call_guid"]
        end
      else
        call.first.update_attributes attr_hash 
        edited_ones += 1
      end
    end
    report = ""
    report << "#{new_ones} Calls Created successfully!\r\n" if new_ones > 0
    report << "#{edited_ones} Calls Updated successfully!\r\n" if edited_ones > 0
    report << "#{invalid_ones} Invalid Calls their guids are :\r\n" if invalid_ones > 0
    invalid_ones_guids.each do |g|
      report << "#{g} \r\n"
    end
    report << "Reload page to see changes!\r\n"    if new_ones > 0 || edited_ones > 0
    return report
  end

  def self.parse_modified_call call_paths
    self.parse_new_call call_paths
  end

  def self.parse_removed_call call_paths
  end

  def self.extract_attributes call_path
    guid = File.basename(call_path, '.xml')
    contents = File.read(call_path)
    attr_hash = Hash.from_xml contents
    call_notes = extract_call_notes(attr_hash['CallRecord'])
    attr_hash = Hash[attr_hash['CallRecord'].map {|k, v| [k.underscore, v] if k.underscore.in?(Call.column_names)}]
    set_user_id(attr_hash)
    set_media_type(attr_hash)
    attr_hash["audio_recording_time"] = time_stamp_to_date_time(attr_hash["audio_recording_time"])
    attr_hash["call_notes_attributes"] = call_notes
    attr_hash
  end

  def self.time_stamp_to_date_time(art) # 08162013094132
    # DateTime.new(2013,8,16, 9,41,32) 
    DateTime.new(art[4..7].to_i, art[0..1].to_i, art[2..3].to_i, art[8..9].to_i, art[10..11].to_i, art[11..12].to_i) rescue DateTime.now
  end

  def self.extract_call_notes attr_hash
    call_notes = []
    if !attr_hash["TrayComments"].blank? 
      if attr_hash["TrayComments"].class == Hash # only one comment
        comment = attr_hash["TrayComments"]["TrayComment"]
        users = User.where(:agent_id => comment["AgentID"])
        call_notes << {:note => comment["Comment"], :user_id => users.first.id} unless users.blank?
      elsif !attr_hash["TrayComments"].first["TrayComment"].blank?
        attr_hash["TrayComments"].first["TrayComment"].each do |comment|
          users = User.where(:agent_id => comment["AgentID"])
          call_notes << {:note => comment["Comment"], :user_id => users.first.id} unless users.blank?
        end  
      end
    end
    call_notes
  end

  def self.set_user_id attr_hash
    users = User.where(:agent_id => attr_hash["agent_id"])
    attr_hash["user_id"] = users.first.id unless users.blank?
  end

  def self.set_media_type attr_hash
    if attr_hash["audio_file"].blank? && attr_hash["mp4_file"].blank?
      attr_hash[:media_type] = MEDIA_TYPES[:none] 
    elsif attr_hash["audio_file"].blank?
      attr_hash["mp4_file"] = "#{configatron.media_storage_path}#{attr_hash["mp4_file"]}"
      attr_hash[:media_type] = MEDIA_TYPES[:video] 
    elsif attr_hash["mp4_file"].blank?
      attr_hash["audio_file"] = "#{configatron.media_storage_path}#{attr_hash["audio_file"]}"
      attr_hash[:media_type] = MEDIA_TYPES[:audio] 
    else
      attr_hash["mp4_file"] = "#{configatron.media_storage_path}#{attr_hash["mp4_file"]}"
      attr_hash["audio_file"] = "#{configatron.media_storage_path}#{attr_hash["audio_file"]}"
      attr_hash[:media_type] = MEDIA_TYPES[:audio_and_video] 
    end
  end

  # Parsing Damaged XML files
  def self.parse_damaged_new_call call_paths
    xml_files = []
    call_paths.collect{ |cp| xml_files << cp if File.extname(cp) == '.xml' }
    xml_files.each do |call_path|
      attr_hash = extract_damaged_attributes call_path
      call = Call.where :call_guid => attr_hash["call_guid"]
      call_notes = attr_hash.delete "call_notes_attributes"
      if call.blank?
        call = Call.create attr_hash
        call_notes.each do |note|
          call_note = CallNote.new note
          call_note.call = call
          call_note.save
        end
      else
        call.first.update_attributes attr_hash 
      end
    end
  end

  def self.extract_damaged_attributes call_path
    guid = File.basename(call_path, '.xml')
    contents = File.read(call_path)
    attr_hash = Hash.from_xml contents
    call_notes = extract_damaged_call_notes(attr_hash['CallRecord'])
    attr_hash = Hash[attr_hash['CallRecord'].map {|k, v| [k.underscore, v] if k.underscore.in?(Call.column_names)}]
    set_damaged_user_id(attr_hash)
    set_damaged_media_type(attr_hash)
    attr_hash["call_notes_attributes"] = call_notes
    attr_hash
  end

  def self.extract_damaged_call_notes attr_hash
    call_notes = []
    if !attr_hash["TrayComments"].blank? 
      if attr_hash["TrayComments"].class == Hash # only one comment
        comment = attr_hash["TrayComments"]["TrayComment"]
        users = User.where(:agent_id => comment["AgentID"])
        if users.blank?
          call_notes << {:note => comment["Comment"], :user_id => User.all.map{ |u| u.id }.sample}
        else  
          call_notes << {:note => comment["Comment"], :user_id => users.first.id}
        end
      elsif !attr_hash["TrayComments"].first["TrayComment"].blank?
        attr_hash["TrayComments"].first["TrayComment"].each do |comment|
          users = User.where(:agent_id => comment["AgentID"])
          if users.blank?
            call_notes << {:note => comment["Comment"], :user_id => User.all.map{ |u| u.id }.sample}
          else  
            call_notes << {:note => comment["Comment"], :user_id => users.first.id}
          end
        end  
      end
    end
    call_notes
  end

  def self.set_damaged_user_id attr_hash
    users = User.where(:agent_id => attr_hash["agent_id"])
    if users.blank?
      attr_hash["user_id"] = User.all.map{ |u| u.id }.sample
    else  
      attr_hash["user_id"] = users.first.id
    end
  end

  def self.set_damaged_media_type attr_hash
    attr_hash[:media_type] = [MEDIA_TYPES[:video], MEDIA_TYPES[:audio] , MEDIA_TYPES[:none] ].sample
    case attr_hash[:media_type]
    when MEDIA_TYPES[:video]
      attr_hash["audio_file"] = nil
      attr_hash["mp4_file"] = "public/mp4.mp4"
    when MEDIA_TYPES[:audio]
      attr_hash["audio_file"] = "public/mp3.mp3"
      attr_hash["mp4_file"] = nil
    when MEDIA_TYPES[:none]
      attr_hash["audio_file"] = nil
      attr_hash["mp4_file"] = nil
    end
  end
end