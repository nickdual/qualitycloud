class CallNote < ActiveRecord::Base
  belongs_to :call
  belongs_to :user
  attr_accessible :note, :call_id, :user_id, :user, :call


  validates :note, :call_id, :user_id, :presence => true
  validates_uniqueness_of :note, :scope => [:call_id, :user_id]

  # User.all.each {|u| Call.all.each {|c| 
  # 	CallNote.create :user => u, :call => c, :note => "#{u.name} is commenting on #{c.user.name}'s call " } }

end

# == Schema Information
#
# Table name: call_notes
#
#  id         :integer          not null, primary key
#  call_id    :integer
#  user_id    :integer
#  note       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

