class Permission
  #IMPORTANT
  # when adding a new permission, when a permission depend on another one, add it below the one it depends on.
  #this is because of an issue in client side
  PERMISSIONS={
    "QC Functionality"=> [
      {:id => :show_organization_tab, :name => "Show Organization Tab"},
      {:id => :show_dashboard_tab, :name => "Show Dashboard Tab"},
      {:id => :show_search_tab, :name => "Show Search Tab"},
      {:id => :show_settings_tab, :name => "Show Settings Tab"}
    ],
    "Users" => [
      {id: :read_users  , :name => "Show Users", :dependant => :show_organization_tab},
      {id: :edit_users  , :name => "Edit Users", :dependant => :read_users},
      {id: :change_user_password , :name => "Change User Password", :dependant => :edit_users},
      {id: :create_users   , :name => "Create Users", :dependant => :read_users},
      {id: :import_users, :name => "Import Users", :dependant => :create_users},
      {id: :delete_users, :name => "Delete Users", :dependant => :read_users},
      {id: :modify_user_columns, :name => "Modify User Columns", :dependant => :read_users}
    ],
    "Search Functions" => [
      {id: :search_calls, :name => "Search Calls", :dependant => :read_calls},
      {id: :search_users, :name => "Search Users", :dependant => :read_users}
    ],
    "Groups"=> [
      {:id => :read_groups, :name => "Read Groups", :dependant => :show_organization_tab},
      {:id => :create_groups, :name => "Create Groups", :dependant => :read_groups},
      {:id => :modify_groups, :name => "Modify Groups", :dependant => :read_groups},
      {:id => :delete_groups, :name => "Delete Groups", :dependant => :read_groups}

    ],
    "Calls"=> [
      {:id => :read_calls, :name => "Show Calls", :dependant => :show_search_tab},
      {:id => :playback_call, :name => "Playback Call", :dependant => :read_calls},
      {:id => :manage_search_tabs, :name => "Save Searches", :dependant => :read_calls},
      {:id => :export_csv, :name => "Export as CSV", :dependant => :read_calls},
      {:id => :load_calls, :name => "Load Calls", :dependant => :read_calls},
      {:id => :delete_calls, :name => "Delete Calls", :dependant => :read_calls},
      {:id => :view_notes, :name => "View Comments", :dependant => :read_calls},
      {:id => :create_notes, :name => "Create Comments", :dependant => :view_notes},
      {:id => :modify_call_columns, :name => "Modify Call Columns", :dependant => :read_calls}
    ],
    "Roles"=> [
      {:id => :read_roles, :name => "Read Roles", :dependant => :show_organization_tab},
      {:id => :add_roles, :name => "Create Roles", :dependant => :read_roles},
      {:id => :modify_roles, :name => "Modify Roles", :dependant => :read_roles},
      {:id => :delete_roles, :name => "Delete Roles", :dependant => :read_roles},
      {:id => :assign_user_roles, :name => "Change Role Access to View Users", :dependant => :read_roles},
      {:id => :assign_group_roles, :name => "Change Role Access to View Groups", :dependant => :read_roles},
      {:id => :assign_template_roles, :name => "Change Role Access to Use Templates", :dependant => :read_roles},
      {:id => :assign_role_permission, :name => "Assign Role Permission", :dependant => :read_roles},
      {:id => :assign_roles_group_tab, :name => "Assign Roles to Groups", :dependant => :modify_groups},
      {:id => :assign_roles_user_tab, :name => "Assign Roles to Users", :dependant => :edit_users}
    ],
    "QA Designer"=> [
      {:id => :show_qa_template_tool, :name => "Access QA Designer", :dependant => :show_settings_tab},
      {:id => :create_templates, :name => "Create Templates", :dependant => :show_qa_template_tool},
      {:id => :edit_templates, :name => "Modify Templates", :dependant => :create_templates},
      {:id => :delete_templates, :name => "Delete Templates", :dependant => :show_qa_template_tool},
      {:id => :delete_template_data, :name => "Delete Data Permanently", :dependant => :delete_templates},
      {:id => :import_templates, :name => "Import Templates", :dependant => :create_templates},
      {:id => :export_templates, :name => "Export Templates", :dependant => :show_qa_template_tool},
      {:id => :copy_templates, :name => "Copy Templates", :dependant => :create_templates},
      {:id => :enable_email_alerts, :name => "Enable Email Alerts", :dependant => :edit_templates},
      {:id => :use_advanced_settings, :name => "Use Advanced Settings", :dependant => :edit_templates}
    ],
    "QA Scoring"=> [
      {:id => :show_qa_tab, :name => "Access QA Tab Under Call Playback", :dependant => :show_search_tab},
      {:id => :delete_template_answer, :name => "Delete QA Record", :dependant => :show_qa_tab},
      {:id => :edit_other_template_answer, :name => "Edit Others QA Records", :dependant => :show_qa_tab},
      {:id => :per_line_comment, :name => "Make Per-Line Comments", :dependant => :show_qa_tab},
      {:id => :overall_comment, :name => "Make Overall Comments", :dependant => :show_qa_tab}
    ]
  }

  def self.permissions_array
    PERMISSIONS.values.flatten.map{|permission|permission[:id]}
  end
end