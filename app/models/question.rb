class Question < ActiveRecord::Base

  # Associations
  belongs_to :template
  belongs_to :questions_section
  belongs_to :question
  has_many :question_possible_answers, dependent: :destroy
  has_many :question_answers, dependent: :destroy

  # Attributes
  attr_accessible :coach_hint, :default_response, :rank, :spawn_nested_response, :text, :type, :weight, :template_id, :questions_section_id, :question_id, :question_possible_answers_attributes
  accepts_nested_attributes_for :question_possible_answers, 
    :allow_destroy => true

  # Validations
  validates :type, :weight, :rank, :text, :template_id, :presence => true
  validates :weight, :rank, :numericality => { :only_integer => true, :greater_than_or_equal_to => 0 }

  # Callbacks
  before_validation :check_type_changes
  before_save :check_template_editablity
  before_destroy :check_template_editablity

  def check_type_changes
    # TODO handle type changes and possible answers
  end

  def check_template_editablity
    return self.template.check_template_editability(self.errors)
  end

  # Defines the question types
  TYPES = [ 'QuestionYesNo', 'QuestionScale' ]  

  # Create the predicate methods for the question.
  # Example: question.question_yes_no?
  TYPES.map(&:parameterize).map(&:underscore).each do |type|
    define_method "#{type}?" do
      self.is_a?(type.titleize.gsub(/\s+/, '').constantize)
    end
  end

  def as_json(*args)
    #TODO: refactor convert to jbuilder
    hash = super(*args)
    hash.merge!({:type => self.type})
    hash
  end
end

# belongs_to template 
# belongs_to questions_section
# belongs_to question : the parent question (if this is a nested question)
# has_many questions_possible_answers
# has_many question_answers
# question_type: as integer, each type is handled by programmer, it differs in: rendering, calculating answer weight, and answer value
# text: question body
# coach_hint 
# default_response: as text contains the value of an answer
# spawn_nested_response: as text contains the value of an answer that if user enters, it should spawn the nested questions
# weight: as integer
# rank: to order its appearance in the template