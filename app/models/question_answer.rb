class QuestionAnswer < ActiveRecord::Base

  # Associations
  belongs_to :template_answer
  belongs_to :question
  belongs_to :user

  # Attributes
  attr_accessible :answer, :weight
  
end

# belongs_to template_answer
# belongs_to question
# belongs_to user: who answered it
# answer 
# weight: as integer
