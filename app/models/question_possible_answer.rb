class QuestionPossibleAnswer < ActiveRecord::Base
  
  # Associations
  belongs_to :question

  # Attributes
  attr_accessible :answer, :weight

   # Callbacks
  before_save :check_template_editablity
  before_destroy :check_template_editablity

  def check_template_editablity
    return self.question.template.check_template_editability(self.errors)
  end
  
end

# belongs_to question: it depends on question type for example; it could be the answer itself, or could be the boundries for question of type "on scale of (1 to x)"
# answer 
# weight: as integer
