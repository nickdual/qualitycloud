class QuestionScale < Question

  # Validations
  validate :check_scales

  def weigh_answer question_answer
    from, to = self.scale_figuers
    diff = to - from

    answer_weight = (question_answer.answer-from) / diff * self.weight
    question_answer.update_attribute :weight, answer_weight
  end

  def list_possible_answers
    from, to = self.scale_figuers
    (from..to).to_a
  end

  def scale
    # it should be validated in before_save of the question
    self.question_possible_answers.order("answer asc")
  end

  def scale_figuers
    # it should be validated in before_save of the question
    self.question_possible_answers.order("answer asc").pluck(:answer).map{|a| a.to_i}
  end

  def check_scales
    if self.question_possible_answers.count != 2
      self.errors[:base] << "you should specify the start and end of the scale"
    elsif self.question_possible_answers.pluck('answer').select{|i| i.to_i.to_s == i }.count !=2
      self.errors[:base] << "the 'from' and 'to' should be integers"
    end
  end

  def as_json(*args)
    #TODO: refactor convert to jbuilder
    hash = super(*args)
    from, to = self.scale
    hash.merge!({:from => from.answer, :to => to.answer, :from_id => from.id, :to_id => to.id})
    hash
  end

end

# Scale Question
# Has two question_possible_answers:
  # 
# "Yes" answer scores all the question weight, while "No" scores zero