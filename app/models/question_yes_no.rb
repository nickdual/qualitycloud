class QuestionYesNo < Question

  def weigh_answer question_answer
    answer_weight = "Yes".casecmp(question_answer.answer) == 0 ? self.weight : 0
    question_answer.update_attribute :weight, answer_weight
  end

  def list_possible_answers
    ["Yes", "No"]
  end

end

# Simple (Yes/No) question
# Has no question_possible_answers
# "Yes" answer scores all the question weight, while "No" scores zero