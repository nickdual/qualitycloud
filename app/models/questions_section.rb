class QuestionsSection < ActiveRecord::Base

  # Associations
  belongs_to :template
  has_many :questions, dependent: :destroy

  # Attributes
  attr_accessible :rank, :title
  
  # Callbacks
  before_save :check_template_editablity
  before_destroy :check_template_editablity

  def check_template_editablity
    return self.template.check_template_editability(self.errors)
  end

end

# belongs_to template
# has_many questions
# title
# rank: to order its appearance in the template
