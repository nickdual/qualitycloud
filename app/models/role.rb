class Role < ActiveRecord::Base
  attr_accessible :name, :permissions, :supervisable, :disabled
  attr_accessor :new_role_id
  serialize :permissions, Array

  validates :name, :presence => true
  validates :name, :uniqueness => {:case_sensitive => false}

  has_many :assigned_roles
  has_many :assigned_groups, through: :assigned_roles, conditions: {roleable_type: 'Group'}
  has_many :assigned_users, through: :assigned_roles, conditions: {roleable_type: 'User'}

  has_many :role_accesses, dependent: :destroy
  has_many :accessed_groups, through: :role_accesses, conditions: {accessable_type: 'Group'}
  has_many :accessed_users, through: :role_accesses, conditions: {accessable_type: 'User'}

  before_destroy :check_deleted_role
  before_save :check_preset_roles_attributes

  default_scope order('roles.name ASC')

  def check_deleted_role
    if name == configatron.default_role_name
      self.errors[:base] << "Cannot delete '#{configatron.default_role_name}' role."
      return false 
    # Alert: SUPER_USER
    elsif name == configatron.super_role_name  
      self.errors[:base] << "Cannot delete '#{configatron.super_role_name}' role."
      return false 
    else
      replacement_role = Role.where(:id => self.new_role_id).first ||
        Role.find_by_name(configatron.default_role_name)
      groups.each do |group| 
        group.update_attribute(:role_ids, [replacement_role.id]) unless group.has_other_role?(self)
      end
      users.each do |user| 
        user.update_attribute(:user_roles, [replacement_role]) unless user.has_other_role?(self)
      end
    end 
  end

  # Alert: SUPER_USER
  def check_preset_roles_attributes
    # make sure that the preset roles aren't changed
    if self.name_was == configatron.super_role_name
      self.name = configatron.super_role_name
    elsif self.name_was == configatron.default_role_name
      self.name = configatron.default_role_name
    end
  end

  scope :supervisable, where(:supervisable => true)
  scope :disabled, where(:disabled => true)
  scope :enabled, where(:disabled => false)

  def groups
    Group.find assigned_roles.where(roleable_type: 'Group').map{|r| r.roleable_id}
  end

  def users
    User.find assigned_roles.where(roleable_type: 'User').map{|r| r.roleable_id}
  end

  def can_delete?
  end

  def as_json(*args)
    hash = super(*args)
    hash.merge!({:permissions => self.permissions})
    hash
  end

end

# == Schema Information
#
# Table name: roles
#
#  id           :integer          not null, primary key
#  name         :string(255)      default(""), not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  permissions  :integer
#  supervisable :boolean
#  disabled     :boolean          default(FALSE)
#

