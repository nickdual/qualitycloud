class RoleAccess < ActiveRecord::Base
  attr_accessible :accessable_id, :accessable_type, :role_id, :accessable

  belongs_to :role
  belongs_to :accessable
  validates :role_id, :accessable_id, :accessable_type, presence: true
  # validate :user_not_have_group

  # Alert: SUPER_USER
  before_save :check_super_user_access
  def check_super_user_access
    # super email user can't be accessed, other super users can be accessed normally
    if self.accessable_type == "User" && User.find(self.accessable_id).is_super_email?
      errors.add("Super users are not accessibles.")
    elsif self.accessable_type == "Group" && Group.find(self.accessable_id).is_super?
      errors.add("Super group are not accessibles.")
    end
  end


  def user_not_have_group
    if self.accessable_type == "User"
      if User.find(self.accessable_id).group_id
        errors.add("Can't got access user who is a member of a group")
      end
    end
  end
end

# == Schema Information
#
# Table name: role_accesses
#
#  id              :integer          not null, primary key
#  role_id         :integer
#  accessable_id   :integer
#  accessable_type :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

