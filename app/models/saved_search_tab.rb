class SavedSearchTab < ActiveRecord::Base
  attr_accessible :name, :search_params, :user_id

  belongs_to :user

  validates :name, :uniqueness => {:scope => :user_id}
end

# == Schema Information
#
# Table name: saved_search_tabs
#
#  id            :integer          not null, primary key
#  user_id       :integer
#  search_params :string(255)
#  name          :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

