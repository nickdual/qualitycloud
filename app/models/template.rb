class Template < ActiveRecord::Base

  # Associations
  belongs_to :user
  has_many :questions_sections, dependent: :destroy
  has_many :questions, dependent: :destroy
  has_many :template_answers, dependent: :destroy

  # Attributes
  attr_accessible :min_score_alert, :title, :total_weight, :user_id, :enabled

  # Validations
  validates :title, :user_id, :presence => true
  validates :title, :uniqueness => {:case_sensitive => false}

  # Callbacks
  before_update :check_editablity

  def check_editablity
    self.errors.add("", "This template can't be edited, as users already scored against it!") unless self.template_answers.empty?
    self.errors("", "This template can't be edited, as it is disabled!") unless self.enabled
  end

  # Template can't be edited if it has template answer
  def editable
    self.enabled && self.template_answers.empty?
  end

  # Utilities
  def check_template_editability errors
    errors.add("", "This template can't be edited, as users already scored against it!") unless self.template_answers.empty?
    errors.add("", "This template can't be edited, as it is disabled!") unless self.enabled
    return self.editable
  end

  def as_json(*args)
    #TODO: refactor convert to jbuilder
    hash = super(*args)
    hash.merge!({:editable => self.editable})
    hash
  end
  
end

# acts as roleable, and accessible
# belongs_to user : who created it
# has_many questions_sections
# has_many template_answers
# min_score_alert: when a user scores this template with overall score lower than this, it alerts the system
# total_weight: the sum of weights of questions in this template
