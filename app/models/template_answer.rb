class TemplateAnswer < ActiveRecord::Base
 
  # Associations
  belongs_to :template
  belongs_to :user
  belongs_to :call
  has_many :question_answers

  # Attributes
  attr_accessible :total_weight

end

# belongs_to template
# belongs_to call : to be valued
# belongs_to user : who answered the template
# has_many question_answers
# total_weight: as integer
