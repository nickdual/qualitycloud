class User < ActiveRecord::Base
  #---------Attributes---------#

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :first_name,
    :last_name, :phone, :supervisor_id, :avatar, :department, :network_login, :agent_id, 
    :group_id, :inherit_role, :full_name, :user_role_ids, :creator, :settings,
    :locked, :change_password, :complex_password, :super_user, :inherit_group_access

  # for Admin
	attr_accessible :email, :password, :password_confirmation, :remember_me, :first_name,
    :last_name, :phone, :supervisor_id, :avatar, :department, :network_login, :agent_id,
    :group_id, :inherit_role, :full_name, :user_role_ids, :type, :creator, :settings, 
    :locked, :change_password, :complex_password, :inherit_group_access, :super_user, as: :admin


  # Setting user's avatar settings
  has_attached_file :avatar, {
    :styles => { :medium => "215x215#", :thumb => "24x24#" },
    :default_url => "/images/:style/missing.png",
    :url => '/system/avatars/:style/:id'
  }

  serialize :settings, Hash
=begin
  {
    users_table_columns:[
      {id:1, isVisible:true},
      {id:1, isVisible:true},
      {id:1, isVisible:true}
    ],
    calls_table_columns:[
      {id:1, isVisible:true},
      {id:1, isVisible:true},
      {id:1, isVisible:true}
    ],
    users_table_per_page:20,
    calls_table_per_page:20,
  }
=end

  # the user who is creating/editing/deleting this user object
  attr_accessor :creator, :full_error_msgs

  #---------Validations---------#
  validates :first_name, :last_name, :group_id, :presence => true
  validates :password, :presence => true, :on => :create
  validates :password_confirmation, :presence => true, :if => :password_changed?
  validates :agent_id, :uniqueness => true
  validates :email, :uniqueness => true, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i}
  validates :network_login, :uniqueness => true, :allow_nil => true
  validate :check_supervisor #, :if => :supervisor_changed?
  validate :check_network_login #, :if => :supervisor_changed?
  validate :password_complexity, :if => :force_complex_password?
  validate :avatar, :paperclip_image_dimensions, :unless => "avatar.queued_for_write[:original].blank? || errors.any?"

  #---------Associations---------#
  belongs_to :supervisor, :class_name => 'User'
  has_many :subordinates, class_name: 'User',
                          foreign_key: "supervisor_id"
  belongs_to :group
  has_many :group_roles, through: :group, :source => :roles
  has_many :assigned_roles, dependent: :destroy, as: :roleable
  has_many :user_roles, through: :assigned_roles, source: :role
  has_many :role_accesses, dependent: :destroy, as: :accessable
  has_many :calls
  has_many :saved_search_tabs
  
  default_scope order('users.full_name ASC')
  scope :inheriters, where(:inherit_role => true) 
  scope :uninheriters, where(:inherit_role => false) 
  scope :user_names, select("id, first_name, last_name")
  
  before_save :process_roles_ids, :set_full_name, :check_group, :check_super_user_attributes, :check_network_login
  after_commit :give_creator_access_to_user
  after_initialize :init
  before_destroy :check_super_user

  # Custom validator for checking uploaded image dimention
  def paperclip_image_dimensions
    min_width = min_height = 215
    file = avatar.queued_for_write[:original]
    if file
      dimensions = Paperclip::Geometry.from_file(file.path)
      if dimensions.width < min_width && dimensions.height < min_height
        errors.add(:avatar, "image too small, must be at least #{min_width}x#{min_height} pixels")
      end
    end
  end

  # Alert: SUPER_USER
  def check_super_user_attributes
    # make sure that the super email user isn't changed
    if self.email_was == configatron.super_user_email
      self.email = configatron.super_user_email
      self.locked = false
      self.change_password = true
      self.complex_password = true
      self.super_user = true
      self.group_id = Group.find_by_name(configatron.super_group_name).id
    end
    # make sure that only super users can edit other super users
    if (self.is_super? || self.super_user_was) && self.creator && !self.creator.is_super?
      self.errors[:base] << "Only super user can edit other super users!"
    end
  end

  # Alert: SUPER_USER
  def check_super_user
    # the email user can't be deleted
    if self.is_super_email?
      self.errors[:base] << "Cannot delete '#{full_name}' User."
      return false 
    elsif self.is_super?
      # super user can delete other super user
      return self.creator.is_super?
    end 
  end

  def check_network_login
    self.network_login = nil if (self.has_attribute? :network_login) && ( (self.network_login.blank?) || ("none".casecmp(self.network_login) == 0) )
  end

  def set_full_name
    self.full_name = "#{first_name} #{last_name}".titleize
  end

  def check_group
    self.group_id = Group.find_by_name(configatron.default_group_name).id if (self.has_attribute? :group_id) && self.group_id.blank?
  end

  def process_roles_ids
    if self.inherit_role
      self.user_role_ids = []
    end
  end

  def give_creator_access_to_user
    role = creator.try(:roles).try(:first)
    if role
      role.role_accesses.create(:accessable_id => self.id, :accessable_type => "User")
    end
  end

  def  init
    self.inherit_role = true if (self.has_attribute? :inherit_role) && self.inherit_role.nil?
    self.group_id = Group.find_by_name(configatron.default_group_name).id if (self.has_attribute? :group_id) && self.group_id.blank?
  end

  #---------Super User---------#
  # Alert: SUPER_USER
  def is_super?
    self.super_user || email == configatron.super_user_email
  end

  def is_super_email?
    email == configatron.super_user_email
  end
  #-----Disabling user-------#
  def disabled?
    locked && self.roles.disabled.count == self.roles.count
  end

  #this method is called by devise to check for "active" state of the model
  def active_for_authentication?
    # Alert: SUPER_USER
    is_super? || (super && !disabled?)
  end

  def inactive_message
    disabled? ? "You have been blocked by the adminstration." : super
  end

  #---------Scopes---------#
  scope :filter_users, lambda{|key|
    key = "%#{key}%"
    User.where("first_name ilike ? or 
     last_name ilike ? or
     email ilike ? or 
     phone ilike ? or
     department ilike ? or
     network_login ilike ? or
     full_name ilike ?", key, key, key, key, key, key, key)
  }

  scope :filter_user_group, lambda{|key|
    key = "%#{key}%"
    joins{group.outer}.where{(group.name.like key)}
  }

  scope :filter_users_and_users_group, lambda{|key|
    wild_key = "%#{key}%"
    int_key = key != key.to_i.to_s ? -1 : key
    joins{group.outer}.joins{supervisor.outer}.where{ (group.name.like wild_key) |
      (supervisor.full_name.like wild_key) |
      (first_name.like wild_key) | 
      (last_name.like wild_key) |
      (full_name.like wild_key) |
      (email.like wild_key) | 
      (phone.like wild_key) |
      (department.like wild_key) |
      (agent_id.like wild_key) | 
      (network_login.like wild_key)
    }
  }

  scope :order_joins, lambda{ |by| joins{group.outer}.joins{supervisor.outer}.reorder(by) }

  #--------- Supervisors ---------#

  def self.supervisables
    users = []
    User.all.each { |u| users << u if u.supervisable? }
    users
  end

  def supervisable?
    self.roles.supervisable.count > 0
  end

  def supervisor_changed?
    self.supervisor_id_changed?
  end

  def check_supervisor
    errors.add(:supervisor, "Supervisor must have 'Supervisor' role") if !self.supervisor.blank? && !self.supervisor.supervisable?
  end

  #--------- Import Users ---------#

  def self.import(file, current_user)
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    header = header.map{|h| h.underscore.strip.tr " ", "_"}
    counter = 0
    valid_users = []
    invalid_users = []
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      if row["agent_id"].blank? || (row["agent_id"] && "none".casecmp(row["agent_id"]) == 0)
        user = User.new
      else
        user = find_by_agent_id(row["agent_id"]) || new
      end
      row["network_login"] = nil if row["network_login"] && "none".casecmp(row["network_login"]) == 0
      row["phone"] = row["telephone"]
      row["full_name"] = "#{row["first_name"]} #{row["last_name"]}".titleize
      row["password"] = row["default_pw"]
      row["password_confirmation"] = row["default_pw"]
      row["group_id"] = Group.where{ (name.like row["group"]) }.first.try(:id)
      if row["supervisor_id"].blank? || (row["supervisor_id"] && "none".casecmp(row["supervisor_id"]) == 0)
        row["supervisor_id"] = nil
      else
        row["supervisor_id"] = find_by_agent_id(row["supervisor"]).try(:id) 
      end
      user_role = Role.where{ (name.like row["role"]) }.first
      user.user_roles << user_role if user_role
      user.attributes = row.to_hash.slice(*accessible_attributes)
      counter += 1 
      user.creator = current_user
      if user.save
        valid_users << user 
      else
        user.full_error_msgs = user.errors.full_messages
        invalid_users << user 
      end
    end
    {:valid_users => valid_users, :invalid_users => invalid_users}
  end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when ".csv" then Roo::Csv.new(file.path, nil, :ignore)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path, nil, :ignore)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  #--------- Roles & Permissions ---------#

  def role
    roles.first
  end

  def accessed_users
    # Alert: SUPER_USER
    if is_super?
      User.scoped
    else
      groups_ids = RoleAccess.where("role_id in (?) and accessable_type = 'Group'", self.roles).pluck(:accessable_id)
      users_ids = RoleAccess.where("role_id in (?) and accessable_type = 'User'", self.roles).pluck(:accessable_id)
      User.where("(users.id in (?) and users.inherit_group_access = false) or (users.group_id in (?) and users.inherit_group_access = true)", users_ids, groups_ids)
    end
  end

  def accessed_groups
    # Alert: SUPER_USER
    if is_super?
      Group.scoped
    else
      groups_ids = RoleAccess.where("role_id in (?) and accessable_type = 'Group'", self.roles).pluck(:accessable_id)
      Group.where("groups.id in (?)", groups_ids)
    end
  end

  def roles(enabled=false)
    if self.inherit_role
      all_roles = group_roles
    else
      all_roles = user_roles
    end
    all_roles = all_roles.enabled if enabled 
    all_roles
  end

  def has_role?
    ! (roles.empty? or roles.all? {|role| role.marked_for_destruction? })
  end

  def has_other_role? deleted_role
    ! (roles.empty? or roles.all? {|role| role.id == deleted_role.id })
  end

  def permissions
    # Alert: SUPER_USER
    if is_super?
      Permission.permissions_array
    else
      # only enabled roles
      self.roles(true).map(&:permissions).flatten
    end
  end

  def has_permissions?(permissoins_arr)
    permissoins_arr.each{ |permission| 
      return false unless self.permissions.include?("#{permission}") || self.permissions.include?(permission)
    }
    return true
  end

  def has_any_permissions?(permissoins_arr)
    permissoins_arr.each{ |permission| 
      return true if self.permissions.include?("#{permission}") || self.permissions.include?(permission)
    }
    return false
  end

  def validates_role_uniqness(role)
    raise ActiveRecord::Rollback if self.user_roles.include? role
  end

  #---------Utilities---------#
  # override the original to_s function
  def to_s
    name
  end

  def name
    "#{first_name.capitalize} #{last_name.capitalize}"
  end

  # Show the supervisor by his name
  def supervisor_name
    self.supervisor.blank? ? 'none' : self.supervisor.name
  end

  def password_changed?
    self.encrypted_password_changed?
  end

  def force_complex_password?
    self.complex_password
  end
  
  def password_complexity
    if password_changed? and not password.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+/)
      errors.add :password, "must include at least one lowercase letter, one uppercase letter, and one digit"
    end
  end

  def as_json(*args)
    #TODO: refactor convert to jbuilder
    hash = super(*args)
    
    hash.merge!({:permissions => self.permissions})
    hash.merge!({:full_error_msgs => self.full_error_msgs}) unless self.full_error_msgs.blank?

    hash
  end

# Alert: SUPER_USER
  def self.send_reset_password_instructions(attributes = {})
    u = User.select("email, change_password").find_by_email attributes["email"]
    if u and u.change_password == false and not u.is_super?
      u.errors[:base] << "Sorry you cannot change your password, contact your administrator"
    else
      u = super(attributes)
    end
    return u
  end

end

# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  type                   :string(255)      default("Agent")
#  first_name             :string(255)
#  last_name              :string(255)
#  network_login          :string(255)
#  supervisor_id          :integer
#  department             :string(255)
#  phone                  :string(255)
#  avatar_file_name       :string(255)
#  avatar_content_type    :string(255)
#  avatar_file_size       :integer
#  avatar_updated_at      :datetime
#  group_id               :integer
#  agent_id               :string(255)
#  inherit_role           :boolean
#  full_name              :string(255)
#

