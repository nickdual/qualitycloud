# Put all your default configatron settings here.

# Example:
#   configatron.emails.welcome.subject = 'Welcome!'
#   configatron.emails.sales_reciept.subject = 'Thanks for your order'
# 
#   configatron.file.storage = :s3
configatron.vblaze_url = ""

configatron.default_group_name = "Default"
configatron.default_role_name = "Default"

# Alert: SUPER_USER
configatron.super_group_name       = "Super Group"
configatron.super_role_name        = "Super Role"
configatron.super_user_email       = "super@abc.com"
configatron.super_user_password    = "SuperUserPassword1"
configatron.super_user_first_name  = "Super"
configatron.super_user_last_name   = "User"