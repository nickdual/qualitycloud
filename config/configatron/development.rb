# Override your default settings for the Development environment here.
# 
# Example:
#   configatron.file.storage = :local
configatron.vblaze_url = ""

configatron.database.development.adapter = 'mysql2'
configatron.database.development.encoding = 'utf8'
configatron.database.development.reconnect = false
configatron.database.development.database = 'quality_cloud_development'
configatron.database.development.pool = 5
configatron.database.development.username = 'root'
configatron.database.development.password = 'root'
configatron.database.development.socket = '/var/run/mysqld/mysqld.sock'


configatron.local_storage_path ='public/storage'
configatron.media_storage_path =''
