set :application, "quality_cloud"
set :rails_env, "production"
set :domain, "quality_cloud.com"
set :env, "production"
set :user, "deployer"
set :branch, "master"
set :deploy_to, "/var/www/rails_apps/quality_cloud"
server "172.16.20.110", :web, :app, :db, primary: true