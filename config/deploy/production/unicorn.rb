root = "/var/www/rails_apps/quality_cloud/current"
working_directory root
pid "#{root}/tmp/pids/unicorn.pid"
stderr_path "#{root}/log/unicorn.log"
stdout_path "#{root}/log/unicorn.log"

listen "/tmp/unicorn.quality_cloud.sock"
worker_processes 2
timeout 30