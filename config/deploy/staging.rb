set :application, "quality_cloud_staging"
set :rails_env, "staging"
set :env, "staging"
set :domain, "quality_cloud_staging.com"
set :user, "deployer"
set :branch, "staging"
set :deploy_to, "/var/www/rails_apps/quality_cloud_staging"
server "172.16.20.110", :web, :app, :db, primary: true
