## Create a callback
#callback = Proc.new do |modified, added, removed|
#  puts ">>>>>>> in listener callback"
#  # This proc will be called when there are changes.
#  Call.parse_new_call added
#  Call.parse_modified_call modified
#  Call.parse_removed_call removed
#end
#
#Listen.to(configatron.local_storage_path)
#  .latency(1)
#  .relative_paths(false)
#  .polling_fallback_message('Fallback to polling')
#  .change(&callback)
#  .start