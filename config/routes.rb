QualityCloud::Application.routes.draw do

  resources :question_answers


  resources :template_answers


  resources :question_possible_answers


  resources :questions


  resources :questions_sections


  resources :templates do
    resources :questions
  end


  get 'calls/:call_id/notes' => "call_notes#call_notes"
  resources :call_notes

  get '/users/:user_id/saved_search_tabs' => "saved_search_tabs#user_saved_tabs"
  get '/users/current_user_saved_search_tabs' => "saved_search_tabs#current_user_saved_tabs"
  resources :saved_search_tabs

  match '/organization' => "home#personal", as: :personal_tab
  match '/administration' => "home#administration", as: :administration_tab
  match '/search' => "home#search", as: :search_tab
  get '/settings' => "home#settings", as: :settings_tab
  
  resources :roles do
    member do
      put :set_permissions
    end
  end
  
  match "/permissions" => "roles#permissions"

  # get '/groups/:group_id/non_users' => "users#get_non_group_users"
  resources :groups do
    resources :users, only: [] do
      collection do
        get "/", action: :group_members
      end
    end
  end

  get 'calls/download_calls' => "calls#download_calls"
  get 'calls/:id/stream_video' => "calls#stream_video"
  get 'calls/:id/stream_audio' => "calls#stream_audio"
  match '/calls/search' => "calls#search"
  match '/calls/load_calls' => "calls#load_calls"
  resources :calls do 
    member do
      get :status, :show_popup
    end
  end

  resources :dashboard, only: [ :index ]
  resources :searches, only: [ :index, :create ], controller: :search, path: '/search'

  get '/admin/users/:id/user_avatar/:type' => "users#user_avatar"
  get '/admin/users/user_names' => "users#user_names"
  scope :admin, path: '/admin' do
    resources :users do
      collection { post :import }
    end
  end
  
  devise_for :users, :controllers => { :passwords => "passwords" }
  authenticated :user do
    root :to => 'home#search'
  end
  get "home/email_confirmation" => "home#email_confirmation"

  # if not authenticated, route him to login page
  unauthenticated :user do
    devise_scope :user do 
      get "/" => "devise/sessions#new"
    end
  end

  # root to: 'devise/sessions#new'
  # root to: 'home#personal'

  resources :assigned_roles
  resources :role_accesses

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
