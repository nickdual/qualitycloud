class RenameUploadsToCalls < ActiveRecord::Migration
  def up
  	rename_table :uploads, :calls
  end

  def down
  	rename_table :calls, :uploads
  end
end
