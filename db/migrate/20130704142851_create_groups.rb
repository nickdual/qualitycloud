class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string  :name, :null => false, :default => ""
      t.integer :role_id 
      t.timestamps
    end
  end
end
