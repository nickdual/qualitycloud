class AddInheritRoleToUsers < ActiveRecord::Migration
  def change
    add_column :users, :inherit_role, :boolean
  end
end
