class AddPermissionsToRoles < ActiveRecord::Migration
  def change
    add_column :roles, :permissions, :integer
  end
end
