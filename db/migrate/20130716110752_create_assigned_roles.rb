class CreateAssignedRoles < ActiveRecord::Migration
  def change
    create_table :assigned_roles do |t|
      t.integer :role_id
      t.integer :roleable_id
      t.string :roleable_type

      t.timestamps
    end
    add_index :assigned_roles, :roleable_id
    add_index :assigned_roles, :roleable_type
  end
end
