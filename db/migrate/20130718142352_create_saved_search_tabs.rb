class CreateSavedSearchTabs < ActiveRecord::Migration
  def change
    create_table :saved_search_tabs do |t|
      t.integer :user_id
      t.string :search_params
      t.string :name

      t.timestamps
    end
  end
end
