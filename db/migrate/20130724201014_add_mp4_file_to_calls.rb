class AddMp4FileToCalls < ActiveRecord::Migration
  def change
    add_column :calls, :mp4_file, :string
  end
end
