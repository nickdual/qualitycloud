class ChangeAgentIdToString < ActiveRecord::Migration
  def self.up
    change_column :users, :agent_id, :string
  end

  def self.down
    change_column :users, :agent_id, :integer
  end
end
