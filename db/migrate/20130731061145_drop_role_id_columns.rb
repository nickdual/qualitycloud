class DropRoleIdColumns < ActiveRecord::Migration
  def up
    remove_column :users, :role_id
    remove_column :groups, :role_id
  end

  def down
    add_column :users, :role_id, :integer
    add_column :groups, :role_id, :integer
  end
end
