class ModifyCallsAttributes < ActiveRecord::Migration
	def up
    remove_column :calls, :name
    remove_column :calls, :file_file_name
    remove_column :calls, :file_content_type
    remove_column :calls, :file_file_size
    remove_column :calls, :file_updated_at
    remove_column :calls, :decoded_text
    remove_column :calls, :subscriber_id_1
    remove_column :calls, :subscriber_id_2
    remove_column :calls, :call_recording_url
    remove_column :calls, :call_recording_time

    add_column :calls, :audio_file, :string
    add_column :calls, :agent_id, :string
    add_column :calls, :media_type, :integer

    rename_column :calls, :dialed_number, :digits_dialed

    change_column :calls, :call_type, :string
    change_column :calls, :call_direction, :string
  end
  
  def down
    add_column :calls, :name, :string
    add_column :calls, :file_file_name, :string
    add_column :calls, :file_content_type, :string
    add_column :calls, :file_file_size, :integer
    add_column :calls, :file_updated_at, :datetime
    add_column :calls, :decoded_text, :text
    add_column :calls, :subscriber_id_1, :string
    add_column :calls, :subscriber_id_2, :string
    add_column :calls, :call_recording_url, :string
    add_column :calls, :call_recording_time, :datetime

    remove_column :calls, :audio_file
    remove_column :calls, :agent_id
    remove_column :calls, :media_type

    rename_column :calls, :digits_dialed, :dialed_number

    change_column :calls, :call_type, :integer
    change_column :calls, :call_direction, :integer
  end
end
