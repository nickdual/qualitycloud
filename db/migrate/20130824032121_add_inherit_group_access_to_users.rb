class AddInheritGroupAccessToUsers < ActiveRecord::Migration
  def change
    add_column :users, :inherit_group_access, :boolean, :default => true
  end
end
