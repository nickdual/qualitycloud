class CreateTemplates < ActiveRecord::Migration
  def change
    create_table :templates do |t|
      t.references :user
      t.string :title
      t.integer :min_score_alert
      t.integer :total_weight

      t.timestamps
    end
    add_index :templates, :user_id
  end
end
