class CreateQuestionsSections < ActiveRecord::Migration
  def change
    create_table :questions_sections do |t|
      t.references :template
      t.string :title
      t.integer :rank

      t.timestamps
    end
    add_index :questions_sections, :template_id
  end
end
