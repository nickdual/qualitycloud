class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.references :template
      t.references :questions_section
      t.references :question
      t.string :type
      t.text :text
      t.string :coach_hint
      t.string :default_response
      t.string :spawn_nested_response
      t.integer :weight
      t.integer :rank

      t.timestamps
    end
    add_index :questions, :template_id
    add_index :questions, :questions_section_id
    add_index :questions, :question_id
  end
end
