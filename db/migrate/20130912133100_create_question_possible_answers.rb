class CreateQuestionPossibleAnswers < ActiveRecord::Migration
  def change
    create_table :question_possible_answers do |t|
      t.references :question
      t.string :answer
      t.integer :weight

      t.timestamps
    end
    add_index :question_possible_answers, :question_id
  end
end
