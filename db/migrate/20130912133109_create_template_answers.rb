class CreateTemplateAnswers < ActiveRecord::Migration
  def change
    create_table :template_answers do |t|
      t.references :template
      t.references :user
      t.references :call
      t.integer :total_weight

      t.timestamps
    end
    add_index :template_answers, :template_id
    add_index :template_answers, :user_id
    add_index :template_answers, :call_id
  end
end
