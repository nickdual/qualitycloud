class CreateQuestionAnswers < ActiveRecord::Migration
  def change
    create_table :question_answers do |t|
      t.references :template_answer
      t.references :question
      t.references :user
      t.string :answer
      t.integer :weight

      t.timestamps
    end
    add_index :question_answers, :template_answer_id
    add_index :question_answers, :question_id
    add_index :question_answers, :user_id
  end
end
