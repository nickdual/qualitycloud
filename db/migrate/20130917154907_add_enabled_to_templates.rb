class AddEnabledToTemplates < ActiveRecord::Migration
  def change
    add_column :templates, :enabled, :boolean, :default => true
  end
end
