class AddAudioRecordingTimeToCalls < ActiveRecord::Migration
  def change
    add_column :calls, :audio_recording_time, :datetime
    
    add_index :calls , :audio_recording_time
  end
end
