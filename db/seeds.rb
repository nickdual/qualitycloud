# Creating system admin
# puts '==> Creating a system admin with creadintials admin@example.com/12345678'.colorize(:green)
# SystemAdmin.create!(email: 'admin@example.com', password: '12345678', password_confirmation: '12345678',
#                     first_name: 'system', last_name: 'admin', network_login: 'systemadmin')

# # Creating customer agent admin
# puts '==> Creating a customer agent admin with creadintials customer.admin@example.com/12345678'.colorize(:green)
# CustomerAgentAdmin.create!(email: 'customer.admin@example.com', password: '12345678', password_confirmation: '12345678',
#                     first_name: 'customer agent', last_name: 'admin', network_login: 'customeragentadmin')

# # Creating an agent
# puts '==> Creating an agent with creadintials agent@example.com/12345678'.colorize(:green)
# Agent.create!(email: 'agent@example.com', password: '12345678', password_confirmation: '12345678',
#                     first_name: 'agent', last_name: 'agent', network_login: 'agent')

# Alert: SUPER_USER

# Creating Super User
puts '==> Creating the super role'.colorize(:green)
Role.find_or_create_by_name!(name: configatron.super_role_name)

puts '==> Creating the super group'.colorize(:green)
super_group = Group.find_or_create_by_name!(name: configatron.super_group_name, role_ids: [Role.find_by_name(configatron.super_role_name).id])

puts "==> Creating Super User #{configatron.super_user_email}/#{configatron.super_user_password}".colorize(:green)
User.find_or_create_by_email!(email: configatron.super_user_email,
  password: configatron.super_user_password,
  password_confirmation: configatron.super_user_password,
  first_name: configatron.super_user_first_name,
  last_name: configatron.super_user_last_name,
  group_id: super_group.id,
  change_password: true,
  super_user: true,
  complex_password: true)

# Creating Super User
puts '==> Creating the default role'.colorize(:green)
Role.find_or_create_by_name!(name: configatron.default_role_name)

puts '==> Creating the default group'.colorize(:green)
Group.find_or_create_by_name!(name: configatron.default_group_name, role_ids: [Role.find_by_name(configatron.default_role_name).id])