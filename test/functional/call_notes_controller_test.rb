require 'test_helper'

class CallNotesControllerTest < ActionController::TestCase
  setup do
    @call_note = call_notes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:call_notes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create call_note" do
    assert_difference('CallNote.count') do
      post :create, call_note: { note: @call_note.note }
    end

    assert_redirected_to call_note_path(assigns(:call_note))
  end

  test "should show call_note" do
    get :show, id: @call_note
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @call_note
    assert_response :success
  end

  test "should update call_note" do
    put :update, id: @call_note, call_note: { note: @call_note.note }
    assert_redirected_to call_note_path(assigns(:call_note))
  end

  test "should destroy call_note" do
    assert_difference('CallNote.count', -1) do
      delete :destroy, id: @call_note
    end

    assert_redirected_to call_notes_path
  end
end
