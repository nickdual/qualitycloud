require 'test_helper'

class QuestionPossibleAnswersControllerTest < ActionController::TestCase
  setup do
    @question_possible_answer = question_possible_answers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:question_possible_answers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create question_possible_answer" do
    assert_difference('QuestionPossibleAnswer.count') do
      post :create, question_possible_answer: { answer: @question_possible_answer.answer, weight: @question_possible_answer.weight }
    end

    assert_redirected_to question_possible_answer_path(assigns(:question_possible_answer))
  end

  test "should show question_possible_answer" do
    get :show, id: @question_possible_answer
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @question_possible_answer
    assert_response :success
  end

  test "should update question_possible_answer" do
    put :update, id: @question_possible_answer, question_possible_answer: { answer: @question_possible_answer.answer, weight: @question_possible_answer.weight }
    assert_redirected_to question_possible_answer_path(assigns(:question_possible_answer))
  end

  test "should destroy question_possible_answer" do
    assert_difference('QuestionPossibleAnswer.count', -1) do
      delete :destroy, id: @question_possible_answer
    end

    assert_redirected_to question_possible_answers_path
  end
end
