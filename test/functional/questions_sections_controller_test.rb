require 'test_helper'

class QuestionsSectionsControllerTest < ActionController::TestCase
  setup do
    @questions_section = questions_sections(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:questions_sections)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create questions_section" do
    assert_difference('QuestionsSection.count') do
      post :create, questions_section: { rank: @questions_section.rank, title: @questions_section.title }
    end

    assert_redirected_to questions_section_path(assigns(:questions_section))
  end

  test "should show questions_section" do
    get :show, id: @questions_section
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @questions_section
    assert_response :success
  end

  test "should update questions_section" do
    put :update, id: @questions_section, questions_section: { rank: @questions_section.rank, title: @questions_section.title }
    assert_redirected_to questions_section_path(assigns(:questions_section))
  end

  test "should destroy questions_section" do
    assert_difference('QuestionsSection.count', -1) do
      delete :destroy, id: @questions_section
    end

    assert_redirected_to questions_sections_path
  end
end
