require 'test_helper'

class TemplateAnswersControllerTest < ActionController::TestCase
  setup do
    @template_answer = template_answers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:template_answers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create template_answer" do
    assert_difference('TemplateAnswer.count') do
      post :create, template_answer: { total_weight: @template_answer.total_weight }
    end

    assert_redirected_to template_answer_path(assigns(:template_answer))
  end

  test "should show template_answer" do
    get :show, id: @template_answer
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @template_answer
    assert_response :success
  end

  test "should update template_answer" do
    put :update, id: @template_answer, template_answer: { total_weight: @template_answer.total_weight }
    assert_redirected_to template_answer_path(assigns(:template_answer))
  end

  test "should destroy template_answer" do
    assert_difference('TemplateAnswer.count', -1) do
      delete :destroy, id: @template_answer
    end

    assert_redirected_to template_answers_path
  end
end
